Dokumentacja techniczna ekspandera GPIO PCA6408A: [https://www.nxp.com/products/.../PCA6408A](https://www.nxp.com/products/interfaces/ic-spi-i3c-interface-devices/general-purpose-i-o-gpio/low-voltage-8-bit-ic-bus-and-smbus-i-o-expander-with-interrupt-output-reset-and-configuration-registers:PCA6408A)

## Format argumentów CLI

```
i2ctool [OPTIONS] INTERFACE-NAME INTERFACE-INSTANCE BITRATE dev SLAVE-ADDRESS pca6408a FUNCTION MODE ARGS

Where:
  [OPTIONS]             See help (-h, --help).
  INTERFACE-NAME:       I2C interface name.
  INTERFACE-INSTANCE:   I2C interface instance number (0-255, HEX/DEC format).
  BITRATE:              I2C interface bitrate in bps (HEX/DEC format).
  SLAVE-ADDRESS:        I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
  FUNCTION:             Device functions: get, set.
  MODE:                 Device function modes: in, out, polarity, dir.
  ARGS:                 Device function arguments: temperature, configuration.
```

## Funkcje

| Function | Mode     | Args            | Return          | Description                                           | Bits value
| :----    | :----    | :----           | :----           | :----                                                 | :----
| get      | in       | -               | Inputs state    | Read 8 inputs state register.                         | 0=L, 1=H
|          | out      | -               | Outputs state   | Read 8 outputs state from output register (not pins). | 0=L, 1=H
|          | polarity | -               | Inputs polarity | Read 8 inputs polarity register.                      | 0=no change, 1=inverted
|          | dir      | -               | Pins direction  | Read 8 pins direction register.                       | 0=output, 1=input
| set      | out      | Outputs state   | -               | Set 8 outputs state.                                  | 0=L, 1=H
|          | polarity | Inputs polarity | -               | Set 8 inputs polarity.                                | 0=no change, 1=inverted
|          | dir      | Pins direction  | -               | Set 8 pins direction.                                 | 0=output, 1=input

Wartości w kolumnach *Args* i *Return* mają format tablicy 8 bitów (bitmapa) gdzie MSB jest po lewej stronie.

Np. wartość 10000000 odpowiada bajtowi 0x80.

## Przykłady

### **pca6408a get in**

Odczytanie stanu wejść:

```
c:\>i2ctool --repeat 3 --delay 100 cp2112 0 100000 dev 0x40 pca6408a get in
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Get inputs: 0b00000111
Wait 100ms for command repetition 1 from 3...
Get inputs: 0b00000111
Wait 100ms for command repetition 2 from 3...
Get inputs: 0b00000111
Wait 100ms for command repetition 3 from 3...
Get inputs: 0b00000111
Close I2C interface instance: 0
Command result: success
```

### **pca6408a get dir**

Odczytanie rejestru *Direction*:

```
c:\>i2ctool cp2112 0 100000 dev 0x40 pca6408a get dir
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Get direction: 0b11111111
Close I2C interface instance: 0
Command result: success
```

Po resecie wszystkie piny są wejściami.

### **pca6408a set dir 00001111**

Zapisanie rejestru *Direction* (piny 7...4 są wyjściami):

```
c:\>i2ctool cp2112 0 100000 dev 0x40 pca6408a set dir 00001111
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Set direction: 0b00001111
Close I2C interface instance: 0
Command result: success
```

### **pca6408a set out 00110000**

Ustawienie wyjść:

```
c:\>i2ctool cp2112 0 100000 dev 0x40 pca6408a set out 00110000
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Set outputs: 0b00110000
Close I2C interface instance: 0
Command result: success
```
