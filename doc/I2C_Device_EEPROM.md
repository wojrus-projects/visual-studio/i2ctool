Obecna wersja programu obsługuje typy EEPROM jak poniżej:

| Name        | Manufacturer      | Size (bytes) | Documentation page
| :---        | :---              | :---         | :---
| 24AA00      | Microchip         | 16           | [Product page](https://www.microchip.com/en-us/product/24AA00)
| 24AA01      | Microchip         | 128          | [Product page](https://www.microchip.com/en-us/product/24AA01)
| 24AA02      | Microchip         | 256          | [Product page](https://www.microchip.com/en-us/product/24AA02)
| 24AA04      | Microchip         | 512          | [Product page](https://www.microchip.com/en-us/product/24AA04)
| 24AA08      | Microchip         | 1 KB         | [Product page](https://www.microchip.com/en-us/product/24AA08)
| 24AA16      | Microchip         | 2 KB         | [Product page](https://www.microchip.com/en-us/product/24AA16)
| 24AA32A     | Microchip         | 4 KB         | [Product page](https://www.microchip.com/en-us/product/24AA32A)
| 24AA64      | Microchip         | 8 KB         | [Product page](https://www.microchip.com/en-us/product/24AA64)
| 24AA128     | Microchip         | 16 KB        | [Product page](https://www.microchip.com/en-us/product/24AA128)
| 24AA256     | Microchip         | 32 KB        | [Product page](https://www.microchip.com/en-us/product/24AA256)
| 24AA512     | Microchip         | 64 KB        | [Product page](https://www.microchip.com/en-us/product/24AA512)
| AT24CM02    | Microchip         | 256 KB       | [Product page](https://www.microchip.com/en-us/product/AT24CM02)
| 24M01       | ST                | 128 KB       | [Product page](https://www.st.com/en/memories/m24m01-r.html)

## Format argumentów CLI

```
i2ctool [OPTIONS] INTERFACE-NAME INTERFACE-INSTANCE BITRATE dev SLAVE-ADDRESS eeprom [--list (OPTIONAL)] -t,--type TYPE -f,--function FUNCTION -a,--address MEMORY-ADDRESS -l,--length MEMORY-LENGTH -d,--data "WRITE-DATA" -b,--byte FILL-BYTE --file FILE-NAME

Where:
  [OPTIONS]             See help (-h, --help).
  INTERFACE-NAME:       I2C interface name.
  INTERFACE-INSTANCE:   I2C interface instance number (0-255, HEX/DEC format).
  BITRATE:              I2C interface bitrate in bps (HEX/DEC format).
  SLAVE-ADDRESS:        I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
  --list                Show supported EEPROMs.
  TYPE:                 EEPROM type (see documentation).
  FUNCTION:             Memory function: rd, wr, fill.
  MEMORY-ADDRESS:       Memory start address (hex or dec).
  MEMORY-LENGTH:        Memory length in bytes (hex or dec).
  WRITE-DATA            Write data (quoted string with bytes in HEX format with optional '0x' prefix and optional separators: whitespaces, commas, colons and dashes).
  FILL-BYTE             Fill value for fill function (hex or dec).
  FILE-NAME             File name for rd/wr functions [OPTIONAL].
```

## Składnia funkcji

```
-t TYPE -f rd   -a MEMORY-ADDRESS -l MEMORY-LENGTH
-t TYPE -f rd   -a MEMORY-ADDRESS -l MEMORY-LENGTH --file FILE-NAME
-t TYPE -f wr   -a MEMORY-ADDRESS -d "WRITE-DATA"
-t TYPE -f wr   -a MEMORY-ADDRESS -l MEMORY-LENGTH --file FILE-NAME
-t TYPE -f fill -a MEMORY-ADDRESS -l MEMORY-LENGTH -b FILL-BYTE
```

## Przykłady

### **Show supported EEPROMs**

```
c:\>i2ctool cp2112 0 100000 dev 0xa0 eeprom --list
Supported EEPROMs:
Microchip    24AA00     16 bytes
Microchip    24AA01     128 bytes
Microchip    24AA02     256 bytes
Microchip    24AA04     512 bytes
Microchip    24AA08     1024 bytes
Microchip    24AA128    16384 bytes
Microchip    24AA16     2048 bytes
Microchip    24AA256    32768 bytes
Microchip    24AA32A    4096 bytes
Microchip    24AA512    65536 bytes
Microchip    24AA64     8192 bytes
Microchip    AT24CM02   262144 bytes
ST           24M01      131072 bytes
```

### **EEPROM read (1)**

```
c:\>i2ctool cp2112 0 100000 dev 0xa0 eeprom -t 24aa01 -f rd -a 0x0 -l 128
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Read 128 byte(s) at 0x00000000 from EEPROM 24AA01 (8-bit address: 0xA0)...

            0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
0x00000000: 31 32 33 AB CD 55 56 57 00 00 00 00 00 00 00 00   | 123..UVW........ |
0x00000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000050: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000060: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000070: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |

<CArray>
0x31, 0x32, 0x33, 0xAB, 0xCD, 0x55, 0x56, 0x57, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
</CArray>

Close I2C interface instance: 0
Command result: success
```

### **EEPROM read (1) to file**

```
c:\>i2ctool cp2112 0 100000 dev 0xa0 eeprom -t 24aa01 -f rd -a 0x0 -l 128 --file d:\dump.bin
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Read 128 byte(s) at 0x00000000 from EEPROM 24AA01 (8-bit address: 0xA0)...
Write data to file: 'd:\dump.bin'
Close I2C interface instance: 0
Command result: success
```

### **EEPROM fill**

```
c:\>i2ctool cp2112 0 100000 dev 0xa0 eeprom -t 24aa01 -f fill -a 0x60 -l 5 -b 0x77
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Fill 5 byte(s) with value 0x77 at 0x00000060 in EEPROM 24AA01 (8-bit address: 0xA0)...
Close I2C interface instance: 0
Command result: success
```

### **EEPROM write**

```
c:\>i2ctool cp2112 0 100000 dev 0xa0 eeprom -t 24aa01 -f wr -a 0x55 -d "0x70,0x71,0x72  0x73-0x74-0x75"
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Write 6 byte(s) at 0x00000055 to EEPROM 24AA01 (8-bit address: 0xA0)...

            0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
0x00000000: 70 71 72 73 74 75 -- -- -- -- -- -- -- -- -- --   | pqrstu---------- |

<CArray>
0x70, 0x71, 0x72, 0x73, 0x74, 0x75
</CArray>

Close I2C interface instance: 0
Command result: success
```

### **EEPROM read (2) to verify fill and write**

```
c:\>i2ctool cp2112 0 100000 dev 0xa0 eeprom -t 24aa01 -f rd -a 0x0 -l 128
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Read 128 byte(s) at 0x00000000 from EEPROM 24AA01 (8-bit address: 0xA0)...

            0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
0x00000000: 31 32 33 AB CD 55 56 57 00 00 00 00 00 00 00 00   | 123..UVW........ |
0x00000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000050: 00 00 00 00 00 70 71 72 73 74 75 00 00 00 00 00   | .....pqrstu..... |
0x00000060: 77 77 77 77 77 00 00 00 00 00 00 00 00 00 00 00   | wwwww........... |
0x00000070: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |

<CArray>
0x31, 0x32, 0x33, 0xAB, 0xCD, 0x55, 0x56, 0x57, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x00, 0x00, 0x00, 0x00, 0x00,
0x77, 0x77, 0x77, 0x77, 0x77, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
</CArray>

Close I2C interface instance: 0
Command result: success
```
