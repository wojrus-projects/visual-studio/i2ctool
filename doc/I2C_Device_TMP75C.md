Dokumentacja techniczna czujnika TMP75C: https://www.ti.com/product/TMP75C

## Format argumentów CLI

```
i2ctool [OPTIONS] INTERFACE-NAME INTERFACE-INSTANCE BITRATE dev SLAVE-ADDRESS tmp75c FUNCTION MODE ARGS

Where:
  [OPTIONS]             See help (-h, --help).
  INTERFACE-NAME:       I2C interface name.
  INTERFACE-INSTANCE:   I2C interface instance number (0-255, HEX/DEC format).
  BITRATE:              I2C interface bitrate in bps (HEX/DEC format).
  SLAVE-ADDRESS:        I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
  FUNCTION:             Device functions: get, set, one-shot.
  MODE:                 Device function modes: temp, limit-l, limit-h, conf.
  ARGS:                 Device function arguments: temperature, configuration.
```

## Funkcje

| Function | Mode    | Args                 | Return                      | Description
| :----    | :----   | :----                | :----                       | :----
| get      | temp    | -                    | Measured temperature in C   | Read temperature.
|          | limit-l | -                    | Alarm low temperature in C  | Read alarm low temperature.
|          | limit-h | -                    | Alarm high temperature in C | Read alarm high temperature.
|          | conf    | -                    | Configuration register      | Read configuration register (see datasheet).
| set      | limit-l | Temperature in C     | -                           | Set alarm low temperature.
|          | limit-h | Temperature in C     | -                           | Set alarm high temperature.
|          | conf    | Configuration string | -                           | Set configuration (quoted string, format as in 'get conf').
| one-shot | -       | -                    | Measured temperature in C   | Start one-shot measurement and return temperature.<br>Require OS=1 in configuration.

All temperatures are in float format.

## Przykłady

### **tmp75c get temp**

Odczyt temperatury:

```
c:\>i2ctool --repeat 3 --delay 100 cp2112 0 100000 dev 0x90 tmp75c get temp
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Temperature: 24.9375 C
Wait 100ms for command repetition 1 from 3...
Temperature: 24.9375 C
Wait 100ms for command repetition 2 from 3...
Temperature: 24.9375 C
Wait 100ms for command repetition 3 from 3...
Temperature: 24.9375 C
Close I2C interface instance: 0
Command result: success
```

### **tmp75c get limit-l**

Odczyt niskiego progu alarmowego temperatury:

```
c:\>i2ctool cp2112 0 100000 dev 0x90 tmp75c get limit-l
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Get temperature limit low: 75 C
Close I2C interface instance: 0
Command result: success
```

### **tmp75c set limit-l**

Ustawienie niskiego progu alarmowego temperatury:

```
c:\>i2ctool cp2112 0 100000 dev 0x90 tmp75c set limit-l -10.5
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Set temperature limit low: -10.5 C
Close I2C interface instance: 0
Command result: success
```

### **tmp75c get conf**

Odczyt konfiguracji:

```
c:\>i2ctool cp2112 0 100000 dev 0x90 tmp75c get conf
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Configuration: SD: 0, TM: 0, POL: 0, FQ: 0, OS: 0
Close I2C interface instance: 0
Command result: success
```

Oznaczenia parametrów i ich wartości są zgodne z datasheet.

### **tmp75c set conf**

Ustawienie konfiguracji (format jak w 'get conf'):

```
c:\>i2ctool cp2112 0 100000 dev 0x90 tmp75c set conf "SD: 0, TM: 0, POL: 0, FQ: 0, OS: 1"
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Set configuration: SD: 0, TM: 0, POL: 0, FQ: 0, OS: 1
Close I2C interface instance: 0
Command result: success
```

### **tmp75c one-shot**

Wykonanie jednego pomiaru temperatury i odczytanie wyniku (wymagana jest opcja OS=1 w konfiguracji):

```
c:\>i2ctool --repeat 3 --delay 3000 cp2112 0 100000 dev 0x90 tmp75c one-shot
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Temperature: 24.3125 C
Wait 3000ms for command repetition 1 from 3...
Temperature: 24.5 C
Wait 3000ms for command repetition 2 from 3...
Temperature: 28.0625 C
Wait 3000ms for command repetition 3 from 3...
Temperature: 29.4375 C
Close I2C interface instance: 0
Command result: success
```
