Dokumentacja techniczna generatora zegarowego Si5351: [https://www.skyworksinc.com/.../si5351a-b-gt](https://www.skyworksinc.com/en/Products/Timing/CMOS-Clock-Generators/si5351a-b-gt)

Obecna wersja programu obsługuje Si5351A-B-GT (XTAL 25 MHz only device, MSOP-10).

Obsługiwane przykładowe urządzenia/moduły:

- [SI5351A_EVB_V1_0](https://gitlab.com/wojrus-projects/altium-designer/evb/si5351a_evb_v1_0)
- [Mikroe Clock Gen Click](https://www.mikroe.com/clock-gen-click)
- [Adafruit 2045](https://www.adafruit.com/product/2045)

## Format argumentów CLI

```
i2ctool [OPTIONS] INTERFACE-NAME INTERFACE-INSTANCE BITRATE dev SLAVE-ADDRESS si5351 --init -o,--output OUTPUT={0,2} -e,--enable ENABLE={0,1}  -i,--invert INVERT={0,1} -s,--strength STRENGTH={0,1,2,3} -f,--frequency FREQUENCY

Where:
  [OPTIONS]             See help (-h, --help).
  INTERFACE-NAME:       I2C interface name.
  INTERFACE-INSTANCE:   I2C interface instance number (0-255, HEX/DEC format).
  BITRATE:              I2C interface bitrate in bps (HEX/DEC format).
  SLAVE-ADDRESS:        I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
  --init                Initialize registers [REQUIRED after reset].
  OUTPUT                Select output: 0, 2 (1 is not supported) [REQUIRED].
  ENABLE                Enable/disable output: 0=off, 1=on.
  INVERT                Invert output: 0=not inverted, 1=inverted.
  STRENGTH              Set output drive strength: 0=2mA, 1=4mA, 2=6mA, 3=8mA.
  FREQUENCY             Set output frequency in Hz: 8 kHz to 160 MHz.
```

## Przykłady

### **Initialize (required after reset)**

```
c:\>i2ctool cp2112 0 100000 dev 0xc0 si5351 --init
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Initialize Si5351
Close I2C interface instance: 0
Execution time: 66ms
Command result: success
```

### **Set output CLK0**

```
c:\>i2ctool cp2112 0 100000 dev 0xc0 si5351 -o 0 -e 1 -f 2000000 -s 0 -i 0
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Enable output: 0, state: 1
Set output: 0, frequency: 2000000 Hz, strength: 0, invert: 0
Close I2C interface instance: 0
Execution time: 101ms
Command result: success
```
