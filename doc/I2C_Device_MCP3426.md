Dokumentacja techniczna ADC MCP3426/7/8: https://www.microchip.com/en-us/product/MCP3426

```
i2ctool [OPTIONS] INTERFACE-NAME INTERFACE-INSTANCE BITRATE dev SLAVE-ADDRESS mcp3426 FUNCTION MODE ARGS

Where:
  [OPTIONS]             See help (-h, --help).
  INTERFACE-NAME:       I2C interface name.
  INTERFACE-INSTANCE:   I2C interface instance number (0-255, HEX/DEC format).
  BITRATE:              I2C interface bitrate in bps (HEX/DEC format).
  SLAVE-ADDRESS:        I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
  FUNCTION:             Device functions: get, set.
  MODE:                 Device function modes: adc, conf.
  ARGS:                 Device function arguments: configuration.
```

## Funkcje

| Function   | Mode    | Args                 | Return                             | Description
| :----      | :----   | :----                | :----                              | :----
| get        | adc     | -                    | Configuration, raw result, voltage | Read configuration, raw ADC 16-bit value (hex/bin)<br>and input voltage in V.
| set        | conf    | Configuration string | -                                  | Set configuration (quoted string, format as in 'get adc').<br>For parameter values see datasheet.
| call-reset | -       | -                    | -                                  | ADC will perform internal reset similar to a Power-On-Reset (POR).
| call-latch | -       | -                    | -                                  | ADC will latch the logic status of the external address selection pins (Adr0 and Adr1 pins).
| call-start | -       | -                    | -                                  | All devices on the bus initiate a conversion simultaneously.

## Przykłady

### **mcp3426 get adc**

Odczyt ADC (tryb 16-bit):

```
c:\>i2ctool --repeat 3 --delay 100 cp2112 0 100000 dev 0xd0 mcp3426 get adc
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Configuration: 'gain: 0, sr: 2, oc: 1, ch: 0, rdy: 0', data: 0xA22D / 0b1010001000101101, voltage: -1.5011876 V
Wait 100ms for command repetition 1 from 3...
Configuration: 'gain: 0, sr: 2, oc: 1, ch: 0, rdy: 0', data: 0xA22E / 0b1010001000101110, voltage: -1.5011251 V
Wait 100ms for command repetition 2 from 3...
Configuration: 'gain: 0, sr: 2, oc: 1, ch: 0, rdy: 0', data: 0xA22D / 0b1010001000101101, voltage: -1.5011876 V
Wait 100ms for command repetition 3 from 3...
Configuration: 'gain: 0, sr: 2, oc: 1, ch: 0, rdy: 0', data: 0xA22E / 0b1010001000101110, voltage: -1.5011251 V
Close I2C interface instance: 0
Command result: success
```

### **mcp3426 set conf**

Zapisanie konfiguracji:

```
c:\>i2ctool cp2112 0 100000 dev 0xd0 mcp3426 set conf "gain: 0, sr: 2, oc: 1, ch: 0, rdy: 0"
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Set configuration: gain: 0, sr: 2, oc: 1, ch: 0, rdy: 0
Close I2C interface instance: 0
Command result: success
```
