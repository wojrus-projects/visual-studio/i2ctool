# Specyfikacja

Program *I2CTool* jest uniwersalnym narzędziem CLI do niskopoziomowych testów i diagnostyki urządzeń I2C slave. Funkcjonalność jest podobna do pakietu *i2c-tools* w Linuksie. Podstawowe funkcje/rozkazy:

- **detect** - wyszukanie adresów slave podłączonych do magistrali I2C.
- **wr (write)** - zapisanie od 1 do N bajtów do slave.
- **rd (read)** - odczytanie od 1 do N bajtów ze slave.
- **rda (read with address)** - odczytanie od 1 do N bajtów ze slave z możliwością podania adresu początkowego pamięci np. EEPROM.
- **dev (device)** - wysokopoziomowa obsługa konkretnych urządzeń slave.

Platforma: Windows 10/11

Toolchain: Visual Studio Community 2022 v17.10.00

Język: C++ 2020 latest

Instalacja: nie jest wymagana. Interfejsy CP2112/FT260 nie wymagają sterowników (używany jest protokół USB-HID).

# Interfejsy I2C

Obecna wersja programu obsługuje następujące typy interfejsów I2C:
- [Silabs CP2112](https://www.silabs.com/interface/usb-bridges/classic/device.cp2112?tab=specs) m.in. w modułach:
  - [CP2112EK development kit](https://www.silabs.com/development-tools/interface/cp2112ek-development-kit)
  - [CP2112_EVB_V1_0 custom interface](https://gitlab.com/wojrus-projects/altium-designer/evb/cp2112_evb_v1_0)
- [FTDI FT260x](https://ftdichip.com/products/ft260q) m.in. w module:
  - [UMFT260EV1A DIP development module](https://ftdichip.com/products/umft260ev1a)
  
Parametry interfejsów:
  
| Parametr          | CP2112      | FT260                                       |
| :----             | :----:      | :----:                                      |
| Bit rate bps min. | 1           | 60'000                                      |
| Bit rate bps max. | 400'000     | 3'400'000                                   |
| Write bytes min.  | 1           | 1                                           |
| Write bytes max.  | 61          | 65'535                                      |
| Read bytes min.   | 1           | 1                                           |
| Read bytes max.   | 512         | 65'535                                      |
| Timeouts          | -           | Read-only                                   |
| Clock stretching  | yes         | yes                                         |
| Pin SCL           | pin 24      | DIO5 (GPIO0)<br>QFN: pin 12, TSSOP: pin 16  |
| Pin SDA           | pin 1       | DIO6 (GPIO1)<br>QFN: pin 13, TSSOP: pin 17  |
| LEDs I2C Rx/Tx    | yes         | no                                          |

# Pomoc

Program uruchomiony z argumentem *"-h" lub "--help"* zwróci pomoc podstawową:

```
c:\>i2ctool -h
I2C bus universal diagnostic CLI tool v1.7 (Jan  6 2023, 12:46:04)
Author: W.R. <rwxrwx@interia.pl>
License: MIT
Supported I2C masters: Silabs CP2112, FTDI FT260.

Usage: i2ctool [OPTIONS] INTERFACE-NAME INTERFACE-INSTANCE BITRATE SUBCOMMAND

Positionals:
  INTERFACE-NAME         I2C interface name (CP2112 or FT260).
  INTERFACE-INSTANCE     I2C interface instance number (0-255, HEX/DEC format).
  BITRATE                I2C interface bitrate in bps (HEX/DEC format).

Options:
  -h,--help              Print this help message and exit
  --help-all             Expand all help
  -t,--timeout           I2C transfer timeout in seconds (1-60, HEX/DEC format).
  --repeat               Command repeat counter.
  --delay                Command repeat delay in ms.

Subcommands:
  detect                 Detect slave devices connected to I2C bus (by ACK check at write or read address).
  wr                     Write data to I2C slave.
  rd                     Read data from I2C slave (without memory address).
  rda                    Read data from I2C slave (with memory address).
  dev                    High level functions for various I2C slave devices.
```

Agument *"--help-all"* zwróci pomoc rozszerzoną:

```
c:\>i2ctool --help-all
I2C bus universal diagnostic CLI tool v1.7 (Jan  6 2023, 12:46:04)
Author: W.R. <rwxrwx@interia.pl>
License: MIT
Supported I2C masters: Silabs CP2112, FTDI FT260.

Usage: i2ctool [OPTIONS] INTERFACE-NAME INTERFACE-INSTANCE BITRATE SUBCOMMAND

Positionals:
  INTERFACE-NAME         I2C interface name (CP2112 or FT260).
  INTERFACE-INSTANCE     I2C interface instance number (0-255, HEX/DEC format).
  BITRATE                I2C interface bitrate in bps (HEX/DEC format).

Options:
  -h,--help              Print this help message and exit
  --help-all             Expand all help
  -t,--timeout           I2C transfer timeout in seconds (1-60, HEX/DEC format).
  --repeat               Command repeat counter.
  --delay                Command repeat delay in ms.

Subcommands:
detect
  Detect slave devices connected to I2C bus (by ACK check at write or read address).

wr
  Write data to I2C slave.
  Positionals:
    SLAVE-ADDRESS          I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
    DATA                   Write data (quoted string with bytes in HEX format with optional '0x' prefix and optional separators: whitespaces, commas, colons and dashes).

rd
  Read data from I2C slave (without memory address).
  Positionals:
    SLAVE-ADDRESS          I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
    READ-LENGTH            Data read length (HEX/DEC format).

rda
  Read data from I2C slave (with memory address).
  Positionals:
    SLAVE-ADDRESS          I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
    READ-LENGTH            Data read length (HEX/DEC format).
    MEMORY-ADDRESS-LENGTH  Memory Address Length (1-4 bytes, HEX/DEC format).
    MEMORY-ADDRESS         Memory Address (0x00000000-0xFFFFFFFF, HEX/DEC format, send MSB first).

dev
  High level functions for various I2C slave devices.
  Positionals:
    SLAVE-ADDRESS          I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
  Subcommands:
    TMP75C                 FUNCTION={get,set,one-shot} MODE={temp,limit-l,limit-h,conf} ARGS={temperature,configuration}.
    MCP3426                FUNCTION={get,set} MODE={adc,conf} ARGS={configuration}.
    PCA6408A               FUNCTION={get,set} MODE={in,out,polarity,dir} ARGS={value}.
    EEPROM                 --list -t TYPE -f FUNCTION={rd,wr,fill} -a MEMORY-ADDRESS -l MEMORY-LENGTH -d "WRITE-DATA" --file FILE -b FILL BYTE.
    Si5351                 --init -o OUTPUT -e ENABLE -i INVERT -s STRENGTH -f FREQUENCY.
```

# Rozkaz 'detect'

Składnia:
```
i2ctool INTERFACE-NAME INTERFACE-INSTANCE BITRATE detect

Where:
  INTERFACE-NAME:       I2C interface name (CP2112 or FT260).
  INTERFACE-INSTANCE:   I2C interface instance number (0-255, HEX/DEC format).
  BITRATE:              I2C interface bitrate in bps (HEX/DEC format).
```

Program próbuje odczytać lub zapisać 1 bajt z/do slave w pętli dla 127 kolejnych adresów (8-bit: 0x02...0xFE (parzyste) / 7-bit: 0x01...0x7F):
```
Step 1:
  [START][ADDRESS+R][xx][STOP]

Step 2 (optional, only if step 1 return NACK):
  [START][ADDRESS+W][00][STOP]
```

Gdy slave zwróci ACK przy zapisie adresu to dany adres jest dodany do listy wykrytych urządzeń I2C slave.

Przykład (układy PCF8566 + 24AA01):

```
c:\>i2ctool cp2112 0 100000 detect
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Detect I2C slaves...
Found 9 slave(s)
Address list:
+-----+-------------------------+-------------------------+
| Idx |        7-bit            |        8-bit            |
+-----+-------------------------+-------------------------+
| 0   | 0x3E - 0b00111110 - 62  | 0x7C - 0b01111100 - 124 |
| 1   | 0x50 - 0b01010000 - 80  | 0xA0 - 0b10100000 - 160 |
| 2   | 0x51 - 0b01010001 - 81  | 0xA2 - 0b10100010 - 162 |
| 3   | 0x52 - 0b01010010 - 82  | 0xA4 - 0b10100100 - 164 |
| 4   | 0x53 - 0b01010011 - 83  | 0xA6 - 0b10100110 - 166 |
| 5   | 0x54 - 0b01010100 - 84  | 0xA8 - 0b10101000 - 168 |
| 6   | 0x55 - 0b01010101 - 85  | 0xAA - 0b10101010 - 170 |
| 7   | 0x56 - 0b01010110 - 86  | 0xAC - 0b10101100 - 172 |
| 8   | 0x57 - 0b01010111 - 87  | 0xAE - 0b10101110 - 174 |
+-----+-------------------------+-------------------------+

Address list (7-bit):
<CArray>
0x3E, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57
</CArray>

Address list (8-bit):
<CArray>
0x7C, 0xA0, 0xA2, 0xA4, 0xA6, 0xA8, 0xAA, 0xAC, 0xAE
</CArray>

Close I2C interface instance: 0
Command result: success
```

Wyniki zawarte w tagu `<CArray>` są w formacie zgodnym z językami C, Python itp.

Tag służy do parsowania wyników za pomocą skryptów lub do kopiowania danych CTRL+C/V.

# Rozkaz 'wr'

Składnia:
```
i2ctool INTERFACE-NAME INTERFACE-INSTANCE BITRATE wr SLAVE-ADDRESS "DATA"

Where:
  INTERFACE-NAME:       I2C interface name (CP2112 or FT260).
  INTERFACE-INSTANCE:   I2C interface instance number (0-255, HEX/DEC format).
  BITRATE:              I2C interface bitrate in bps (HEX/DEC format).
  SLAVE-ADDRESS:        I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
  "DATA":               Write data (quoted string with bytes in HEX format with optional '0x' prefix and optional separators: whitespaces, commas, colons and dashes).
```

Rozkaz zapisuje do slave ciąg 1...N bajtów:

```
[START][ADDRESS+W][BYTE-0][BYTE-1]...[BYTE-(N-1)][STOP]
```

Bajty muszą być podane w cudzysłowie w postaci HEX z opcjonalnymi prefiksami '0x' oraz z opcjonalnymi separatorami (spacja, przecinek, dwukropek, minus).

Przykład:

```
c:\>i2ctool cp2112 0 100000 wr 0xA0 "0x00, 0x31, 0x32, 0x33 AB:CD 55-56-57"
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Write 9 byte(s) to I2C slave (8-bit address: 0xA0)...

            0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
0x00000000: 00 31 32 33 AB CD 55 56 57 -- -- -- -- -- -- --   | .123..UVW------- |

<CArray>
0x00, 0x31, 0x32, 0x33, 0xAB, 0xCD, 0x55, 0x56, 0x57
</CArray>

Close I2C interface instance: 0
Command result: success
```

# Rozkaz 'rd'

Składnia:
```
i2ctool --timeout 5 INTERFACE-NAME INTERFACE-INSTANCE BITRATE rd SLAVE-ADDRESS READ-LENGTH

Where:
  --timeout:            I2C transfer timeout in seconds (1-60, HEX/DEC format) [OPTIONAL].
  INTERFACE-NAME:       I2C interface name (CP2112 or FT260).
  INTERFACE-INSTANCE:   I2C interface instance number (0-255, HEX/DEC format).
  BITRATE:              I2C interface bitrate in bps (HEX/DEC format).
  SLAVE-ADDRESS:        I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
  READ-LENGTH:          Data read length (HEX/DEC format).
```

Rozkaz odczytuje ze slave 1...N bajtów:

```
[START][ADDRESS+R][BYTE-0][BYTE-1]...[BYTE-(N-1)][STOP]
```

Przykład:

```
c:\>i2ctool cp2112 0 100000 rd 0xA0 25
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Read 25 byte(s) from I2C slave (8-bit address: 0xA0)...

            0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
0x00000000: 31 32 33 AB CD 55 56 57 00 00 00 00 00 00 00 00   | 123..UVW........ |
0x00000010: 00 00 00 00 00 00 00 00 00 -- -- -- -- -- -- --   | .........------- |

<CArray>
0x31, 0x32, 0x33, 0xAB, 0xCD, 0x55, 0x56, 0x57, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
</CArray>

Close I2C interface instance: 0
Command result: success
```

# Rozkaz 'rda'

Składnia:
```
i2ctool --timeout 5 INTERFACE-NAME INTERFACE-INSTANCE BITRATE rd SLAVE-ADDRESS READ-LENGTH MEMORY-ADDRESS-LENGTH MEMORY-ADDRESS

Where:
  --timeout:                I2C transfer timeout in seconds (1-60, HEX/DEC format) [OPTIONAL].
  INTERFACE-NAME:           I2C interface name (CP2112 or FT260).
  INTERFACE-INSTANCE:       I2C interface instance number (0-255, HEX/DEC format).
  BITRATE:                  I2C interface bitrate in bps (HEX/DEC format).
  SLAVE-ADDRESS:            I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
  READ-LENGTH:              Data read length (HEX/DEC format).
  MEMORY-ADDRESS-LENGTH:    Memory Address Length (1-4 bytes, HEX/DEC format).
  MEMORY-ADDRESS:           Memory Address (0x00000000-0xFFFFFFFF, HEX/DEC format, send MSB first).
```

Rozkaz odczytuje ze slave 1...N bajtów od zadanego adresu początkowego pamięci np. EEPROM:

```
CP2112: [START][ADDRESS+W][MEMORY-ADDRESS byte(s)][RESTART][ADDRESS+R][BYTE-0][BYTE-1]...[BYTE-(N-1)][STOP]
FT260:  [START][ADDRESS+W][MEMORY-ADDRESS byte(s)][STOP][START][ADDRESS+R][BYTE-0][BYTE-1]...[BYTE-(N-1)][STOP]
```

Adres pamięci (argument *MEMORY-ADDRESS*) może mieć rozmiar 1...4 bajtów (argument *MEMORY-ADDRESS-LENGTH*).

Gdy *MEMORY-ADDRESS-LENGTH* = 2, 3, 4 to wtedy adres pamięci wysyłany jest zaczynając od bajta MSB.

Przykład:

```
c:\>i2ctool cp2112 0 100000 rda 0xA0 50 1 0x00
Open I2C interface CP2112 instance: 0
Set SCL frequency: 100000 Hz
Read 50 byte(s) from I2C slave (8-bit address: 0xA0, MAL: 1 byte(s), MA: 0x00000000)...

            0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
0x00000000: 31 32 33 AB CD 55 56 57 00 00 00 00 00 00 00 00   | 123..UVW........ |
0x00000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   | ................ |
0x00000030: 00 00 -- -- -- -- -- -- -- -- -- -- -- -- -- --   | ..-------------- |

<CArray>
0x31, 0x32, 0x33, 0xAB, 0xCD, 0x55, 0x56, 0x57, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00
</CArray>

Close I2C interface instance: 0
Command result: success
```

W przykładzie ramka I2C może mieć różną postać, zależną od typu interfejsu:

```
CP2112: [START][0xA0][0x00][RESTART][0xA1][BYTE-0][BYTE-1]...[BYTE-49][STOP]
FT260:  [START][0xA0][0x00][STOP][START][0xA1][BYTE-0][BYTE-1]...[BYTE-49][STOP]
```

# Rozkaz 'dev'

Składnia:
```
i2ctool INTERFACE-NAME INTERFACE-INSTANCE BITRATE dev SLAVE-ADDRESS DEVICE-NAME [DEVICE-ARGS (OPTIONAL)]

Where:
  INTERFACE-NAME:       I2C interface name (CP2112 or FT260).
  INTERFACE-INSTANCE:   I2C interface instance number (0-255, HEX/DEC format).
  BITRATE:              I2C interface bitrate in bps (HEX/DEC format).
  SLAVE-ADDRESS:        I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).
  DEVICE-NAME:          I2C slave name (TMP75C, MCP3426, PCA6408A, Si5351, EEPROM).
  DEVICE-ARGS           Optional arguments for specific slave (see documentation below for details).
```

Rozkaz służy do wysokopoziomowej obsługi poniższych urządzeń I2C slave:

| Name        | Manufacturer      | Description           | Documentation page
| :---        | :---              | :---                  | :---
| TMP75C      | Texas Instruments | Czujnik temperatury   | [TMP75C](doc/I2C_Device_TMP75C.md)
| MCP3426/7/8 | Microchip         | ADC 16-bit            | [MCP3426/7/8](doc/I2C_Device_MCP3426.md)
| PCA6408A    | NXP               | Ekspander GPIO 8-bit  | [PCA6408A](doc/I2C_Device_PCA6408A.md)
| Si5351      | Skyworks          | Generator zegarowy    | [Si5351](doc/I2C_Device_Si5351.md)
| EEPROM      | Microchip, ST     | EEPROM                | [EEPROM](doc/I2C_Device_EEPROM.md)

# Licencja

MIT

# Support

No merge requests. If you want to report a bug, create an issue.
