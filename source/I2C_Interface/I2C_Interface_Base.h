#pragma once

#include <cstdint>
#include <span>
#include <utility>
#include <string>
#include <chrono>


namespace I2C_Interface
{
    // Slave address 8-bit range:
    inline constexpr uint8_t SLAVE_ADDRESS_MIN = 0x02;
    inline constexpr uint8_t SLAVE_ADDRESS_MAX = 0xFE;


    enum class I2C_Interface_Status
    {
        Success,
        UnknownError,
        InvalidArgument,
        USBError,
        DeviceError,
        I2CBusy,
        I2CTimeout,
        I2CNack,
        I2CArbitrationLost,
        I2CTransferIncomplete
    };


    std::string toString(I2C_Interface_Status status);


    class I2C_Interface_Base
    {
    public:
        I2C_Interface_Base(std::string name)
            : name{ name }
        {
        }

        virtual ~I2C_Interface_Base() = default;

        virtual I2C_Interface_Status open(int instanceNumber) = 0;
        virtual I2C_Interface_Status close() = 0;
        virtual I2C_Interface_Status setBitRate(uint32_t bitRate_bps) = 0;
        virtual I2C_Interface_Status write(uint8_t slaveAddress, const uint8_t* pData, size_t dataLength) = 0;
        virtual I2C_Interface_Status write(uint8_t slaveAddress, std::span<const uint8_t> data) = 0;
        virtual I2C_Interface_Status read(uint8_t slaveAddress, uint8_t* pData, size_t dataLength, uint32_t memoryAddress = 0, uint32_t memoryAddressSize = 0) = 0;

        virtual std::pair<uint32_t, uint32_t> getBitRateRange() = 0;
        virtual std::pair<uint32_t, uint32_t> getWriteLengthRange() = 0;
        virtual std::pair<uint32_t, uint32_t> getReadLengthRange() = 0;

        std::string getName()
        {
            return name;
        }

        void setTimeout(std::chrono::seconds timeout)
        {
            this->timeout = timeout;
        }

    protected:
        std::string name;
        std::chrono::seconds timeout{};
    };
} // namespace I2C_Interface
