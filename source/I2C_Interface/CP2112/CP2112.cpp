#include "CP2112.h"

#include <algorithm>


#ifndef NDEBUG
    #include <iostream>
    #include <format>

    #define CP2112_LOG(...)       std::cout << std::format(__VA_ARGS__)
#else
    #define CP2112_LOG(...)
#endif


CP2112::~CP2112()
{
    if (pDeviceHandler != nullptr)
    {
        bool openState;

        if (isOpen(openState) == Status::Success)
        {
            if (openState != false)
            {
                close();
            }
        }
    }
}


CP2112::Status CP2112::open(int deviceNumber)
{
    HID_SMBUS_STATUS hidSmbusStatus;
    int connectedDeviceNumber;

    if (pDeviceHandler != nullptr)
    {
        return Status::UnknownError;
    }

    connectedDeviceNumber = countDevices();
    if (connectedDeviceNumber < 1)
    {
        return Status::USBDeviceError;
    }

    if ((deviceNumber < 0) || (deviceNumber > (connectedDeviceNumber - 1)))
    {
        return Status::InvalidArgument;
    }

    hidSmbusStatus = HidSmbus_Open(&pDeviceHandler, (DWORD)deviceNumber, VID, PID);
    CP2112_LOG("HidSmbus_Open(): {} (deviceNumber={})\n", hidSmbusStatus, deviceNumber);

    return convertStatus(hidSmbusStatus);
}


CP2112::Status CP2112::close()
{
    Status status;
    HID_SMBUS_STATUS hidSmbusStatus;

    if (pDeviceHandler == nullptr)
    {
        return Status::Success;
    }

    hidSmbusStatus = HidSmbus_Close(pDeviceHandler);
    CP2112_LOG("HidSmbus_Close(): {}\n", hidSmbusStatus);
    status = convertStatus(hidSmbusStatus);
    if (status == Status::Success)
    {
        pDeviceHandler = nullptr;
    }

    return status;
}


CP2112::Status CP2112::isOpen(bool& openState)
{
    Status status;
    HID_SMBUS_STATUS hidSmbusStatus;
    BOOL isOpened = FALSE;

    hidSmbusStatus = HidSmbus_IsOpened(pDeviceHandler, &isOpened);
    CP2112_LOG("HidSmbus_IsOpened(): {} (isOpened={})\n", hidSmbusStatus, isOpened);
    status = convertStatus(hidSmbusStatus);
    if (status == Status::Success)
    {
        openState = (isOpened == TRUE);
    }

    return status;
}


CP2112::Status CP2112::setConfiguration(uint32_t bitRate, uint32_t transferTimeout, uint32_t responseTimeout)
{
    Status status;
    HID_SMBUS_STATUS hidSmbusStatus;

    bitRate         = std::clamp(bitRate, uint32_t(HID_SMBUS_MIN_BIT_RATE), 400'000u);
    transferTimeout = std::clamp(transferTimeout, 1u, uint32_t(HID_SMBUS_MAX_TIMEOUT));
    responseTimeout = std::clamp(responseTimeout, 1u, 10'000u);

    if (responseTimeout < transferTimeout)
    {
        return Status::InvalidArgument;
    }

    const BYTE address         = HID_SMBUS_MIN_ADDRESS;
    const BOOL autoReadRespond = FALSE;
    const WORD writeTimeout    = (WORD)transferTimeout;
    const WORD readTimeout     = (WORD)transferTimeout;
    const BOOL sclLowTimeout   = TRUE;
    const WORD transferRetries = 1;

    hidSmbusStatus = HidSmbus_SetSmbusConfig(pDeviceHandler, (DWORD)bitRate, address, autoReadRespond, writeTimeout, readTimeout, sclLowTimeout, transferRetries);
    CP2112_LOG("HidSmbus_SetSmbusConfig(): {} (bitRate={} [Hz], transferTimeout={} [ms])\n", hidSmbusStatus, bitRate, transferTimeout);
    status = convertStatus(hidSmbusStatus);
    if (status != Status::Success)
    {
        return status;
    }

    hidSmbusStatus = HidSmbus_SetTimeouts(pDeviceHandler, (DWORD)responseTimeout);
    CP2112_LOG("HidSmbus_SetTimeouts(): {} (responseTimeout={} [ms])\n", hidSmbusStatus, responseTimeout);

    return convertStatus(hidSmbusStatus);
}


CP2112::Status CP2112::write(uint8_t slaveAddress, const uint8_t* pData, size_t dataLength)
{
    Status status;
    HID_SMBUS_STATUS hidSmbusStatus;
    bool openState;

    if (slaveAddress < HID_SMBUS_MIN_ADDRESS || slaveAddress > HID_SMBUS_MAX_ADDRESS)
    {
        return Status::InvalidArgument;
    }

    if (dataLength < HID_SMBUS_MIN_WRITE_REQUEST_SIZE)
    {
        return Status::InvalidArgument;
    }
    else if (dataLength > HID_SMBUS_MAX_WRITE_REQUEST_SIZE)
    {
        dataLength = HID_SMBUS_MAX_WRITE_REQUEST_SIZE;
    }

    status = isOpen(openState);
    if (status != Status::Success)
    {
        return status;
    }

    if (openState != true)
    {
        return Status::USBDeviceError;
    }

    hidSmbusStatus = HidSmbus_WriteRequest(pDeviceHandler, (BYTE)slaveAddress, (BYTE*)pData, (BYTE)dataLength);
    CP2112_LOG("HidSmbus_WriteRequest(): {} (slaveAddress=0x{:02X}, dataLength={})\n", hidSmbusStatus, slaveAddress, dataLength);
    status = convertStatus(hidSmbusStatus);
    if (status != Status::Success)
    {
        return status;
    }

    HID_SMBUS_S0 status0;
    HID_SMBUS_S1 status1;
    WORD numRetries;
    WORD bytesRead;

    do
    {
        hidSmbusStatus = HidSmbus_TransferStatusRequest(pDeviceHandler);
        CP2112_LOG("HidSmbus_TransferStatusRequest(): {}\n", hidSmbusStatus);
        status = convertStatus(hidSmbusStatus);
        if (status != Status::Success)
        {
            return status;
        }

        hidSmbusStatus = HidSmbus_GetTransferStatusResponse(pDeviceHandler, &status0, &status1, &numRetries, &bytesRead);
        CP2112_LOG("HidSmbus_GetTransferStatusResponse(): {} (status0={}, status1={}, numRetries={}, bytesRead={})\n",
            hidSmbusStatus, status0, status1, numRetries, bytesRead);
        status = convertStatus(hidSmbusStatus);
        if (status != Status::Success)
        {
            return status;
        }
    }
    while (status0 == HID_SMBUS_S0_BUSY);

    return convertStatus(hidSmbusStatus, status0, status1);
}


CP2112::Status CP2112::write(uint8_t slaveAddress, std::span<const uint8_t> data)
{
    return write(slaveAddress, data.data(), data.size());
}


CP2112::Status CP2112::read(uint8_t slaveAddress, uint8_t* pData, size_t dataLength, uint32_t memoryAddress, uint32_t memoryAddressSize)
{
    Status status;
    HID_SMBUS_STATUS hidSmbusStatus;
    bool openState;
    const bool enableMemoryMode = (memoryAddressSize > 0);

    if (slaveAddress < HID_SMBUS_MIN_ADDRESS || slaveAddress > HID_SMBUS_MAX_ADDRESS)
    {
        return Status::InvalidArgument;
    }

    if (dataLength < HID_SMBUS_MIN_READ_REQUEST_SIZE)
    {
        return Status::InvalidArgument;
    }
    else if (dataLength > HID_SMBUS_MAX_READ_REQUEST_SIZE)
    {
        dataLength = HID_SMBUS_MAX_READ_REQUEST_SIZE;
    }

    if (enableMemoryMode == true)
    {
        // Limit rozmiaru adresu pamięci do 1...4 bajtów.
        if (memoryAddressSize < HID_SMBUS_MIN_TARGET_ADDRESS_SIZE || memoryAddressSize > (HID_SMBUS_MIN_TARGET_ADDRESS_SIZE + 3))
        {
            return Status::InvalidArgument;
        }
    }

    status = isOpen(openState);
    if (status != Status::Success)
    {
        return status;
    }

    if (openState != true)
    {
        return Status::USBDeviceError;
    }

    if (enableMemoryMode == true)
    {
        hidSmbusStatus = HidSmbus_AddressReadRequest(pDeviceHandler, slaveAddress, (WORD)dataLength, (BYTE)memoryAddressSize, (BYTE*)&memoryAddress);
        CP2112_LOG("HidSmbus_AddressReadRequest(): {} (slaveAddress=0x{:02X}, dataLength={}, memoryAddressSize={}, memoryAddress=0x{:08X})\n",
            hidSmbusStatus, slaveAddress, dataLength, memoryAddressSize, memoryAddress);
        status = convertStatus(hidSmbusStatus);
        if (status != Status::Success)
        {
            return status;
        }
    }
    else
    {
        hidSmbusStatus = HidSmbus_ReadRequest(pDeviceHandler, slaveAddress, (WORD)dataLength);
        CP2112_LOG("HidSmbus_ReadRequest(): {} (slaveAddress=0x{:02X}, dataLength={})\n", hidSmbusStatus, slaveAddress, dataLength);
        status = convertStatus(hidSmbusStatus);
        if (status != Status::Success)
        {
            return status;
        }
    }

    HID_SMBUS_S0 status0;
    HID_SMBUS_S1 status1;
    WORD numRetries;
    WORD bytesRead;

    do
    {
        hidSmbusStatus = HidSmbus_TransferStatusRequest(pDeviceHandler);
        CP2112_LOG("HidSmbus_TransferStatusRequest(): {}\n", hidSmbusStatus);
        status = convertStatus(hidSmbusStatus);
        if (status != Status::Success)
        {
            return status;
        }

        hidSmbusStatus = HidSmbus_GetTransferStatusResponse(pDeviceHandler, &status0, &status1, &numRetries, &bytesRead);
        CP2112_LOG("HidSmbus_GetTransferStatusResponse(): {} (status0={}, status1={}, numRetries={}, bytesRead={})\n",
            hidSmbusStatus, status0, status1, numRetries, bytesRead);
        status = convertStatus(hidSmbusStatus);
        if (status != Status::Success)
        {
            return status;
        }
    }
    while (status0 == HID_SMBUS_S0_BUSY);

    status = convertStatus(hidSmbusStatus, status0, status1);
    if (status != Status::Success)
    {
        return status;
    }

    hidSmbusStatus = HidSmbus_ForceReadResponse(pDeviceHandler, (WORD)dataLength);
    CP2112_LOG("HidSmbus_ForceReadResponse(): {} (data_length={})\n", hidSmbusStatus, dataLength);
    status = convertStatus(hidSmbusStatus);
    if (status != Status::Success)
    {
        return status;
    }

    BYTE responseBuffer[HID_SMBUS_MAX_READ_RESPONSE_SIZE];
    size_t readIndex = 0;

    while (readIndex < dataLength)
    {
        BYTE readResponseLength = 0;

        hidSmbusStatus = HidSmbus_GetReadResponse(pDeviceHandler, &status0, responseBuffer, HID_SMBUS_MAX_READ_RESPONSE_SIZE, &readResponseLength);
        CP2112_LOG("HidSmbus_GetReadResponse(): {} (status0={}, readResponseLength={}, readIndex={})\n",
            hidSmbusStatus, status0, readResponseLength, readIndex);
        status = convertStatus(hidSmbusStatus);
        if (status != Status::Success)
        {
            return status;
        }

        if (!(status0 == HID_SMBUS_S0_IDLE
           || status0 == HID_SMBUS_S0_BUSY
           || status0 == HID_SMBUS_S0_COMPLETE))
        {
            return Status::SMBusReadIncomplete;
        }

        memcpy(&pData[readIndex], responseBuffer, readResponseLength);
        readIndex += readResponseLength;
    }

    return Status::Success;
}


CP2112::Status CP2112::read(uint8_t slaveAddress, std::span<uint8_t> data, uint32_t memoryAddress, uint32_t memoryAddressSize)
{
    return read(slaveAddress, data.data(), data.size(), memoryAddress, memoryAddressSize);
}


CP2112::Status CP2112::setGPIOConfiguration(uint8_t pinDirectionBitmap, uint8_t pinModeBitmap, uint8_t pinSpecialBitmap, uint8_t clkDiv)
{
    HID_SMBUS_STATUS hidSmbusStatus;

    hidSmbusStatus = HidSmbus_SetGpioConfig(pDeviceHandler, (BYTE)pinDirectionBitmap, (BYTE)pinModeBitmap, (BYTE)pinSpecialBitmap, (BYTE)clkDiv);
    CP2112_LOG("HidSmbus_SetGpioConfig(): {}\n", hidSmbusStatus);

    return convertStatus(hidSmbusStatus);
}


CP2112::Status CP2112::writeGPIOLatch(uint8_t latchValue, uint8_t latchMask)
{
    HID_SMBUS_STATUS hidSmbusStatus;

    hidSmbusStatus = HidSmbus_WriteLatch(pDeviceHandler, (BYTE)latchValue, (BYTE)latchMask);
    CP2112_LOG("HidSmbus_WriteLatch(): {} (latchValue=0b{:b}, latchMask=0b{:b})\n", hidSmbusStatus, latchValue, latchMask);

    return convertStatus(hidSmbusStatus);
}


CP2112::Status CP2112::readGPIOLatch(uint8_t& latchValue)
{
    HID_SMBUS_STATUS hidSmbusStatus;

    hidSmbusStatus = HidSmbus_ReadLatch(pDeviceHandler, &latchValue);
    CP2112_LOG("HidSmbus_ReadLatch(): {} (latchValue=0b{:b})\n", hidSmbusStatus, latchValue);

    return convertStatus(hidSmbusStatus);
}


CP2112::Status CP2112::enableTXRXPins()
{
    constexpr uint8_t pinDirectionBitmap = 0b0000'0011;
    constexpr uint8_t pinModeBitmap      = 0b0000'0000;
    constexpr uint8_t pinSpecialBitmap   = 0b0000'0110;
    constexpr uint8_t clkDiv             = 0;

    return setGPIOConfiguration(pinDirectionBitmap, pinModeBitmap, pinSpecialBitmap, clkDiv);
}


int CP2112::countDevices()
{
    HID_SMBUS_STATUS hidSmbusStatus;
    DWORD numDevices;

    hidSmbusStatus = HidSmbus_GetNumDevices(&numDevices, VID, PID);
    CP2112_LOG("HidSmbus_GetNumDevices(): {} (numDevices={})\n", hidSmbusStatus, numDevices);
    if (convertStatus(hidSmbusStatus) != Status::Success)
    {
        numDevices = 0;
    }

    return numDevices;
}


CP2112::Status CP2112::convertStatus(uint8_t hidSmbusStatus)
{
    Status status;

    switch (hidSmbusStatus)
    {
        case HID_SMBUS_SUCCESS:                 status = Status::Success; break;
        case HID_SMBUS_DEVICE_NOT_FOUND:        status = Status::USBDeviceError; break;
        case HID_SMBUS_INVALID_HANDLE:          status = Status::USBDeviceError; break;
        case HID_SMBUS_INVALID_DEVICE_OBJECT:   status = Status::USBDeviceError; break;
        case HID_SMBUS_INVALID_PARAMETER:       status = Status::InvalidArgument; break;
        case HID_SMBUS_INVALID_REQUEST_LENGTH:  status = Status::InvalidArgument; break;

        case HID_SMBUS_READ_ERROR:              status = Status::USBTransferError; break;
        case HID_SMBUS_WRITE_ERROR:             status = Status::USBTransferError; break;
        case HID_SMBUS_READ_TIMED_OUT:          status = Status::SMBusTimeout; break;
        case HID_SMBUS_WRITE_TIMED_OUT:         status = Status::SMBusTimeout; break;
        case HID_SMBUS_DEVICE_IO_FAILED:        status = Status::USBDeviceError; break;
        case HID_SMBUS_DEVICE_ACCESS_ERROR:     status = Status::USBDeviceError; break;
        case HID_SMBUS_DEVICE_NOT_SUPPORTED:    status = Status::USBDeviceError; break;

        case HID_SMBUS_UNKNOWN_ERROR:           status = Status::UnknownError; break;

        default: status = Status::UnknownError;
    }

    return status;
}


CP2112::Status CP2112::convertStatus(uint8_t hidSmbusStatus, uint8_t status0, uint8_t status1)
{
    Status status = convertStatus(hidSmbusStatus);

    if (status != Status::Success)
    {
        return status;
    }

    switch (status0)
    {
        case HID_SMBUS_S0_IDLE:
            break;

        case HID_SMBUS_S0_BUSY:
            if (status1 == HID_SMBUS_S1_BUSY_ADDRESS_NACKED)
            {
                status = Status::SMBusTimeoutNack;
            }
            break;

        case HID_SMBUS_S0_COMPLETE:
            break;

        case HID_SMBUS_S0_ERROR:
            switch (status1)
            {
                case HID_SMBUS_S1_ERROR_TIMEOUT_NACK:           status = Status::SMBusTimeoutNack; break;
                case HID_SMBUS_S1_ERROR_TIMEOUT_BUS_NOT_FREE:   status = Status::SMBusTimeoutBusNotFree; break;
                case HID_SMBUS_S1_ERROR_ARB_LOST:               status = Status::SMBusArbitrationLost; break;
                case HID_SMBUS_S1_ERROR_READ_INCOMPLETE:        status = Status::SMBusReadIncomplete; break;
                case HID_SMBUS_S1_ERROR_WRITE_INCOMPLETE:       status = Status::SMBusWriteIncomplete; break;
                case HID_SMBUS_S1_ERROR_SUCCESS_AFTER_RETRY:    status = Status::Success; break;

                default: status = Status::UnknownError;
            }
            break;

        default: status = Status::UnknownError;
    }

    return status;
}


std::string CP2112::toString(Status status)
{
    switch (status)
    {
        using enum CP2112::Status;

        case Success:                   return "Success";
        case UnknownError:              return "UnknownError";
        case InvalidArgument:           return "InvalidArgument";
        case USBDeviceError:            return "USBDeviceError";
        case USBTransferError:          return "USBTransferError";
        case SMBusTimeout:              return "SMBusTimeout";
        case SMBusTimeoutNack:          return "SMBusTimeoutNack";
        case SMBusTimeoutBusNotFree:    return "SMBusTimeoutBusNotFree";
        case SMBusArbitrationLost:      return "SMBusArbitrationLost";
        case SMBusReadIncomplete:       return "SMBusReadIncomplete";
        case SMBusWriteIncomplete:      return "SMBusWriteIncomplete";

        default:
            return "?";
    }
}
