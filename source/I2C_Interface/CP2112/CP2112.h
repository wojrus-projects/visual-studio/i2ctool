//
// Interfejs (mostek) USB<->SMBus/I2C master (SiLabs CP2112).
//
// Requirements: C++20 latest.
//

#pragma once

#include <cstdint>
#include <span>
#include <string>

#include "Silabs_CP2112_lib/SLABCP2112.h"


/// <summary>
/// Interfejs (mostek) USB<->SMBus/I2C master (SiLabs CP2112).
/// </summary>
class CP2112 final
{
public:
    /// <summary>
    /// Domyślne kody USB VID i PID.
    /// </summary>
    static constexpr WORD VID = 0x10C4;
    static constexpr WORD PID = 0xEA90;

    /// <summary>
    /// Zakres 8-bitowych adresów slave I2C (bit 0 musi być = 0).
    /// </summary>
    static constexpr uint8_t SLAVE_ADDRESS_MIN = 0x02;
    static constexpr uint8_t SLAVE_ADDRESS_MAX = 0xFE;

    /// <summary>
    /// Limity długości transferów SMBus.
    /// </summary>
    static constexpr uint32_t WRITE_MIN = 1;
    static constexpr uint32_t WRITE_MAX = 61;
    static constexpr uint32_t READ_MIN = 1;
    static constexpr uint32_t READ_MAX = 512;

    /// <summary>
    /// Limity parametru 'memoryAddressSize' funkcji read().
    /// </summary>
    static constexpr uint32_t MEMORY_ADDRESS_SIZE_MIN = 1;
    static constexpr uint32_t MEMORY_ADDRESS_SIZE_MAX = 4;

    /// <summary>
    /// Zakres f_clk [Hz].
    /// </summary>
    static constexpr uint32_t SCL_FREQUENCY_MIN = 1;
    static constexpr uint32_t SCL_FREQUENCY_MAX = 400'000;

    /// <summary>
    /// Kody statusu.
    /// </summary>
    enum class Status
    {
        Success,
        UnknownError,
        InvalidArgument,
        USBDeviceError,
        USBTransferError,
        SMBusTimeout,
        SMBusTimeoutNack,
        SMBusTimeoutBusNotFree,
        SMBusArbitrationLost,
        SMBusReadIncomplete,
        SMBusWriteIncomplete
    };

public:
    CP2112() = default;
    ~CP2112();

    // Disallow copying and moving objects.
    CP2112(const CP2112&) = delete;
    CP2112& operator=(const CP2112&) = delete;
    CP2112(CP2112&&) = delete;
    CP2112& operator=(CP2112&&) = delete;

    /// <summary>
    /// Otworzenie interfejsu SMBus.
    /// </summary>
    /// <param name="deviceNumber">Numer instancji interfejsu: 0...MAX (MAX = countInterfaces() - 1).</param>
    /// <returns>Status</returns>
    Status open(int deviceNumber);

    /// <summary>
    /// Zamknięcie interfejsu SMBus.
    /// </summary>
    /// <returns>Status</returns>
    Status close();

    /// <summary>
    /// Sprawdzenie czy interfejs jest otwarty.
    /// </summary>
    /// <param name="openState">Stan interfejsu.</param>
    /// <returns>Status</returns>
    Status isOpen(bool& openState);

    /// <summary>
    /// Ustawienie konfiguracji SMBus.
    /// </summary>
    /// <param name="bitRate">Częstotliwość SCL w [Hz] (1...400'000).</param>
    /// <param name="transferTimeout">Timeout transmisji SMBus Read/Write w [ms] (1...1000).</param>
    /// <param name="responseTimeout">Timeout odpowiedzi USB-HID w [ms] (1...10'000). Musi być większy od transferTimeout.</param>
    /// <returns>Status</returns>
    Status setConfiguration(uint32_t bitRate, uint32_t transferTimeout, uint32_t responseTimeout);

    /// <summary>
    /// Zapisanie danych do slave SMBus.
    /// </summary>
    /// <param name="slaveAddress">Adres 8-bitowy slave: 0x02...0xFE.</param>
    /// <param name="pData">Dane do wysłania.</param>
    /// <param name="dataLength">Długość danych: 1...61 bajtów.</param>
    /// <returns>Status</returns>
    Status write(uint8_t slaveAddress, const uint8_t* pData, size_t dataLength);

    /// <summary>
    /// Zapisanie danych do slave SMBus (wersja z std::span<>).
    /// </summary>
    /// <param name="slaveAddress">Adres 8-bitowy slave: 0x02...0xFE.</param>
    /// <param name="data">Dane do wysłania (1...61 bajtów).</param>
    /// <returns>Status</returns>
    Status write(uint8_t slaveAddress, std::span<const uint8_t> data);

    /// <summary>
    /// Odczytanie danych ze slave SMBus.
    /// </summary>
    /// <param name="slaveAddress">Adres 8-bitowy slave: 0x02...0xFE.</param>
    /// <param name="pData">Bufor na dane odczytane.</param>
    /// <param name="dataLength">Długość bufora: 1...512 bajtów.</param>
    /// <param name="memoryAddress">[Opcja] Adres pamięci np. EEPROM, który będzie zapisany do slave przed jego odczytem.</param>
    /// <param name="memoryAddressSize">[Opcja] Rozmiar (1...4 bajty) słowa adresu pamięci. Wartość 0 wyłącza fukcję adresacji pamięci.</param>
    /// <returns>Status</returns>
    Status read(uint8_t slaveAddress, uint8_t* pData, size_t dataLength, uint32_t memoryAddress = 0, uint32_t memoryAddressSize = 0);

    /// <summary>
    /// Odczytanie danych ze slave SMBus (wersja z std::span<>).
    /// </summary>
    /// <param name="slaveAddress">Adres 8-bitowy slave: 0x02...0xFE.</param>
    /// <param name="data">Bufor na dane odczytane (1...512 bajtów).</param>
    /// <param name="memoryAddress">[Opcja] Adres pamięci np. EEPROM, który będzie zapisany do slave przed jego odczytem.</param>
    /// <param name="memoryAddressSize">[Opcja] Rozmiar (1...4 bajty) słowa adresu pamięci. Wartość 0 wyłącza fukcję adresacji pamięci.</param>
    /// <returns>Status</returns>
    Status read(uint8_t slaveAddress, std::span<uint8_t> data, uint32_t memoryAddress = 0, uint32_t memoryAddressSize = 0);

    /// <summary>
    /// Ustawienie konfiguracji GPIO.
    /// </summary>
    /// <param name="pinDirectionBitmap">Kierunek pinów 0...7 zawarty na bitach 0...7 bitmapy (wartość bitu: 0=Input, 1=Output).</param>
    /// <param name="pinModeBitmap">Tryb pracy pinów 0...7 ustawionych jako Output zawarty na bitach 0...7 bitmapy (wartość bitu: 0=Open-Drain, 1=Push-Pull).</param>
    /// <param name="pinSpecialBitmap">Włączenie funkcji specjalnych zamiast GPIO (włącza stan 1 w bitmapie): bit0=GPIO_7_CLK, bit1=GPIO_0_TXT, bit2=GPIO_1_RXT.</param>
    /// <param name="clkDiv">Dzielnik zegara wyprowadzonego na GPIO.7 (f=48 MHz / (2 x clkDiv) dla clkDiv=1...255 lub f=48 MHz dla clkDiv=0).</param>
    /// <returns>Status</returns>
    Status setGPIOConfiguration(uint8_t pinDirectionBitmap, uint8_t pinModeBitmap, uint8_t pinSpecialBitmap, uint8_t clkDiv);

    /// <summary>
    /// Ustawienie stanu wyjść GPIO.
    /// </summary>
    /// <param name="latchValue">Stan wyjść 0...7 zawarty na bitach 0...7 bitmapy (wartość bitu: 0=L, 1=H).</param>
    /// <param name="latchMask">Maska bitowa do wybrania grupy wyjść do ustawienia.</param>
    /// <returns>Status</returns>
    Status writeGPIOLatch(uint8_t latchValue, uint8_t latchMask);

    /// <summary>
    /// Odczytanie stanu wejść GPIO.
    /// </summary>
    /// <param name="latchValue">Stan wejść 0...7.</param>
    /// <returns>Status</returns>
    Status readGPIOLatch(uint8_t& latchValue);

    /// <summary>
    /// Włączenie funkcji TX/RX LED na pinach GPIO.0 i GPIO.1.
    /// </summary>
    /// <returns></returns>
    Status enableTXRXPins();

    /// <summary>
    /// Zwraca ilość podłączonych układów CP2112.
    /// </summary>
    /// <remarks>
    /// Funkcja musi być wywołana jako pierwsza (lub wystarczy istniejące już wywołanie w open()).
    /// </remarks>
    /// <returns>Ilość układów</returns>
    static int countDevices();

    /// <summary>
    /// Konwersja statusu do string.
    /// </summary>
    /// <param name="status">Status</param>
    /// <returns>String</returns>
    static std::string toString(Status status);

private:
    static Status convertStatus(uint8_t hidSmbusStatus);
    static Status convertStatus(uint8_t hidSmbusStatus, uint8_t status0, uint8_t status1);

    HID_SMBUS_DEVICE pDeviceHandler = nullptr;
};
