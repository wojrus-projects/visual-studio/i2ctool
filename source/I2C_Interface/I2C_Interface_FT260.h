#pragma once

#include "I2C_Interface_Base.h"
#include "FT260/LibFT260.h"


namespace I2C_Interface
{
    using namespace std::chrono_literals;


    class I2C_Interface_FT260 final : public I2C_Interface_Base
    {
    public:
        I2C_Interface_FT260();
        ~I2C_Interface_FT260() override = default;

        // Disallow copying and moving objects.
        I2C_Interface_FT260(const I2C_Interface_FT260&) = delete;
        I2C_Interface_FT260& operator=(const I2C_Interface_FT260&) = delete;
        I2C_Interface_FT260(I2C_Interface_FT260&&) = delete;
        I2C_Interface_FT260& operator=(I2C_Interface_FT260&&) = delete;

        I2C_Interface_Status open(int instanceNumber) override;
        I2C_Interface_Status close() override;
        I2C_Interface_Status setBitRate(uint32_t bitRate_bps) override;
        I2C_Interface_Status write(uint8_t slaveAddress, const uint8_t* pData, size_t dataLength) override;
        I2C_Interface_Status write(uint8_t slaveAddress, std::span<const uint8_t> data) override;
        I2C_Interface_Status read(uint8_t slaveAddress, uint8_t* pData, size_t dataLength, uint32_t memoryAddress = 0, uint32_t memoryAddressSize = 0) override;

        std::pair<uint32_t, uint32_t> getBitRateRange() override;
        std::pair<uint32_t, uint32_t> getWriteLengthRange() override;
        std::pair<uint32_t, uint32_t> getReadLengthRange() override;

    private:
        static constexpr WORD VID = 0x0403;
        static constexpr WORD PID = 0x6030;

        // Slave address 8-bit range:
        static constexpr uint8_t SLAVE_ADDRESS_MIN = 0x02;
        static constexpr uint8_t SLAVE_ADDRESS_MAX = 0xFE;

        // Write/read data length bytes ranges:
        static constexpr uint32_t WRITE_MIN = 1;
        static constexpr uint32_t WRITE_MAX = 0xFFFF;
        static constexpr uint32_t READ_MIN = 1;
        static constexpr uint32_t READ_MAX = 0xFFFF;

        // SCL frequency/bitrate range:
        static constexpr uint32_t SCL_FREQUENCY_MIN = 60'000;
        static constexpr uint32_t SCL_FREQUENCY_MAX = 3'400'000;

        // Standard read timeout.
        // Worst case is 60kbps x 0xFFFF bytes = 15s.
        static constexpr std::chrono::seconds READ_TIMEOUT_s = 5s;

        FT260_HANDLE ftHandle = INVALID_HANDLE_VALUE;

        I2C_Interface_Status I2CMasterControllerWaitForIdle();

        static I2C_Interface_Status convertStatusFT260(FT260_STATUS ft260Status);
        static I2C_Interface_Status convertStatusI2CMaster(uint8_t i2cMasterStatus);

        // Check I2C master status from FT260_I2CMaster_GetStatus().
        static bool I2CMasterControllerIsBusy(uint8_t i2cMasterStatus);
        static bool I2CMasterControllerIsAnyError(uint8_t i2cMasterStatus);
        static bool I2CMasterControllerIsDataNack(uint8_t i2cMasterStatus);
        static bool I2CMasterControllerIsAddressNack(uint8_t i2cMasterStatus);
        static bool I2CMasterControllerIsArbitrationLost(uint8_t i2cMasterStatus);
        static bool I2CMasterControllerIsIdle(uint8_t i2cMasterStatus);
        static bool I2CMasterControllerIsIdleWithoutError(uint8_t i2cMasterStatus);
        static bool I2CMasterControllerIsBusBusy(uint8_t i2cMasterStatus);
    };
} // namespace I2C_Interface
