#include "I2C_Interface_Base.h"


namespace I2C_Interface
{
    std::string toString(I2C_Interface_Status status)
    {
        switch (status)
        {
            using enum I2C_Interface_Status;

            case Success:                   return "Success";
            case UnknownError:              return "UnknownError";
            case InvalidArgument:           return "InvalidArgument";
            case USBError:                  return "USBError";
            case DeviceError:               return "DeviceError";
            case I2CBusy:                   return "I2CBusy";
            case I2CTimeout:                return "I2CTimeout";
            case I2CNack:                   return "I2CNack";
            case I2CArbitrationLost:        return "I2CArbitrationLost";
            case I2CTransferIncomplete:     return "I2CTransferIncomplete";

            default:
                return "?";
        }
    }
} // namespace I2C_Interface
