#pragma once

#include "I2C_Interface_Base.h"
#include "CP2112/CP2112.h"


namespace I2C_Interface
{
    class I2C_Interface_CP2112 final : public I2C_Interface_Base
    {
    public:
        I2C_Interface_CP2112();
        ~I2C_Interface_CP2112() override = default;

        // Disallow copying and moving objects.
        I2C_Interface_CP2112(const I2C_Interface_CP2112&) = delete;
        I2C_Interface_CP2112& operator=(const I2C_Interface_CP2112&) = delete;
        I2C_Interface_CP2112(I2C_Interface_CP2112&&) = delete;
        I2C_Interface_CP2112& operator=(I2C_Interface_CP2112&&) = delete;

        I2C_Interface_Status open(int instanceNumber) override;
        I2C_Interface_Status close() override;
        I2C_Interface_Status setBitRate(uint32_t bitRate_bps) override;
        I2C_Interface_Status write(uint8_t slaveAddress, const uint8_t* pData, size_t dataLength) override;
        I2C_Interface_Status write(uint8_t slaveAddress, std::span<const uint8_t> data) override;
        I2C_Interface_Status read(uint8_t slaveAddress, uint8_t* pData, size_t dataLength, uint32_t memoryAddress = 0, uint32_t memoryAddressSize = 0) override;

        std::pair<uint32_t, uint32_t> getBitRateRange() override;
        std::pair<uint32_t, uint32_t> getWriteLengthRange() override;
        std::pair<uint32_t, uint32_t> getReadLengthRange() override;

    private:
        CP2112 CP2112;

        static I2C_Interface_Status convertStatus(CP2112::Status cp2112Status);
    };
} // namespace I2C_Interface
