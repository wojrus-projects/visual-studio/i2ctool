#pragma once

#include <string>
#include <memory>

#include "I2C_Interface_Base.h"
#include "I2C_Interface_CP2112.h"
#include "I2C_Interface_FT260.h"


namespace I2C_Interface
{
    std::shared_ptr<I2C_Interface_Base> I2CInterfaceMake(std::string name);
}
