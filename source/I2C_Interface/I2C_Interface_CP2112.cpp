#include "I2C_Interface_CP2112.h"


namespace I2C_Interface
{
    I2C_Interface_CP2112::I2C_Interface_CP2112()
        : I2C_Interface_Base{ "CP2112" }
    {
    }


    I2C_Interface_Status I2C_Interface_CP2112::open(int instanceNumber)
    {
        I2C_Interface_Status i2cInterfaceStatus;

        i2cInterfaceStatus = convertStatus(CP2112.open(instanceNumber));
        if (i2cInterfaceStatus != I2C_Interface_Status::Success)
        {
            return i2cInterfaceStatus;
        }

        CP2112.enableTXRXPins();

        return I2C_Interface_Status::Success;
    }


    I2C_Interface_Status I2C_Interface_CP2112::close()
    {
        CP2112.close();

        return I2C_Interface_Status::Success;
    }


    I2C_Interface_Status I2C_Interface_CP2112::setBitRate(uint32_t bitRate_bps)
    {
        return convertStatus(CP2112.setConfiguration(bitRate_bps, 20, 50));
    }


    I2C_Interface_Status I2C_Interface_CP2112::write(uint8_t slaveAddress, const uint8_t* pData, size_t dataLength)
    {
        return convertStatus(CP2112.write(slaveAddress, pData, dataLength));
    }


    I2C_Interface_Status I2C_Interface_CP2112::write(uint8_t slaveAddress, std::span<const uint8_t> data)
    {
        return write(slaveAddress, data.data(), data.size());
    }


    I2C_Interface_Status I2C_Interface_CP2112::read(uint8_t slaveAddress, uint8_t* pData, size_t dataLength, uint32_t memoryAddress, uint32_t memoryAddressSize)
    {
        return convertStatus(CP2112.read(slaveAddress, pData, dataLength, memoryAddress, memoryAddressSize));
    }


    std::pair<uint32_t, uint32_t> I2C_Interface_CP2112::getBitRateRange()
    {
        return { CP2112::SCL_FREQUENCY_MIN, CP2112::SCL_FREQUENCY_MAX };
    }


    std::pair<uint32_t, uint32_t> I2C_Interface_CP2112::getWriteLengthRange()
    {
        return { CP2112::WRITE_MIN, CP2112::WRITE_MAX };
    }


    std::pair<uint32_t, uint32_t> I2C_Interface_CP2112::getReadLengthRange()
    {
        return { CP2112::READ_MIN, CP2112::READ_MAX };
    }


    I2C_Interface_Status I2C_Interface_CP2112::convertStatus(CP2112::Status cp2112Status)
    {
        switch (cp2112Status)
        {
            using enum CP2112::Status;

            case Success:                       return I2C_Interface_Status::Success;
            case UnknownError:                  return I2C_Interface_Status::UnknownError;
            case InvalidArgument:               return I2C_Interface_Status::InvalidArgument;
            case USBDeviceError:                return I2C_Interface_Status::DeviceError;
            case USBTransferError:              return I2C_Interface_Status::USBError;
            case SMBusTimeout:                  return I2C_Interface_Status::I2CTimeout;
            case SMBusTimeoutNack:              return I2C_Interface_Status::I2CNack;
            case SMBusTimeoutBusNotFree:        return I2C_Interface_Status::I2CBusy;
            case SMBusArbitrationLost:          return I2C_Interface_Status::I2CArbitrationLost;
            case SMBusReadIncomplete:           return I2C_Interface_Status::I2CTransferIncomplete;
            case SMBusWriteIncomplete:          return I2C_Interface_Status::I2CTransferIncomplete;

            default:                            return I2C_Interface_Status::UnknownError;
        }
    }
} // namespace I2C_Interface
