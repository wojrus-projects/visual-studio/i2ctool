#include <algorithm>
#include <cctype>

#include "I2C_Interface.h"


namespace I2C_Interface
{
    std::shared_ptr<I2C_Interface_Base> I2CInterfaceMake(std::string name)
    {
        std::transform(name.begin(), name.end(), name.begin(), ::tolower);

        if (name == "cp2112")
        {
            return std::make_shared<I2C_Interface_CP2112>();
        }
        else if (name == "ft260")
        {
            return std::make_shared<I2C_Interface_FT260>();
        }
        else
        {
            return {};
        }
    }
} // namespace I2C_Interface
