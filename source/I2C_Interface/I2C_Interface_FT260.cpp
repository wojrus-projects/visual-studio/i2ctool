#include <chrono>

#include <spdlog.h>

#include "I2C_Interface_FT260.h"


namespace I2C_Interface
{
    static constexpr bool FT260_EnableI2CMasterReset = false;


    I2C_Interface_FT260::I2C_Interface_FT260()
        : I2C_Interface_Base{ "FT260" }
    {
        setTimeout(READ_TIMEOUT_s);
    }


    I2C_Interface_Status I2C_Interface_FT260::open(int instanceNumber)
    {
        I2C_Interface_Status i2cInterfaceStatus;
        DWORD devNum = 0;

        i2cInterfaceStatus = convertStatusFT260(FT260_CreateDeviceList(&devNum));
        if (i2cInterfaceStatus != I2C_Interface_Status::Success)
        {
            return i2cInterfaceStatus;
        }

        if (devNum == 0)
        {
            return I2C_Interface_Status::USBError;
        }

        i2cInterfaceStatus = convertStatusFT260(FT260_OpenByVidPid(VID, PID, (DWORD)instanceNumber, &ftHandle));
        if (i2cInterfaceStatus != I2C_Interface_Status::Success)
        {
            return i2cInterfaceStatus;
        }

        i2cInterfaceStatus = convertStatusFT260(FT260_I2CMaster_Init(ftHandle, 100));
        if (i2cInterfaceStatus != I2C_Interface_Status::Success)
        {
            return i2cInterfaceStatus;
        }

        return I2C_Interface_Status::Success;
    }


    I2C_Interface_Status I2C_Interface_FT260::close()
    {
        if (ftHandle != INVALID_HANDLE_VALUE)
        {
            FT260_Close(ftHandle);
            ftHandle = INVALID_HANDLE_VALUE;
        }

        return I2C_Interface_Status::Success;
    }


    I2C_Interface_Status I2C_Interface_FT260::setBitRate(uint32_t bitRate_bps)
    {
        const uint32_t bitRate_kbps = bitRate_bps / 1000;

        SPDLOG_DEBUG("FT260_I2CMaster_Init: bitRate_kbps={}", bitRate_kbps);

        return convertStatusFT260(FT260_I2CMaster_Init(ftHandle, bitRate_kbps));
    }


    I2C_Interface_Status I2C_Interface_FT260::write(uint8_t slaveAddress, const uint8_t* pData, size_t dataLength)
    {
        I2C_Interface_Status i2cInterfaceStatus;
        DWORD dataLengthActual = 0;

        if constexpr (FT260_EnableI2CMasterReset == true)
        {
            i2cInterfaceStatus = convertStatusFT260(FT260_I2CMaster_Reset(ftHandle));
            if (i2cInterfaceStatus != I2C_Interface_Status::Success)
            {
                return i2cInterfaceStatus;
            }
        }

        SPDLOG_DEBUG("FT260_I2CMaster_Write");
        i2cInterfaceStatus = convertStatusFT260(FT260_I2CMaster_Write(ftHandle, slaveAddress >> 1, FT260_I2C_START_AND_STOP,
            (LPVOID)pData, (DWORD)dataLength, &dataLengthActual));
        if (i2cInterfaceStatus != I2C_Interface_Status::Success)
        {
            return i2cInterfaceStatus;
        }

        i2cInterfaceStatus = I2CMasterControllerWaitForIdle();
        if (i2cInterfaceStatus != I2C_Interface_Status::Success)
        {
            return i2cInterfaceStatus;
        }

        if ((DWORD)dataLength != dataLengthActual)
        {
            return I2C_Interface_Status::I2CTransferIncomplete;
        }

        return I2C_Interface_Status::Success;
    }


    I2C_Interface_Status I2C_Interface_FT260::write(uint8_t slaveAddress, std::span<const uint8_t> data)
    {
        return write(slaveAddress, data.data(), data.size());
    }


    I2C_Interface_Status I2C_Interface_FT260::read(uint8_t slaveAddress, uint8_t* pData, size_t dataLength, uint32_t memoryAddress, uint32_t memoryAddressSize)
    {
        I2C_Interface_Status i2cInterfaceStatus;
        DWORD dataLengthActual = 0;
        const bool enableMemoryMode = (memoryAddressSize > 0);

        if (enableMemoryMode == true)
        {
            //
            // Write memory start address.
            //

            if constexpr (FT260_EnableI2CMasterReset == true)
            {
                i2cInterfaceStatus = convertStatusFT260(FT260_I2CMaster_Reset(ftHandle));
                if (i2cInterfaceStatus != I2C_Interface_Status::Success)
                {
                    return i2cInterfaceStatus;
                }
            }

            i2cInterfaceStatus = write(slaveAddress, (const uint8_t*)&memoryAddress, (size_t)memoryAddressSize);
            if (i2cInterfaceStatus != I2C_Interface_Status::Success)
            {
                return i2cInterfaceStatus;
            }
        }

        //
        // Read slave with or without memory start address.
        //

        if constexpr (FT260_EnableI2CMasterReset == true)
        {
            i2cInterfaceStatus = convertStatusFT260(FT260_I2CMaster_Reset(ftHandle));
            if (i2cInterfaceStatus != I2C_Interface_Status::Success)
            {
                return i2cInterfaceStatus;
            }
        }

        const DWORD readTimeout_ms = std::chrono::duration<DWORD>(this->timeout).count() * 1000;

        SPDLOG_DEBUG("FT260_I2CMaster_Read: timeout={} ms", readTimeout_ms);
        i2cInterfaceStatus = convertStatusFT260(FT260_I2CMaster_Read(ftHandle, slaveAddress >> 1, FT260_I2C_START_AND_STOP, pData,
            (DWORD)dataLength, &dataLengthActual, readTimeout_ms));
        if (i2cInterfaceStatus != I2C_Interface_Status::Success)
        {
            return i2cInterfaceStatus;
        }

        i2cInterfaceStatus = I2CMasterControllerWaitForIdle();
        if (i2cInterfaceStatus != I2C_Interface_Status::Success)
        {
            return i2cInterfaceStatus;
        }

        if ((DWORD)dataLength != dataLengthActual)
        {
            return I2C_Interface_Status::I2CTransferIncomplete;
        }

        return I2C_Interface_Status::Success;
    }


    std::pair<uint32_t, uint32_t> I2C_Interface_FT260::getBitRateRange()
    {
        return { SCL_FREQUENCY_MIN, SCL_FREQUENCY_MAX };
    }


    std::pair<uint32_t, uint32_t> I2C_Interface_FT260::getWriteLengthRange()
    {
        return { WRITE_MIN, WRITE_MAX };
    }


    std::pair<uint32_t, uint32_t> I2C_Interface_FT260::getReadLengthRange()
    {
        return { READ_MIN, READ_MAX };
    }


    I2C_Interface_Status I2C_Interface_FT260::I2CMasterControllerWaitForIdle()
    {
        using namespace std::chrono_literals;

        constexpr std::chrono::milliseconds TIMEOUT_ms = 1000ms;

        I2C_Interface_Status i2cInterfaceStatus;
        uint8_t i2cMasterStatus;
        uint32_t timeCounter_ms = 0;

        const auto timeStart = std::chrono::high_resolution_clock::now();

        do
        {
            i2cMasterStatus = 0;

            i2cInterfaceStatus = convertStatusFT260(FT260_I2CMaster_GetStatus(ftHandle, &i2cMasterStatus));
            if (i2cInterfaceStatus != I2C_Interface_Status::Success)
            {
                return i2cInterfaceStatus;
            }

            SPDLOG_DEBUG("FT260_I2CMaster_GetStatus: i2cMasterStatus=0x{:02X}", i2cMasterStatus);

            const auto timeDelta = std::chrono::high_resolution_clock::now() - timeStart;

            if (timeDelta > TIMEOUT_ms)
            {
                return I2C_Interface_Status::I2CTimeout;
            }

        } while ((I2CMasterControllerIsBusy(i2cMasterStatus) == true)
              || (I2CMasterControllerIsBusy(i2cMasterStatus) == false && I2CMasterControllerIsBusBusy(i2cMasterStatus) == true && I2CMasterControllerIsAnyError(i2cMasterStatus) == false));

        return convertStatusI2CMaster(i2cMasterStatus);
    }


    I2C_Interface_Status I2C_Interface_FT260::convertStatusFT260(FT260_STATUS ft260Status)
    {
        switch (ft260Status)
        {
            using enum I2C_Interface_Status;

            case FT260_OK:                              return Success;
            case FT260_INVALID_HANDLE:                  return InvalidArgument;
            case FT260_DEVICE_NOT_FOUND:                return DeviceError;
            case FT260_DEVICE_NOT_OPENED:               return DeviceError;
            case FT260_DEVICE_OPEN_FAIL:                return DeviceError;
            case FT260_DEVICE_CLOSE_FAIL:               return DeviceError;
            case FT260_INCORRECT_INTERFACE:             return DeviceError;
            case FT260_INCORRECT_CHIP_MODE:             return DeviceError;
            case FT260_DEVICE_MANAGER_ERROR:            return DeviceError;
            case FT260_IO_ERROR:                        return DeviceError;
            case FT260_INVALID_PARAMETER:               return InvalidArgument;
            case FT260_NULL_BUFFER_POINTER:             return InvalidArgument;
            case FT260_BUFFER_SIZE_ERROR:               return InvalidArgument;
            case FT260_UART_SET_FAIL:                   return UnknownError;
            case FT260_RX_NO_DATA:                      return UnknownError;
            case FT260_GPIO_WRONG_DIRECTION:            return InvalidArgument;
            case FT260_INVALID_DEVICE:                  return DeviceError;
            case FT260_INVALID_OPEN_DRAIN_SET:          return InvalidArgument;
            case FT260_INVALID_OPEN_DRAIN_RESET:        return InvalidArgument;
            case FT260_I2C_READ_FAIL:                   return I2CTransferIncomplete;
            case FT260_OTHER_ERROR:                     return UnknownError;

            default:                                    return UnknownError;
        }
    }


    I2C_Interface_Status I2C_Interface_FT260::convertStatusI2CMaster(uint8_t i2cMasterStatus)
    {
        if (I2CMasterControllerIsBusy(i2cMasterStatus))
        {
            return I2C_Interface_Status::I2CBusy;
        }
        else
        {
            if (I2CMasterControllerIsIdleWithoutError(i2cMasterStatus))
            {
                return I2C_Interface_Status::Success;
            }
            else
            {
                if (I2CMasterControllerIsArbitrationLost(i2cMasterStatus))
                {
                    return I2C_Interface_Status::I2CArbitrationLost;
                }
                else if (I2CMasterControllerIsAddressNack(i2cMasterStatus) || I2CMasterControllerIsDataNack(i2cMasterStatus))
                {
                    return I2C_Interface_Status::I2CNack;
                }
                else if (I2CMasterControllerIsBusBusy(i2cMasterStatus))
                {
                    return I2C_Interface_Status::I2CBusy;
                }
                else
                {
                    return I2C_Interface_Status::UnknownError;
                }
            }
        }

        return I2C_Interface_Status::UnknownError;
    }


    bool I2C_Interface_FT260::I2CMasterControllerIsBusy(uint8_t i2cMasterStatus)
    {
        return ((i2cMasterStatus & 0x01) == 0x01);
    }


    bool I2C_Interface_FT260::I2CMasterControllerIsAnyError(uint8_t i2cMasterStatus)
    {
        return ((i2cMasterStatus & 0x02) == 0x02);
    }


    bool I2C_Interface_FT260::I2CMasterControllerIsDataNack(uint8_t i2cMasterStatus)
    {
        return ((i2cMasterStatus & 0x0A) == 0x0A);
    }


    bool I2C_Interface_FT260::I2CMasterControllerIsAddressNack(uint8_t i2cMasterStatus)
    {
        return ((i2cMasterStatus & 0x06) == 0x06);
    }


    bool I2C_Interface_FT260::I2CMasterControllerIsArbitrationLost(uint8_t i2cMasterStatus)
    {
        return ((i2cMasterStatus & 0x12) == 0x12);
    }


    bool I2C_Interface_FT260::I2CMasterControllerIsIdle(uint8_t i2cMasterStatus)
    {
        return ((i2cMasterStatus & 0x20) == 0x20);
    }


    bool I2C_Interface_FT260::I2CMasterControllerIsIdleWithoutError(uint8_t i2cMasterStatus)
    {
        return (i2cMasterStatus == 0x20);
    }


    bool I2C_Interface_FT260::I2CMasterControllerIsBusBusy(uint8_t i2cMasterStatus)
    {
        return ((i2cMasterStatus & 0x40) == 0x40);
    }
} // namespace I2C_Interface
