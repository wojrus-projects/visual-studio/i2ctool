﻿#include <algorithm>
#include <format>
#include <regex>

#include "TMP75C.h"


namespace I2C_Device
{
    I2C_Interface::I2C_Interface_Status TMP75C::writeRegister(RegisterAddress address, uint16_t value)
    {
        const uint8_t buffer[1 + 2] =
        {
            uint8_t(address),
            uint8_t(value >> 8),
            uint8_t(value >> 0)
        };

        return this->pI2CInterface->write(this->i2cSlaveAddress, buffer);
    }


    I2C_Interface::I2C_Interface_Status TMP75C::readRegister(RegisterAddress address, uint16_t& value)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        uint8_t buffer[2];

        buffer[0] = uint8_t(address);
        i2cInterfaceStatus = this->pI2CInterface->write(this->i2cSlaveAddress, buffer, 1);
        if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
        {
            i2cInterfaceStatus = this->pI2CInterface->read(this->i2cSlaveAddress, buffer, 2);
            if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
            {
                value = ((uint16_t)buffer[0] << 8) | ((uint16_t)buffer[1] << 0);
            }
        }

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status TMP75C::startOneShotConversion()
    {
        return writeRegister(RegisterAddress::OneShot, 0);
    }


    I2C_Interface::I2C_Interface_Status TMP75C::readTemperature(float& temperature)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        uint16_t temperatureRaw = 0;

        i2cInterfaceStatus = readRegister(RegisterAddress::Temperature, temperatureRaw);
        if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
        {
            temperature = convertRegisterToTemperature(temperatureRaw);
        }

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status TMP75C::setTemperatureLimitLow(float temperature)
    {
        return writeRegister(RegisterAddress::T_low, convertTemperatureToRegister(temperature));
    }


    I2C_Interface::I2C_Interface_Status TMP75C::setTemperatureLimitHigh(float temperature)
    {
        return writeRegister(RegisterAddress::T_high, convertTemperatureToRegister(temperature));
    }


    I2C_Interface::I2C_Interface_Status TMP75C::getTemperatureLimitLow(float& temperature)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        uint16_t temperatureRaw = 0;

        i2cInterfaceStatus = readRegister(RegisterAddress::T_low, temperatureRaw);
        if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
        {
            temperature = convertRegisterToTemperature(temperatureRaw);
        }

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status TMP75C::getTemperatureLimitHigh(float& temperature)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        uint16_t temperatureRaw = 0;

        i2cInterfaceStatus = readRegister(RegisterAddress::T_high, temperatureRaw);
        if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
        {
            temperature = convertRegisterToTemperature(temperatureRaw);
        }

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status TMP75C::setConfiguration(const TMP75C_Configuration& configuration)
    {
        return writeRegister(RegisterAddress::Configuration, configuration.toRegister());
    }


    I2C_Interface::I2C_Interface_Status TMP75C::getConfiguration(TMP75C_Configuration& configuration)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        uint16_t registerConfiguration = 0;

        i2cInterfaceStatus = readRegister(RegisterAddress::Configuration, registerConfiguration);
        if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
        {
            configuration.fromRegister(registerConfiguration);
        }

        return i2cInterfaceStatus;
    }


    constexpr float TMP75C::convertRegisterToTemperature(uint16_t registerValue)
    {
        return (float)((int16_t)registerValue >> 4) / 16.0f;
    }


    constexpr uint16_t TMP75C::convertTemperatureToRegister(float temperature)
    {
        temperature = std::clamp(temperature, TEMPERATURE_MIN, TEMPERATURE_MAX);

        return ((uint16_t)(temperature * 16.0f) << 4);
    }


    uint16_t TMP75C_Configuration::toRegister() const
    {
        uint16_t registerValue = 0x0000;

        switch (oneShot)
        {
            case OneShot::ContinuousConversionMode:     registerValue |= 0u << 13; break;
            case OneShot::OneShotMode:                  registerValue |= 1u << 13; break;
        }

        switch (faultQueue)
        {
            case FaultQueue::Wait_1_Fault:     registerValue |= 0u << 11; break;
            case FaultQueue::Wait_2_Fault:     registerValue |= 1u << 11; break;
            case FaultQueue::Wait_4_Fault:     registerValue |= 2u << 11; break;
            case FaultQueue::Wait_6_Fault:     registerValue |= 3u << 11; break;
        }

        switch (alertPolarity)
        {
            case AlertPolarity::ActiveLow:     registerValue |= 0u << 10; break;
            case AlertPolarity::ActiveHigh:    registerValue |= 1u << 10; break;
        }

        switch (alertThermostatMode)
        {
            case AlertThermostatMode::ComparatorMode:    registerValue |= 0u << 9; break;
            case AlertThermostatMode::InterruptMode:     registerValue |= 1u << 9; break;
        }

        switch (shutdownControl)
        {
            case ShutdownControl::ContinuousConversion:     registerValue |= 0u << 8; break;
            case ShutdownControl::ShutdownMode:             registerValue |= 1u << 8; break;
        }

        return registerValue;
    }


    void TMP75C_Configuration::fromRegister(uint16_t registerValue)
    {
        if (registerValue & (1u << 13))
        {
            oneShot = OneShot::OneShotMode;
        }
        else
        {
            oneShot = OneShot::ContinuousConversionMode;
        }

        const uint16_t fqBits = (registerValue >> 11) & 0x3u;

        faultQueue = (FaultQueue)fqBits;

        if (registerValue & (1u << 10))
        {
            alertPolarity = AlertPolarity::ActiveHigh;
        }
        else
        {
            alertPolarity = AlertPolarity::ActiveLow;
        }

        if (registerValue & (1u << 9))
        {
            alertThermostatMode = AlertThermostatMode::InterruptMode;
        }
        else
        {
            alertThermostatMode = AlertThermostatMode::ComparatorMode;
        }

        if (registerValue & (1u << 8))
        {
            shutdownControl = ShutdownControl::ShutdownMode;
        }
        else
        {
            shutdownControl = ShutdownControl::ContinuousConversion;
        }
    }


    std::string TMP75C_Configuration::toString() const
    {
        return std::format("SD: {}, TM: {}, POL: {}, FQ: {}, OS: {}",
            (uint32_t)shutdownControl,
            (uint32_t)alertThermostatMode,
            (uint32_t)alertPolarity,
            (uint32_t)faultQueue,
            (uint32_t)oneShot);
    }


    bool TMP75C_Configuration::fromString(std::string str)
    {
        bool result = false;

        const std::string pattern{ R"(SD:\s*([01]{1})\s*,\s*TM:\s*([01]{1})\s*,\s*POL:\s*([01]{1})\s*,\s*FQ:\s*([0123]{1})\s*,\s*OS:\s*([01]{1}))" };
        const std::regex regexConfiguration(pattern, std::regex::ECMAScript);

        std::smatch match;

        if (std::regex_search(str, match, regexConfiguration) == true)
        {
            shutdownControl = ShutdownControl(std::stoi(match.str(1)));
            alertThermostatMode = AlertThermostatMode(std::stoi(match.str(2)));
            alertPolarity = AlertPolarity(std::stoi(match.str(3)));
            faultQueue = FaultQueue(std::stoi(match.str(4)));
            oneShot = OneShot(std::stoi(match.str(5)));

            result = true;
        }

        return result;
    }
} // namespace I2C_Device
