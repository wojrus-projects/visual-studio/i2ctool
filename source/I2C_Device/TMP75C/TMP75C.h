//
// https://www.ti.com/product/TMP75C
//

#pragma once

#include <string>

#include "I2C_Device_Base.h"


namespace I2C_Device
{
    struct TMP75C_Configuration final
    {
        // Bit 13: OS
        enum class OneShot : uint8_t
        {
            ContinuousConversionMode,
            OneShotMode
        };

        // Bit 12-11: FQ
        enum class FaultQueue : uint8_t
        {
            Wait_1_Fault,
            Wait_2_Fault,
            Wait_4_Fault,
            Wait_6_Fault
        };

        // Bit 10: POL
        enum class AlertPolarity : uint8_t
        {
            ActiveLow,
            ActiveHigh
        };

        // Bit 9: TM
        enum class AlertThermostatMode : uint8_t
        {
            ComparatorMode,
            InterruptMode
        };

        // Bit 8: SD
        enum class ShutdownControl : uint8_t
        {
            ContinuousConversion,
            ShutdownMode
        };

        OneShot oneShot = OneShot::ContinuousConversionMode;
        FaultQueue faultQueue = FaultQueue::Wait_1_Fault;
        AlertPolarity alertPolarity = AlertPolarity::ActiveLow;
        AlertThermostatMode alertThermostatMode = AlertThermostatMode::ComparatorMode;
        ShutdownControl shutdownControl = ShutdownControl::ContinuousConversion;

        uint16_t toRegister() const;
        void fromRegister(uint16_t registerValue);
        std::string toString() const;
        bool fromString(std::string str);
    };


    class TMP75C final : public I2C_Device_Base
    {
    public:
        TMP75C(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, uint8_t i2cSlaveAddress)
            : I2C_Device_Base{ "TMP75C", pI2CInterface, i2cSlaveAddress }
        {
        }

        ~TMP75C() override = default;

        // Disallow copying and moving objects.
        TMP75C(const TMP75C&) = delete;
        TMP75C& operator=(const TMP75C&) = delete;
        TMP75C(TMP75C&&) = delete;
        TMP75C& operator=(TMP75C&&) = delete;

        //
        // Low level API.
        //

        // Pointer register values.
        enum class RegisterAddress : uint8_t
        {
            Temperature = 0x0,
            Configuration,
            T_low,
            T_high,
            OneShot
        };

        I2C_Interface::I2C_Interface_Status writeRegister(RegisterAddress address, uint16_t value);
        I2C_Interface::I2C_Interface_Status readRegister(RegisterAddress address, uint16_t& value);

        //
        // High level API.
        //

        // Temperature measurement range.
        static constexpr float TEMPERATURE_MIN = -55.0f;
        static constexpr float TEMPERATURE_MAX = +127.9375f;

        static constexpr float convertRegisterToTemperature(uint16_t registerValue);
        static constexpr uint16_t convertTemperatureToRegister(float temperature);

        I2C_Interface::I2C_Interface_Status startOneShotConversion();
        I2C_Interface::I2C_Interface_Status readTemperature(float& temperature);
        I2C_Interface::I2C_Interface_Status setTemperatureLimitLow(float temperature);
        I2C_Interface::I2C_Interface_Status setTemperatureLimitHigh(float temperature);
        I2C_Interface::I2C_Interface_Status getTemperatureLimitLow(float& temperature);
        I2C_Interface::I2C_Interface_Status getTemperatureLimitHigh(float& temperature);
        I2C_Interface::I2C_Interface_Status setConfiguration(const TMP75C_Configuration& configuration);
        I2C_Interface::I2C_Interface_Status getConfiguration(TMP75C_Configuration& configuration);
    };
} // namespace I2C_Device
