//
// Generic EEPROM.
//

#pragma once

#include <string>
#include <vector>
#include <chrono>

#include "I2C_Device_Memory_Base.h"


namespace I2C_Device
{
    struct EEPROM_Descriptor final
    {
        std::string_view name;
        std::string_view manufacturer;
        uint8_t i2cAddressDefault{};
        uint32_t sizeBytes{};
        uint32_t pointerSizeBits{};
        uint32_t pointerExtensionSizeBits{};
        uint32_t pointerExtensionOffsetBit{};
        uint32_t pageSizeBytes{};
        std::chrono::milliseconds writeTime{};
    };


    class EEPROM final : public I2C_Device_Memory_Base
    {
    public:
        EEPROM(std::string name, std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, uint8_t i2cSlaveAddress);

        ~EEPROM() override = default;

        // Disallow copying and moving objects.
        EEPROM(const EEPROM&) = delete;
        EEPROM& operator=(const EEPROM&) = delete;
        EEPROM(EEPROM&&) = delete;
        EEPROM& operator=(EEPROM&&) = delete;

        I2C_Interface::I2C_Interface_Status read(uint32_t memoryAddress, size_t readLength, size_t& readLengthActual, uint8_t* pData) override;
        I2C_Interface::I2C_Interface_Status write(uint32_t memoryAddress, size_t writeLength, size_t& writeLengthActual, const uint8_t* pData) override;
        I2C_Interface::I2C_Interface_Status fill(uint32_t memoryAddress, size_t memoryLength, uint8_t byte) override;

        static bool isSupported(std::string name);
        static std::vector<std::string> getSupportedList();

    private:
        static constexpr size_t DATA_CHUNK_SIZE_MAX = 512;
        static constexpr size_t PAGE_SIZE_MAX = 256;
        static constexpr size_t POINTER_REGISTER_SIZE_MIN = 1;
        static constexpr size_t POINTER_REGISTER_SIZE_MAX = 4;

        const EEPROM_Descriptor* pMemoryDescriptor{};

        struct HardwareAddress
        {
            uint8_t i2cAddress8bit{};
            uint32_t pointerRegister{};
        };

        HardwareAddress convertAddressLogicalToHardware(uint32_t logicalAddress);
        uint32_t getPointerSizeBytes();
        bool checkMemoryRange(uint32_t memoryAddress, size_t& memoryLength);
        void pointerToBytes(uint32_t pointerRegister, uint8_t* pBuffer);
        void writeDelay();
        I2C_Interface::I2C_Interface_Status setPointer(uint32_t value);
        I2C_Interface::I2C_Interface_Status writeChunk(uint32_t memoryAddress, size_t chunkSize, const uint8_t* pData);
        I2C_Interface::I2C_Interface_Status readChunk(uint32_t memoryAddress, size_t chunkSize, uint8_t* pData);
    };
} // namespace I2C_Device
