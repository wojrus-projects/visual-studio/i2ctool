#include <cassert>
#include <map>
#include <vector>
#include <algorithm>
#include <thread>

#include <spdlog.h>

#include "EEPROM.h"


namespace I2C_Device
{
    using namespace std::chrono_literals;


    static const std::map<std::string_view, EEPROM_Descriptor> EEPROM_Descriptors
    {
        {
            "24AA00",
            EEPROM_Descriptor
            {
                .name{ "24AA00" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 16,
                .pointerSizeBits = 4,
                .pointerExtensionSizeBits = 0,
                .pointerExtensionOffsetBit = 0,
                .pageSizeBytes = 8,
                .writeTime = 5ms
            }
        },

        {
            "24AA01",
            EEPROM_Descriptor
            {
                .name{ "24AA01" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 128,
                .pointerSizeBits = 7,
                .pointerExtensionSizeBits = 0,
                .pointerExtensionOffsetBit = 0,
                .pageSizeBytes = 8,
                .writeTime = 5ms
            }
        },

        {
            "24AA02",
            EEPROM_Descriptor
            {
                .name{ "24AA02" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 256,
                .pointerSizeBits = 8,
                .pointerExtensionSizeBits = 0,
                .pointerExtensionOffsetBit = 0,
                .pageSizeBytes = 8,
                .writeTime = 5ms
            }
        },

        {
            "24AA04",
            EEPROM_Descriptor
            {
                .name{ "24AA04" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 512,
                .pointerSizeBits = 8,
                .pointerExtensionSizeBits = 1,
                .pointerExtensionOffsetBit = 1,
                .pageSizeBytes = 16,
                .writeTime = 5ms
            }
        },

        {
            "24AA08",
            EEPROM_Descriptor
            {
                .name{ "24AA08" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 1 * 1024,
                .pointerSizeBits = 8,
                .pointerExtensionSizeBits = 2,
                .pointerExtensionOffsetBit = 1,
                .pageSizeBytes = 16,
                .writeTime = 5ms
            }
        },

        {
            "24AA16",
            EEPROM_Descriptor
            {
                .name{ "24AA16" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 2 * 1024,
                .pointerSizeBits = 8,
                .pointerExtensionSizeBits = 3,
                .pointerExtensionOffsetBit = 1,
                .pageSizeBytes = 16,
                .writeTime = 5ms
            }
        },

        {
            "24AA32A",
            EEPROM_Descriptor
            {
                .name{ "24AA32A" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 4 * 1024,
                .pointerSizeBits = 12,
                .pointerExtensionSizeBits = 0,
                .pointerExtensionOffsetBit = 0,
                .pageSizeBytes = 32,
                .writeTime = 5ms
            }
        },

        {
            "24AA64",
            EEPROM_Descriptor
            {
                .name{ "24AA64" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 8 * 1024,
                .pointerSizeBits = 13,
                .pointerExtensionSizeBits = 0,
                .pointerExtensionOffsetBit = 0,
                .pageSizeBytes = 32,
                .writeTime = 5ms
            }
        },

        {
            "24AA128",
            EEPROM_Descriptor
            {
                .name{ "24AA128" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 16 * 1024,
                .pointerSizeBits = 14,
                .pointerExtensionSizeBits = 0,
                .pointerExtensionOffsetBit = 0,
                .pageSizeBytes = 64,
                .writeTime = 5ms
            }
        },

        {
            "24AA256",
            EEPROM_Descriptor
            {
                .name{ "24AA256" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 32 * 1024,
                .pointerSizeBits = 15,
                .pointerExtensionSizeBits = 0,
                .pointerExtensionOffsetBit = 0,
                .pageSizeBytes = 64,
                .writeTime = 5ms
            }
        },

        {
            "24AA512",
            EEPROM_Descriptor
            {
                .name{ "24AA512" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 64 * 1024,
                .pointerSizeBits = 16,
                .pointerExtensionSizeBits = 0,
                .pointerExtensionOffsetBit = 0,
                .pageSizeBytes = 128,
                .writeTime = 5ms
            }
        },

        {
            "AT24CM02",
            EEPROM_Descriptor
            {
                .name{ "AT24CM02" },
                .manufacturer{ "Microchip" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 256 * 1024,
                .pointerSizeBits = 16,
                .pointerExtensionSizeBits = 2,
                .pointerExtensionOffsetBit = 1,
                .pageSizeBytes = 256,
                .writeTime = 5ms
            }
        },

        {
            "24M01",
            EEPROM_Descriptor
            {
                .name{ "24M01" },
                .manufacturer{ "ST" },
                .i2cAddressDefault = 0xA0,
                .sizeBytes = 128 * 1024,
                .pointerSizeBits = 16,
                .pointerExtensionSizeBits = 1,
                .pointerExtensionOffsetBit = 1,
                .pageSizeBytes = 256,
                .writeTime = 5ms
            }
        }
    };


    EEPROM::EEPROM(std::string name, std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, uint8_t i2cSlaveAddress)
        : I2C_Device_Memory_Base{ name, pI2CInterface, i2cSlaveAddress }
    {
        std::transform(name.begin(), name.end(), this->name.begin(), ::toupper);

        this->pMemoryDescriptor = &EEPROM_Descriptors.at(this->name);
    }


    I2C_Interface::I2C_Interface_Status EEPROM::read(uint32_t memoryAddress, size_t readLength, size_t& readLengthActual, uint8_t* pData)
    {
        I2C_Interface::I2C_Interface_Status i2cStatus = I2C_Interface::I2C_Interface_Status::Success;

        SPDLOG_DEBUG("Read: address=0x{:08X}, length={:d}",
            memoryAddress, readLength);

        if (checkMemoryRange(memoryAddress, readLength) != true)
        {
            return I2C_Interface::I2C_Interface_Status::InvalidArgument;
        }

        const auto [_, readLengthMax] = pI2CInterface->getReadLengthRange();

        const size_t chunkSizeMax = std::min((size_t)readLengthMax, DATA_CHUNK_SIZE_MAX);

        for (uint32_t readAddress = memoryAddress; readAddress < (memoryAddress + readLength); readAddress += (uint32_t)chunkSizeMax)
        {
            const size_t dataRemaining = (memoryAddress + readLength) - readAddress;
            const size_t chunkSize = std::min(chunkSizeMax, dataRemaining);

            i2cStatus = readChunk(readAddress, chunkSize, pData);
            if (i2cStatus != I2C_Interface::I2C_Interface_Status::Success)
            {
                return i2cStatus;
            }

            pData += chunkSize;
        }

        readLengthActual = readLength;

        return i2cStatus;
    }


    I2C_Interface::I2C_Interface_Status EEPROM::write(uint32_t memoryAddress, size_t writeLength, size_t& writeLengthActual, const uint8_t* pData)
    {
        I2C_Interface::I2C_Interface_Status i2cStatus = I2C_Interface::I2C_Interface_Status::Success;

        SPDLOG_DEBUG("Write: address=0x{:08X}, length={:d}",
            memoryAddress, writeLength);

        if (checkMemoryRange(memoryAddress, writeLength) != true)
        {
            return I2C_Interface::I2C_Interface_Status::InvalidArgument;
        }

        const auto [_, writeLengthMax] = pI2CInterface->getWriteLengthRange();

        const size_t chunkSizeMax = std::min((size_t)writeLengthMax, DATA_CHUNK_SIZE_MAX);

        for (uint32_t writeAddress = memoryAddress; writeAddress < (memoryAddress + writeLength); writeAddress += (uint32_t)chunkSizeMax)
        {
            const size_t dataRemaining = (memoryAddress + writeLength) - writeAddress;
            const size_t chunkSize = std::min(chunkSizeMax, dataRemaining);

            i2cStatus = writeChunk(writeAddress, chunkSize, pData);
            if (i2cStatus != I2C_Interface::I2C_Interface_Status::Success)
            {
                return i2cStatus;
            }
            
            pData += chunkSize;
        }

        writeLengthActual = writeLength;

        return i2cStatus;
    }


    I2C_Interface::I2C_Interface_Status EEPROM::fill(uint32_t memoryAddress, size_t memoryLength, uint8_t byte)
    {
        I2C_Interface::I2C_Interface_Status i2cStatus = I2C_Interface::I2C_Interface_Status::Success;

        SPDLOG_DEBUG("Fill: address=0x{:08X}, length={:d}, byte=0x{:02X}",
            memoryAddress, memoryLength, byte);

        if (checkMemoryRange(memoryAddress, memoryLength) != true)
        {
            return I2C_Interface::I2C_Interface_Status::InvalidArgument;
        }

        const auto [_, writeLengthMax] = pI2CInterface->getWriteLengthRange();

        const size_t chunkSizeMax = std::min((size_t)writeLengthMax, DATA_CHUNK_SIZE_MAX);

        uint8_t chunkBuffer[DATA_CHUNK_SIZE_MAX];

        std::fill(chunkBuffer, chunkBuffer + sizeof(chunkBuffer), byte);

        for (uint32_t writeAddress = memoryAddress; writeAddress < (memoryAddress + memoryLength); writeAddress += (uint32_t)chunkSizeMax)
        {
            const size_t dataRemaining = (memoryAddress + memoryLength) - writeAddress;
            const size_t chunkSize = std::min(chunkSizeMax, dataRemaining);

            i2cStatus = writeChunk(writeAddress, chunkSize, chunkBuffer);
            if (i2cStatus != I2C_Interface::I2C_Interface_Status::Success)
            {
                return i2cStatus;
            }
        }

        return i2cStatus;
    }


    I2C_Interface::I2C_Interface_Status EEPROM::writeChunk(uint32_t memoryAddress, size_t chunkSize, const uint8_t* pData)
    {
        I2C_Interface::I2C_Interface_Status i2cStatus = I2C_Interface::I2C_Interface_Status::Success;

        const auto [_, i2cWriteLengthMax] = pI2CInterface->getWriteLengthRange();
        const uint32_t pointerSize = getPointerSizeBytes();
        const size_t i2cPayloadLengthMax = i2cWriteLengthMax - pointerSize;
        const size_t pageSize = pMemoryDescriptor->pageSizeBytes;

        uint8_t pageBuffer[POINTER_REGISTER_SIZE_MAX + PAGE_SIZE_MAX];

        assert(i2cWriteLengthMax > pointerSize);

        while (chunkSize > 0)
        {
            const auto hwAddress = convertAddressLogicalToHardware(memoryAddress);
            const size_t pageWriteAvailable = pageSize - (memoryAddress % pageSize);
            const size_t pageWriteLength = std::min({ chunkSize, pageWriteAvailable, i2cPayloadLengthMax });
            const size_t chunkRemaining = chunkSize - pageWriteLength;

            pointerToBytes(hwAddress.pointerRegister, &pageBuffer[0]);
            memcpy(&pageBuffer[pointerSize], pData, pageWriteLength);

            SPDLOG_DEBUG("Write page: address=0x{:08X}, length={:d}, page available={:d}, chunk remaining={:d}, pointer=0x{:08X}, I2C address=0x{:02X}, data:{:a}",
                memoryAddress, pageWriteLength, pageWriteAvailable, chunkRemaining,
                hwAddress.pointerRegister, hwAddress.i2cAddress8bit,
                spdlog::to_hex(&pageBuffer[pointerSize], &pageBuffer[pointerSize] + pageWriteLength));

            i2cStatus = pI2CInterface->write(hwAddress.i2cAddress8bit, &pageBuffer[0], pointerSize + pageWriteLength);
            if (i2cStatus != I2C_Interface::I2C_Interface_Status::Success)
            {
                return i2cStatus;
            }

            writeDelay();

            memoryAddress += (uint32_t)pageWriteLength;
            chunkSize -= pageWriteLength;
            pData += pageWriteLength;
        }

        return i2cStatus;
    }


    I2C_Interface::I2C_Interface_Status EEPROM::readChunk(uint32_t memoryAddress, size_t chunkSize, uint8_t* pData)
    {
        I2C_Interface::I2C_Interface_Status i2cStatus = I2C_Interface::I2C_Interface_Status::Success;

        SPDLOG_DEBUG("Read chunk: address=0x{:08X}, length={:d}", memoryAddress, chunkSize);

        const auto hwAddress = convertAddressLogicalToHardware(memoryAddress);

        i2cStatus = setPointer(hwAddress.pointerRegister);
        if (i2cStatus != I2C_Interface::I2C_Interface_Status::Success)
        {
            return i2cStatus;
        }

        i2cStatus = pI2CInterface->read(hwAddress.i2cAddress8bit, pData, chunkSize);

        return i2cStatus;
    }


    uint32_t EEPROM::getPointerSizeBytes()
    {
        return (pMemoryDescriptor->pointerSizeBits / 8) + ((pMemoryDescriptor->pointerSizeBits % 8) != 0);
    }


    I2C_Interface::I2C_Interface_Status EEPROM::setPointer(uint32_t value)
    {
        const uint32_t pointerSize = getPointerSizeBytes();

        uint8_t pointerBuffer[POINTER_REGISTER_SIZE_MAX]{};

        pointerToBytes(value, pointerBuffer);

        return pI2CInterface->write(i2cSlaveAddress, pointerBuffer, pointerSize);
    }


    void EEPROM::pointerToBytes(uint32_t pointerRegister, uint8_t* pBuffer)
    {
        const uint32_t pointerSize = getPointerSizeBytes();

        assert(pointerSize >= POINTER_REGISTER_SIZE_MIN && pointerSize <= POINTER_REGISTER_SIZE_MAX);

        switch (pointerSize)
        {
            case 1:
                pBuffer[0] = (uint8_t)(pointerRegister >> 0);
                break;

            case 2:
                pBuffer[0] = (uint8_t)(pointerRegister >> 8);
                pBuffer[1] = (uint8_t)(pointerRegister >> 0);
                break;

            case 3:
                pBuffer[0] = (uint8_t)(pointerRegister >> 16);
                pBuffer[1] = (uint8_t)(pointerRegister >> 8);
                pBuffer[2] = (uint8_t)(pointerRegister >> 0);
                break;

            case 4:
                pBuffer[0] = (uint8_t)(pointerRegister >> 24);
                pBuffer[1] = (uint8_t)(pointerRegister >> 16);
                pBuffer[2] = (uint8_t)(pointerRegister >> 8);
                pBuffer[3] = (uint8_t)(pointerRegister >> 0);
                break;

            default:
                std::terminate();
        }
    }


    void EEPROM::writeDelay()
    {
        std::this_thread::sleep_for(pMemoryDescriptor->writeTime);
    }


    EEPROM::HardwareAddress EEPROM::convertAddressLogicalToHardware(uint32_t logicalAddress)
    {
        HardwareAddress hwAddress{};

        const uint32_t memoryBankCount = 1u << pMemoryDescriptor->pointerExtensionSizeBits;
        const uint32_t memoryBankSize = pMemoryDescriptor->sizeBytes / memoryBankCount;
        const uint32_t memoryBank = logicalAddress / memoryBankSize;

        assert(memoryBankCount >= 1 && memoryBankCount <= 16);
        assert(memoryBank >= 0 && memoryBank <= 15);
        assert((1u << pMemoryDescriptor->pointerSizeBits) == memoryBankSize);

        hwAddress.i2cAddress8bit = memoryBank << pMemoryDescriptor->pointerExtensionOffsetBit;
        hwAddress.i2cAddress8bit |= this->i2cSlaveAddress;

        hwAddress.pointerRegister = logicalAddress % memoryBankSize;

        return hwAddress;
    }


    bool EEPROM::checkMemoryRange(uint32_t memoryAddress, size_t& memoryLength)
    {
        const size_t eepromSize = pMemoryDescriptor->sizeBytes;

        if ((memoryAddress >= eepromSize) || (memoryLength > eepromSize))
        {
            SPDLOG_WARN("Memory address=0x{:08X} or read/write length={} exceed EEPROM size={} bytes",
                memoryAddress, memoryLength, eepromSize);

            return false;
        }

        const size_t memoryAvailable = eepromSize - memoryAddress;
        const size_t memoryLengthActual = std::min(memoryLength, memoryAvailable);

        if (memoryLength != memoryLengthActual)
        {
            SPDLOG_WARN("Truncated read/write length: requested: {}, actual: {} byte(s)",
                memoryLength, memoryLengthActual);
        }

        memoryLength = memoryLengthActual;

        return true;
    }


    bool EEPROM::isSupported(std::string name)
    {
        std::transform(name.begin(), name.end(), name.begin(), ::toupper);

        return EEPROM_Descriptors.contains(name);
    }


    std::vector<std::string> EEPROM::getSupportedList()
    {
        std::vector<std::string> names;

        for (const auto& descriptor : EEPROM_Descriptors)
        {
            names.push_back(
                std::format("{: <12} {: <10} {} bytes",
                descriptor.second.manufacturer,
                descriptor.second.name,
                descriptor.second.sizeBytes));
        }

        std::sort(names.begin(), names.end());

        return names;
    }
} // namespace I2C_Device
