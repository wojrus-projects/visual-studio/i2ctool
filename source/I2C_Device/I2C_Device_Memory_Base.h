#pragma once

#include <string>

#include "I2C_Device_Base.h"


namespace I2C_Device
{
    class I2C_Device_Memory_Base : public I2C_Device_Base
    {
    public:
        I2C_Device_Memory_Base(std::string name, std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, uint8_t i2cSlaveAddress)
            : I2C_Device_Base{ name, pI2CInterface, i2cSlaveAddress }
        {
        }

        virtual ~I2C_Device_Memory_Base() override = default;

        virtual I2C_Interface::I2C_Interface_Status read(uint32_t memoryAddress, size_t readLength, size_t& readLengthActual, uint8_t* pData) = 0;
        virtual I2C_Interface::I2C_Interface_Status write(uint32_t memoryAddress, size_t writeLength, size_t& writeLengthActual, const uint8_t* pData) = 0;
        virtual I2C_Interface::I2C_Interface_Status fill(uint32_t memoryAddress, size_t memoryLength, uint8_t byte) = 0;
    };
} // namespace I2C_Device
