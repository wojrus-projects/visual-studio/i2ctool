#pragma once

#include <cstdint>
#include <memory>

#include "I2C_Interface/I2C_Interface_Base.h"


namespace I2C_Device
{
    class I2C_Device_Base
    {
    public:
        I2C_Device_Base(std::string name, std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, uint8_t i2cSlaveAddress)
            : name{ name },
            pI2CInterface{ pI2CInterface },
            i2cSlaveAddress{ i2cSlaveAddress }
        {
        }

        virtual ~I2C_Device_Base() = default;

        std::string getName()
        {
            return name;
        }

    protected:
        std::string name;
        std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface;
        uint8_t i2cSlaveAddress{};
    };
} // namespace I2C_Device
