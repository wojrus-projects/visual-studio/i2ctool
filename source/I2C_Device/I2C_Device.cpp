#include <algorithm>
#include <cctype>

#include "I2C_Device.h"


namespace I2C_Device
{
    std::shared_ptr<I2C_Device_Base> I2CDeviceMake(std::string name,
        std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface,
        uint8_t i2cSlaveAddress)
    {
        if (EEPROM::isSupported(name) == true)
        {
            return std::make_shared<EEPROM>(name, pI2CInterface, i2cSlaveAddress);
        }
        else
        {
            std::transform(name.begin(), name.end(), name.begin(), ::tolower);

            if (name == "tmp75c")
            {
                return std::make_shared<TMP75C>(pI2CInterface, i2cSlaveAddress);
            }
            else if (name == "mcp3426")
            {
                return std::make_shared<MCP3426>(pI2CInterface, i2cSlaveAddress);
            }
            else if (name == "pca6408a")
            {
                return std::make_shared<PCA6408A>(pI2CInterface, i2cSlaveAddress);
            }
            else if (name == "si5351")
            {
                return std::make_shared<Si5351>(pI2CInterface, i2cSlaveAddress);
            }
            else
            {
                return {};
            }
        }
    }
} // namespace I2C_Device
