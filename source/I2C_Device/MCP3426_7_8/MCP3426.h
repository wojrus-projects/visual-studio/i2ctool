//
// https://www.microchip.com/en-us/product/MCP3426
// https://www.microchip.com/en-us/product/MCP3427
// https://www.microchip.com/en-us/product/MCP3428
//

#pragma once

#include <string>
#include <tuple>

#include "I2C_Device_Base.h"


namespace I2C_Device
{
    union MCP3426_Configuration final
    {
        // Bit 1-0: G1-G0
        enum class Gain : uint8_t
        {
            x1, // default
            x2,
            x4,
            x8
        };

        // Bit 3-2: S1-S0
        enum class SampleRate : uint8_t
        {
            SPS_240, // default
            SPS_60,
            SPS_15
        };

        // Bit 4: ~O/C
        enum class ConversionMode : uint8_t
        {
            OneShot,
            Continuous // default
        };

        // Bit 6-5: C1-C0
        enum class Channel : uint8_t
        {
            Channel_1, // default
            Channel_2,
            Channel_3, // MCP3428 only, treated as "00" by the MCP3426/MCP3427.
            Channel_4  // MCP3428 only, treated as "01" by the MCP3426/MCP3427.
        };

        // Bit 7: ~RDY
        enum class ReadyFlag : uint8_t
        {
            // Bit has two functions:
            
            // 1. Reading RDY bit with the read command:
            DataUpdated = 0,
            DataNotUpdated = 1, // default

            // 2. Writing RDY bit with the write command:
            OneShotIgnore = 0,
            OneShotStart = 1
        };

        struct Bits
        {
            uint8_t gain             : 2 = 0b00;
            uint8_t sampleRate       : 2 = 0b00;
            uint8_t conversionMode   : 1 = 0b1;
            uint8_t channelSelection : 2 = 0b00;
            uint8_t readyFlagInv     : 1 = 0b1;
        } bits;

        uint8_t byte;

        MCP3426_Configuration();
        MCP3426_Configuration(uint8_t byte);
        MCP3426_Configuration(Gain gain, SampleRate sampleRate, ConversionMode mode, Channel channel, ReadyFlag readyFlag);

        std::tuple<Gain, SampleRate, ConversionMode, Channel, ReadyFlag> get() const;

        std::string toString() const;
        bool fromString(std::string str);
    };


    static_assert(sizeof(MCP3426_Configuration) == sizeof(uint8_t));


    class MCP3426 final : public I2C_Device_Base
    {
    public:
        MCP3426(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, uint8_t i2cSlaveAddress)
            : I2C_Device_Base{ "MCP3426", pI2CInterface, i2cSlaveAddress }
        {
        }

        ~MCP3426() override = default;

        // Disallow copying and moving objects.
        MCP3426(const MCP3426&) = delete;
        MCP3426& operator=(const MCP3426&) = delete;
        MCP3426(MCP3426&&) = delete;
        MCP3426& operator=(MCP3426&&) = delete;

        static constexpr float V_REF = 2.048f;

        I2C_Interface::I2C_Interface_Status setConfiguration(const MCP3426_Configuration& configuration);
        I2C_Interface::I2C_Interface_Status readData(uint16_t& adcData, MCP3426_Configuration& configuration);
        I2C_Interface::I2C_Interface_Status readDataWaitForReady(uint16_t& adcData, MCP3426_Configuration& configuration);
        I2C_Interface::I2C_Interface_Status generalCallReset();
        I2C_Interface::I2C_Interface_Status generalCallLatchAddress();
        I2C_Interface::I2C_Interface_Status generalCallStartOneShotConversion();

        static float convertDataToVoltage(uint16_t adcData, const MCP3426_Configuration& configuration);

    private:
        enum class GeneralCallType : uint8_t
        {
            Reset = 0x06,
            LatchAddress = 0x04,
            StartOneShotConversion = 0x08
        };

        I2C_Interface::I2C_Interface_Status generalCall(GeneralCallType type);
    };
} // namespace I2C_Device
