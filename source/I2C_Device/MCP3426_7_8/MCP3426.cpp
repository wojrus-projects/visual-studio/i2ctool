﻿#include <regex>
#include <algorithm>
#include <map>
#include <chrono>

#include "MCP3426.h"


namespace I2C_Device
{
    MCP3426_Configuration::MCP3426_Configuration()
        : bits{}
    {
    }


    MCP3426_Configuration::MCP3426_Configuration(uint8_t byte)
        : byte{ byte }
    {
    }


    MCP3426_Configuration::MCP3426_Configuration(Gain gain, SampleRate sampleRate, ConversionMode mode, Channel channel, ReadyFlag readyFlag)
    {
        gain = std::clamp(gain, Gain::x1, Gain::x8);
        sampleRate = std::clamp(sampleRate, SampleRate::SPS_240, SampleRate::SPS_15);
        mode = std::clamp(mode, ConversionMode::OneShot, ConversionMode::Continuous);
        channel = std::clamp(channel, Channel::Channel_1, Channel::Channel_4);
        readyFlag = std::clamp(readyFlag, ReadyFlag::DataUpdated, ReadyFlag::DataNotUpdated);

        bits.gain = uint8_t(gain) & 0x3;
        bits.sampleRate = uint8_t(sampleRate) & 0x3;
        bits.conversionMode = uint8_t(mode) & 0x1;
        bits.channelSelection = uint8_t(channel) & 0x3;
        bits.readyFlagInv = uint8_t(readyFlag) & 0x1;
    }


    std::tuple<MCP3426_Configuration::Gain,
        MCP3426_Configuration::SampleRate,
        MCP3426_Configuration::ConversionMode,
        MCP3426_Configuration::Channel,
        MCP3426_Configuration::ReadyFlag> MCP3426_Configuration::get() const
    {
        const Gain gain = Gain(bits.gain);
        const SampleRate sampleRate = SampleRate(bits.sampleRate);
        const ConversionMode mode = ConversionMode(bits.conversionMode);
        const Channel channel = Channel(bits.channelSelection);
        const ReadyFlag readyFlag = ReadyFlag(bits.readyFlagInv);

        return { gain, sampleRate, mode, channel, readyFlag };
    }


    std::string MCP3426_Configuration::toString() const
    {
        return std::format("gain: {}, sr: {}, oc: {}, ch: {}, rdy: {}",
            (uint32_t)bits.gain,
            (uint32_t)bits.sampleRate,
            (uint32_t)bits.conversionMode,
            (uint32_t)bits.channelSelection,
            (uint32_t)bits.readyFlagInv);
    }


    bool MCP3426_Configuration::fromString(std::string str)
    {
        bool result = false;

        const std::string pattern{ R"(gain:\s*([0123]{1})\s*,\s*sr:\s*([0123]{1})\s*,\s*oc:\s*([01]{1})\s*,\s*ch:\s*([0123]{1})\s*,\s*rdy:\s*([01]{1}))" };
        const std::regex regexConfiguration(pattern, std::regex::ECMAScript);

        std::smatch match;

        if (std::regex_search(str, match, regexConfiguration) == true)
        {
            bits.gain = uint8_t(std::stoi(match.str(1)));
            bits.sampleRate = uint8_t(std::stoi(match.str(2)));
            bits.conversionMode = uint8_t(std::stoi(match.str(3)));
            bits.channelSelection = uint8_t(std::stoi(match.str(4)));
            bits.readyFlagInv = uint8_t(std::stoi(match.str(5)));

            result = true;
        }

        return result;
    }


    I2C_Interface::I2C_Interface_Status MCP3426::setConfiguration(const MCP3426_Configuration& configuration)
    {
        const uint8_t buffer[sizeof(MCP3426_Configuration)] { configuration.byte };

        return this->pI2CInterface->write(this->i2cSlaveAddress, buffer);
    }


    I2C_Interface::I2C_Interface_Status MCP3426::readData(uint16_t& adcData, MCP3426_Configuration& configuration)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        uint8_t buffer[sizeof(uint16_t) + sizeof(MCP3426_Configuration)];

        i2cInterfaceStatus = this->pI2CInterface->read(this->i2cSlaveAddress, buffer, sizeof(buffer));
        if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
        {
            adcData = ((uint16_t)buffer[0] << 8) | ((uint16_t)buffer[1] << 0);
            configuration.byte = buffer[2];
        }

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status MCP3426::readDataWaitForReady(uint16_t& adcData, MCP3426_Configuration& configuration)
    {
        using namespace std::chrono_literals;

        constexpr std::chrono::milliseconds TIMEOUT_ms = 100ms;

        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        MCP3426_Configuration configurationBuffer;
        uint16_t adcDataBuffer;

        const auto timeStart = std::chrono::high_resolution_clock::now();

        do
        {
            i2cInterfaceStatus = readData(adcDataBuffer, configurationBuffer);
            if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
            {
                if (configurationBuffer.bits.readyFlagInv == 0)
                {
                    adcData = adcDataBuffer;
                    configuration = configurationBuffer;

                    return I2C_Interface::I2C_Interface_Status::Success;
                }
                else
                {
                    const auto timeDelta = std::chrono::high_resolution_clock::now() - timeStart;

                    if (timeDelta > TIMEOUT_ms)
                    {
                        return I2C_Interface::I2C_Interface_Status::I2CTimeout;
                    }
                }
            }
        } while (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success);

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status MCP3426::generalCallReset()
    {
        return generalCall(GeneralCallType::Reset);
    }


    I2C_Interface::I2C_Interface_Status MCP3426::generalCallLatchAddress()
    {
        return generalCall(GeneralCallType::LatchAddress);
    }


    I2C_Interface::I2C_Interface_Status MCP3426::generalCallStartOneShotConversion()
    {
        return generalCall(GeneralCallType::StartOneShotConversion);
    }


    I2C_Interface::I2C_Interface_Status MCP3426::generalCall(GeneralCallType type)
    {
        const uint8_t buffer[2]{ 0x00, (uint8_t)type };

        return this->pI2CInterface->write(this->i2cSlaveAddress, buffer);
    }


    float MCP3426::convertDataToVoltage(uint16_t adcData, const MCP3426_Configuration& configuration)
    {
        static const std::map<MCP3426_Configuration::Gain, uint8_t> gainMap
        {
            { MCP3426_Configuration::Gain::x1, 1 },
            { MCP3426_Configuration::Gain::x2, 2 },
            { MCP3426_Configuration::Gain::x4, 4 },
            { MCP3426_Configuration::Gain::x8, 8 }
        };

        static const std::map<MCP3426_Configuration::SampleRate, uint8_t> resolutionMap
        {
            { MCP3426_Configuration::SampleRate::SPS_240, 12 },
            { MCP3426_Configuration::SampleRate::SPS_60, 14 },
            { MCP3426_Configuration::SampleRate::SPS_15, 16 }
        };

        const auto [gain, sampleRate, mode, channel, readyFlag] = configuration.get();
        const uint16_t adcCodeMax = (1u << (resolutionMap.at(sampleRate) - 1u)) - 1u;
        const float voltage = (V_REF * (float)(int16_t)adcData) / ((float)(adcCodeMax + 1u) * (float)gainMap.at(gain));

        return voltage;
    }
} // namespace I2C_Device
