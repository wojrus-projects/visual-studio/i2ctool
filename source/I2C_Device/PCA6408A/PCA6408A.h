//
// https://www.nxp.com/products/interfaces/ic-spi-i3c-interface-devices/general-purpose-i-o-gpio/low-voltage-8-bit-ic-bus-and-smbus-i-o-expander-with-interrupt-output-reset-and-configuration-registers:PCA6408A
//

#pragma once

#include "I2C_Device_Base.h"


namespace I2C_Device
{
    class PCA6408A final : public I2C_Device_Base
    {
    public:
        PCA6408A(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, uint8_t i2cSlaveAddress)
            : I2C_Device_Base{ "PCA6408A", pI2CInterface, i2cSlaveAddress }
        {
        }

        ~PCA6408A() override = default;

        // Disallow copying and moving objects.
        PCA6408A(const PCA6408A&) = delete;
        PCA6408A& operator=(const PCA6408A&) = delete;
        PCA6408A(PCA6408A&&) = delete;
        PCA6408A& operator=(PCA6408A&&) = delete;

        I2C_Interface::I2C_Interface_Status readInput(uint8_t& inputStateBitmap);
        I2C_Interface::I2C_Interface_Status readOutput(uint8_t& outputStateBitmap);
        I2C_Interface::I2C_Interface_Status writeOutput(uint8_t outputStateBitmap);
        I2C_Interface::I2C_Interface_Status readPolarityInversion(uint8_t& inversionStateBitmap);
        I2C_Interface::I2C_Interface_Status writePolarityInversion(uint8_t inversionStateBitmap);
        I2C_Interface::I2C_Interface_Status readDirection(uint8_t& directionStateBitmap);
        I2C_Interface::I2C_Interface_Status writeDirection(uint8_t directionStateBitmap);

    private:
        enum class PointerRegister : uint8_t
        {
            InputPort,
            OutputPort,
            PolarityInversion,
            Configuration
        };
    };
} // namespace I2C_Device
