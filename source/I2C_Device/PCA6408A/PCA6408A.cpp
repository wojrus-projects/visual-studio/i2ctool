﻿#include "PCA6408A.h"


namespace I2C_Device
{
    I2C_Interface::I2C_Interface_Status PCA6408A::readInput(uint8_t& inputStateBitmap)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        const uint8_t buffer[1] { (uint8_t)PointerRegister::InputPort };

        i2cInterfaceStatus = this->pI2CInterface->write(this->i2cSlaveAddress, buffer);
        if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
        {
            i2cInterfaceStatus = this->pI2CInterface->read(this->i2cSlaveAddress, &inputStateBitmap, 1);
        }

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status PCA6408A::readOutput(uint8_t& outputStateBitmap)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        const uint8_t buffer[1]{ (uint8_t)PointerRegister::OutputPort };

        i2cInterfaceStatus = this->pI2CInterface->write(this->i2cSlaveAddress, buffer);
        if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
        {
            i2cInterfaceStatus = this->pI2CInterface->read(this->i2cSlaveAddress, &outputStateBitmap, 1);
        }

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status PCA6408A::writeOutput(uint8_t outputStateBitmap)
    {
        const uint8_t buffer[2]{ (uint8_t)PointerRegister::OutputPort, outputStateBitmap };

        return this->pI2CInterface->write(this->i2cSlaveAddress, buffer);
    }


    I2C_Interface::I2C_Interface_Status PCA6408A::readPolarityInversion(uint8_t& inversionStateBitmap)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        const uint8_t buffer[1]{ (uint8_t)PointerRegister::PolarityInversion };

        i2cInterfaceStatus = this->pI2CInterface->write(this->i2cSlaveAddress, buffer);
        if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
        {
            i2cInterfaceStatus = this->pI2CInterface->read(this->i2cSlaveAddress, &inversionStateBitmap, 1);
        }

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status PCA6408A::writePolarityInversion(uint8_t inversionStateBitmap)
    {
        const uint8_t buffer[2]{ (uint8_t)PointerRegister::PolarityInversion, inversionStateBitmap };

        return this->pI2CInterface->write(this->i2cSlaveAddress, buffer);
    }


    I2C_Interface::I2C_Interface_Status PCA6408A::readDirection(uint8_t& directionStateBitmap)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus;
        const uint8_t buffer[1]{ (uint8_t)PointerRegister::Configuration };

        i2cInterfaceStatus = this->pI2CInterface->write(this->i2cSlaveAddress, buffer);
        if (i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
        {
            i2cInterfaceStatus = this->pI2CInterface->read(this->i2cSlaveAddress, &directionStateBitmap, 1);
        }

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status PCA6408A::writeDirection(uint8_t directionStateBitmap)
    {
        const uint8_t buffer[2]{ (uint8_t)PointerRegister::Configuration, directionStateBitmap };

        return this->pI2CInterface->write(this->i2cSlaveAddress, buffer);
    }
} // namespace I2C_Device
