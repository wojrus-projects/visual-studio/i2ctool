#pragma once

#include <string>
#include <memory>

#include "I2C_Device_Base.h"

#include "TMP75C/TMP75C.h"
#include "MCP3426_7_8/MCP3426.h"
#include "PCA6408A/PCA6408A.h"
#include "EEPROM/EEPROM.h"
#include "Si5351/Si5351.h"


namespace I2C_Device
{
    std::shared_ptr<I2C_Device_Base> I2CDeviceMake(std::string name,
        std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface,
        uint8_t i2cSlaveAddress);
}
