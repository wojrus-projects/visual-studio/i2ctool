//
// https://www.skyworksinc.com/en/Products/Timing/CMOS-Clock-Generators/si5351a-b-gt
//

#pragma once

#include "I2C_Device_Base.h"


namespace I2C_Device
{
    class Si5351 final : public I2C_Device_Base
    {
    public:
        Si5351(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, uint8_t i2cSlaveAddress);

        ~Si5351() override = default;

        // Disallow copying and moving objects.
        Si5351(const Si5351&) = delete;
        Si5351& operator=(const Si5351&) = delete;
        Si5351(Si5351&&) = delete;
        Si5351& operator=(Si5351&&) = delete;

        I2C_Interface::I2C_Interface_Status initialize();
        I2C_Interface::I2C_Interface_Status enableOutput(uint32_t outputNumber, bool outputState);
        I2C_Interface::I2C_Interface_Status setOutput(uint32_t outputNumber, uint32_t frequency_Hz, uint32_t driverStrength, bool invert);

    private:
        static inline Si5351* pCallbackDevice{};
        static bool callbackWriteRegister(uint8_t reg, uint8_t value);
        static bool callbackReadRegister(uint8_t reg, uint8_t* pValue);
    };
} // namespace I2C_Device
