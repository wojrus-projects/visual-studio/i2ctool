﻿#include <cassert>

#include <spdlog.h>

#include "Si5351.h"
#include "Third_Party/Si5351_lib.h"


namespace I2C_Device
{
    Si5351::Si5351(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, uint8_t i2cSlaveAddress)
        : I2C_Device_Base{ "Si5351", pI2CInterface, i2cSlaveAddress }
    {
        // Class Si5351 must be used as singleton.
        pCallbackDevice = this;

        si5351_Register_I2C_Callback(callbackWriteRegister, callbackReadRegister);
    }


    I2C_Interface::I2C_Interface_Status Si5351::initialize()
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus = I2C_Interface::I2C_Interface_Status::Success;

        SPDLOG_DEBUG("Initialize");

        si5351_Init(0);

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status Si5351::enableOutput(uint32_t outputNumber, bool outputState)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus = I2C_Interface::I2C_Interface_Status::Success;

        SPDLOG_DEBUG("Enable output={}, state={}", outputNumber, (uint32_t)outputState);

        const uint8_t outputGroupBits = 1u << outputNumber;
        const uint8_t stateBits = (outputState != false) << outputNumber;

        si5351_EnableOutputs(outputGroupBits, stateBits);

        return i2cInterfaceStatus;
    }


    I2C_Interface::I2C_Interface_Status Si5351::setOutput(uint32_t outputNumber, uint32_t frequency_Hz, uint32_t driverStrength, bool invert)
    {
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus = I2C_Interface::I2C_Interface_Status::Success;

        SPDLOG_DEBUG("Set output={}: frequency={} Hz, driver strength={}, invert={}",
            outputNumber, frequency_Hz, driverStrength, invert);

        if (outputNumber == 0)
        {
            si5351_SetupCLK0(frequency_Hz, (si5351DriveStrength_t)driverStrength, invert);
        }
        else if (outputNumber == 2)
        {
            si5351_SetupCLK2(frequency_Hz, (si5351DriveStrength_t)driverStrength, invert);
        }
        else
        {
            i2cInterfaceStatus = I2C_Interface::I2C_Interface_Status::InvalidArgument;
        }

        return i2cInterfaceStatus;
    }


    bool Si5351::callbackWriteRegister(uint8_t reg, uint8_t value)
    {
        const uint8_t buffer[2] = { reg, value };

        assert(pCallbackDevice != nullptr);

        SPDLOG_DEBUG("Write register 0x{:02X} value 0x{:02X}", reg, value);

        pCallbackDevice->pI2CInterface->write(pCallbackDevice->i2cSlaveAddress, buffer, sizeof(buffer));

        return true;
    }


    bool Si5351::callbackReadRegister(uint8_t reg, uint8_t* pValue)
    {
        const uint8_t buffer[1] = { reg };

        assert(pCallbackDevice != nullptr);

        SPDLOG_DEBUG("Read register 0x{:02X} start", reg);

        pCallbackDevice->pI2CInterface->read(pCallbackDevice->i2cSlaveAddress, pValue, 1, reg, 1);

        SPDLOG_DEBUG("Read register 0x{:02X} value 0x{:02X}", reg, *pValue);

        return true;
    }
} // namespace I2C_Device
