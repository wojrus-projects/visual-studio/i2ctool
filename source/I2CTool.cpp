//
// I2C bus universal diagnostic CLI tool.
//

#define VERSION_STR "v1.7"


#include <cstdint>
#include <cassert>
#include <iostream>
#include <format>
#include <vector>
#include <algorithm>
#include <memory>
#include <optional>
#include <bit>
#include <bitset>

#include <spdlog.h>
#include <CLI/CLI.hpp>

#include "Utilities.h"
#include "I2C_Interface/I2C_Interface.h"
#include "I2C_Device/I2C_Device.h"


static constexpr char HELP_HEADER[] =
    "I2C bus universal diagnostic CLI tool " VERSION_STR " (" __DATE__ ", " __TIME__ ")\n"
    "Author: W.R. <rwxrwx@interia.pl>\n"
    "License: MIT\n"
    "Supported I2C masters: Silabs CP2112, FTDI FT260.\n";


using namespace std::chrono_literals;


/// <summary>
/// CLI parsed arguments.
/// </summary>
struct CLIArguments
{
    uint32_t CommandRepeatCounter = 0;
    std::chrono::milliseconds CommandRepeatDelay = 0ms;

    struct I2CMaster
    {
        std::string InterfaceName;
        uint8_t InterfaceInstance = 0;
        uint32_t Bitrate = 0;
        uint8_t SlaveAddress8bit = 0;
    } I2CMaster{};

    struct CommandBasic
    {
        std::string WriteDataRaw;
        std::vector<uint8_t> WriteData;
        uint32_t ReadLength = 0;
        uint32_t MemoryAddressLength = 0;
        uint32_t MemoryAddress = 0;
        std::chrono::seconds Timeout = 0s;
    } CommandBasic{};

    struct CommandDevice
    {
        std::string DeviceName;
        std::string DeviceFunction;
        std::string DeviceFunctionMode;
        std::string DeviceFunctionArgs;
    } CommandDevice{};

    struct CommandDeviceEEPROM
    {
        bool ListDeviceType = false;
        std::string DeviceType;
        std::string DeviceFunction;
        uint32_t MemoryAddress = 0;
        uint32_t MemoryLength = 0;
        std::string WriteDataRaw;
        std::vector<uint8_t> WriteData;
        std::string DataFileName;
        uint8_t FillByte = 0;
    } CommandDeviceEEPROM{};

    struct CommandDeviceSi5351
    {
        std::optional<bool> Initialize;
        std::optional<uint32_t> Output;
        std::optional<bool> Enable;
        std::optional<bool> Invert;
        std::optional<uint32_t> Strength;
        std::optional<uint32_t> Frequency;
    } CommandDeviceSi5351{};
};


/// <summary>
/// CLI formatter.
/// </summary>
class CustomCLIFormatter : public CLI::Formatter
{
public:
    CustomCLIFormatter() : Formatter() {}
    std::string make_option_opts(const CLI::Option*) const override { return ""; }
};


/// <summary>
/// CLI command base class.
/// </summary>
class ICommand
{
protected:
    std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface;
    CLIArguments cliArgs{};

    explicit ICommand(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, CLIArguments cliArgs)
        : pI2CInterface{ pI2CInterface },
        cliArgs{ cliArgs }
    {
    }

public:
    virtual ~ICommand() = default;

    virtual bool execute() = 0;

    bool openInterface()
    {
        using namespace I2C_Interface;

        I2C_Interface_Status i2cStatus;

        std::cout << std::format("Open I2C interface {} instance: {}\n", pI2CInterface->getName(), cliArgs.I2CMaster.InterfaceInstance);

        i2cStatus = pI2CInterface->open(cliArgs.I2CMaster.InterfaceInstance);
        if (i2cStatus != I2C_Interface_Status::Success)
        {
            std::cout << std::format("I2C interface error: {}\n", I2C_Interface::toString(i2cStatus));
            return false;
        }

        std::cout << std::format("Set SCL frequency: {} Hz\n", cliArgs.I2CMaster.Bitrate);

        i2cStatus = pI2CInterface->setBitRate(cliArgs.I2CMaster.Bitrate);
        if (i2cStatus != I2C_Interface_Status::Success)
        {
            std::cout << std::format("I2C interface error: {}\n", I2C_Interface::toString(i2cStatus));
            return false;
        }

        return true;
    }

    void closeInterface()
    {
        std::cout << std::format("Close I2C interface instance: {}\n", cliArgs.I2CMaster.InterfaceInstance);

        pI2CInterface->close();
    }
};


/// <summary>
/// Command DETECT.
/// </summary>
class CommandDetect final : public ICommand
{
public:
    explicit CommandDetect(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, CLIArguments cliArgs)
        : ICommand{ pI2CInterface, cliArgs }
    {
    }

    virtual ~CommandDetect() override = default;

    virtual bool execute() override
    {
        using namespace I2C_Interface;

        std::vector<uint8_t> foundAddresses7bit;
        std::vector<uint8_t> foundAddresses8bit;

        std::cout << std::format("Detect I2C slaves...\n");

        for (uint32_t slaveAddress = SLAVE_ADDRESS_MIN; slaveAddress <= SLAVE_ADDRESS_MAX; slaveAddress += 2)
        {
            I2C_Interface_Status i2cStatus;
            uint8_t dataBuffer[1];

            //
            // Test 1 (try slave read).
            //

            i2cStatus = pI2CInterface->read(slaveAddress, dataBuffer, sizeof(dataBuffer));

            if (i2cStatus == I2C_Interface_Status::Success)
            {
                foundAddresses7bit.push_back(slaveAddress >> 1);
                foundAddresses8bit.push_back(slaveAddress);
            }
            else if (i2cStatus == I2C_Interface_Status::I2CNack)
            {
                //
                // Test 2 (try slave write).
                //

                dataBuffer[0] = 0x00;

                i2cStatus = pI2CInterface->write(slaveAddress, dataBuffer, sizeof(dataBuffer));

                if (i2cStatus == I2C_Interface_Status::Success)
                {
                    foundAddresses7bit.push_back(slaveAddress >> 1);
                    foundAddresses8bit.push_back(slaveAddress);
                }
                else if (i2cStatus == I2C_Interface_Status::I2CNack)
                {
                    // Not found by ACK, skip address.
                }
                else
                {
                    std::cout << std::format("I2C interface error: {}\n", I2C_Interface::toString(i2cStatus));
                    return false;
                }
            }
            else
            {
                std::cout << std::format("I2C interface error: {}\n", I2C_Interface::toString(i2cStatus));
                return false;
            }
        }

        std::cout << std::format("Found {} slave(s)\n", foundAddresses8bit.size());

        if (foundAddresses8bit.size() > 0)
        {
            std::cout << "Address list:\n";
            Utilities::printI2CAddressTable(foundAddresses8bit);
        }

        std::cout << '\n';
        std::cout << "Address list (7-bit):\n";
        Utilities::printCArray(foundAddresses7bit);
        std::cout << '\n';
        std::cout << "Address list (8-bit):\n";
        Utilities::printCArray(foundAddresses8bit);
        std::cout << '\n';

        return true;
    }
};


/// <summary>
/// Command WRITE.
/// </summary>
class CommandWrite final : public ICommand
{
public:
    explicit CommandWrite(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, CLIArguments cliArgs)
        : ICommand{ pI2CInterface, cliArgs }
    {
    }

    virtual ~CommandWrite() override = default;

    virtual bool execute() override
    {
        using namespace I2C_Interface;

        std::cout << std::format("Write {} byte(s) to I2C slave (8-bit address: 0x{:02X})...\n",
            cliArgs.CommandBasic.WriteData.size(), cliArgs.I2CMaster.SlaveAddress8bit);

        std::cout << '\n';
        Utilities::printData(cliArgs.CommandBasic.WriteData);
        std::cout << '\n';
        Utilities::printCArray(cliArgs.CommandBasic.WriteData);
        std::cout << '\n';

        const I2C_Interface_Status i2cStatus = pI2CInterface->write(cliArgs.I2CMaster.SlaveAddress8bit, cliArgs.CommandBasic.WriteData);

        if (i2cStatus != I2C_Interface_Status::Success)
        {
            std::cout << std::format("I2C interface error: {}\n", I2C_Interface::toString(i2cStatus));
        }

        return (i2cStatus == I2C_Interface_Status::Success);
    }
};


/// <summary>
/// Command READ.
/// </summary>
class CommandRead final : public ICommand
{
public:
    explicit CommandRead(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, CLIArguments cliArgs)
        : ICommand{ pI2CInterface, cliArgs }
    {
    }

    virtual ~CommandRead() override = default;

    virtual bool execute() override
    {
        using namespace I2C_Interface;

        auto dataBuffer = std::make_unique_for_overwrite<uint8_t[]>(cliArgs.CommandBasic.ReadLength);

        std::cout << std::format("Read {} byte(s) from I2C slave (8-bit address: 0x{:02X})...\n",
            cliArgs.CommandBasic.ReadLength, cliArgs.I2CMaster.SlaveAddress8bit);

        const I2C_Interface_Status i2cStatus = pI2CInterface->read(cliArgs.I2CMaster.SlaveAddress8bit, dataBuffer.get(), cliArgs.CommandBasic.ReadLength, 0, 0);

        if (i2cStatus == I2C_Interface_Status::Success)
        {
            std::cout << '\n';
            Utilities::printData(dataBuffer.get(), cliArgs.CommandBasic.ReadLength);
            std::cout << '\n';
            Utilities::printCArray(dataBuffer.get(), cliArgs.CommandBasic.ReadLength);
            std::cout << '\n';
        }
        else
        {
            std::cout << std::format("I2C interface error: {}\n", I2C_Interface::toString(i2cStatus));
        }

        return (i2cStatus == I2C_Interface_Status::Success);
    }
};


/// <summary>
/// Command READ_WITH_ADDRESS.
/// </summary>
class CommandReadWithAddress final : public ICommand
{
public:
    explicit CommandReadWithAddress(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, CLIArguments cliArgs)
        : ICommand{ pI2CInterface, cliArgs }
    {
    }

    virtual ~CommandReadWithAddress() override = default;

    virtual bool execute() override
    {
        using namespace I2C_Interface;

        auto dataBuffer = std::make_unique_for_overwrite<uint8_t[]>(cliArgs.CommandBasic.ReadLength);

        std::cout << std::format("Read {} byte(s) from I2C slave (8-bit address: 0x{:02X}, MAL: {} byte(s), MA: 0x{:08X})...\n",
            cliArgs.CommandBasic.ReadLength, cliArgs.I2CMaster.SlaveAddress8bit, cliArgs.CommandBasic.MemoryAddressLength, cliArgs.CommandBasic.MemoryAddress);

        //
        // Truncate Memory Address and send MSB first.
        //

        switch (cliArgs.CommandBasic.MemoryAddressLength)
        {
            case 1:
                if (cliArgs.CommandBasic.MemoryAddress > 0x0000'00FFul)
                {
                    cliArgs.CommandBasic.MemoryAddress &= 0x0000'00FFul;
                    std::cout << std::format("Truncate MA to 0x{:02X}\n", cliArgs.CommandBasic.MemoryAddress);
                }
                break;

            case 2:
                if (cliArgs.CommandBasic.MemoryAddress > 0x0000'FFFFul)
                {
                    cliArgs.CommandBasic.MemoryAddress &= 0x0000'FFFFul;
                    std::cout << std::format("Truncate MA to 0x{:04X}\n", cliArgs.CommandBasic.MemoryAddress);
                }
                break;

            case 3:
                if (cliArgs.CommandBasic.MemoryAddress > 0x00FF'FFFFul)
                {
                    cliArgs.CommandBasic.MemoryAddress &= 0x00FF'FFFFul;
                    std::cout << std::format("Truncate MA to 0x{:06X}\n", cliArgs.CommandBasic.MemoryAddress);
                }
                break;

            case 4:
                if (cliArgs.CommandBasic.MemoryAddress > 0xFFFF'FFFFul)
                {
                    cliArgs.CommandBasic.MemoryAddress &= 0xFFFF'FFFFul;
                    std::cout << std::format("Truncate MA to 0x{:08X}\n", cliArgs.CommandBasic.MemoryAddress);
                }
                break;

            default:
                throw std::invalid_argument("invalid Memory Address Length");
        }

        static_assert(std::endian::native == std::endian::little, "big-endian is not implemented");

        uint8_t* const pBegin = (uint8_t*)&cliArgs.CommandBasic.MemoryAddress;
        uint8_t* const pEnd = pBegin + cliArgs.CommandBasic.MemoryAddressLength;

        std::reverse(pBegin, pEnd);

        const I2C_Interface_Status i2cStatus = pI2CInterface->read(cliArgs.I2CMaster.SlaveAddress8bit,
            dataBuffer.get(),
            cliArgs.CommandBasic.ReadLength,
            cliArgs.CommandBasic.MemoryAddress,
            cliArgs.CommandBasic.MemoryAddressLength);

        if (i2cStatus == I2C_Interface_Status::Success)
        {
            std::cout << '\n';
            Utilities::printData(dataBuffer.get(), cliArgs.CommandBasic.ReadLength);
            std::cout << '\n';
            Utilities::printCArray(dataBuffer.get(), cliArgs.CommandBasic.ReadLength);
            std::cout << '\n';
        }
        else
        {
            std::cout << std::format("I2C interface error: {}\n", I2C_Interface::toString(i2cStatus));
        }

        return (i2cStatus == I2C_Interface_Status::Success);
    }
};


/// <summary>
/// Command DEVICE.
/// </summary>
class CommandDevice final : public ICommand
{
public:
    std::shared_ptr<I2C_Device::I2C_Device_Base> pI2CDevice;

    explicit CommandDevice(std::shared_ptr<I2C_Interface::I2C_Interface_Base> pI2CInterface, CLIArguments cliArgs,
        std::shared_ptr<I2C_Device::I2C_Device_Base> pI2CDevice)
        : ICommand{ pI2CInterface, cliArgs },
        pI2CDevice{ pI2CDevice }
    {
    }

    virtual ~CommandDevice() override = default;

    struct ProcessResult
    {
        enum class ProcessStatus
        {
            Success,
            UnknownError,
            FileError,
            ArgumentError,
            I2CError
        };

        ProcessStatus processStatus{ ProcessStatus::UnknownError };
        I2C_Interface::I2C_Interface_Status i2cInterfaceStatus{ I2C_Interface::I2C_Interface_Status::UnknownError };
    };

    virtual bool execute() override
    {
        bool executeResult = false;
        ProcessResult processResult{};

        const auto deviceName{ pI2CDevice->getName() };

        if (I2C_Device::EEPROM::isSupported(deviceName) == true)
        {
            processResult = Device_EEPROM_Process();
        }
        else
        {
            if (deviceName == "TMP75C")
            {
                processResult = Device_TMP75C_Process();
            }
            else if (deviceName == "MCP3426")
            {
                processResult = Device_MCP3426_Process();
            }
            else if (deviceName == "PCA6408A")
            {
                processResult = Device_PCA6408A_Process();
            }
            else if (deviceName == "Si5351")
            {
                processResult = Device_Si5351_Process();
            }
            else
            {
                std::cout << std::format("I2C device error: unknown name '{}'\n", deviceName);
                return false;
            }
        }

        if (processResult.processStatus == ProcessResult::ProcessStatus::Success)
        {
            executeResult = true;
        }
        else if (processResult.processStatus == ProcessResult::ProcessStatus::I2CError)
        {
            std::cout << std::format("I2C interface error: {}\n", I2C_Interface::toString(processResult.i2cInterfaceStatus));
        }
        else if (processResult.processStatus == ProcessResult::ProcessStatus::ArgumentError)
        {
            std::cout << "Invalid function, mode or argument\n";
        }
        else if (processResult.processStatus == ProcessResult::ProcessStatus::FileError)
        {
            // Pass
        }
        else
        {
            throw std::domain_error("invalid process status");
        }

        return executeResult;
    }

    ProcessResult Device_TMP75C_Process()
    {
        using namespace I2C_Device;

        auto pTemperatureSensor = std::dynamic_pointer_cast<TMP75C>(pI2CDevice);
        assert(pTemperatureSensor != nullptr);

        const auto& deviceFunction = cliArgs.CommandDevice.DeviceFunction;
        const auto& deviceFunctionMode = cliArgs.CommandDevice.DeviceFunctionMode;
        const auto& deviceFunctionArgs = cliArgs.CommandDevice.DeviceFunctionArgs;

        ProcessResult processResult{};
        float temperature = 0.0f;
        TMP75C_Configuration configuration;

        if (deviceFunction == "get" || deviceFunction == "set")
        {
            if (deviceFunctionMode == "temp")
            {
                if (deviceFunction == "get")
                {
                    processResult.i2cInterfaceStatus = pTemperatureSensor->readTemperature(temperature);
                    if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                    {
                        std::cout << std::format("Temperature: {} C\n", temperature);
                        processResult.processStatus = ProcessResult::ProcessStatus::Success;
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                    }
                }
                else
                {
                    processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                }
            }
            else if (deviceFunctionMode == "limit-l")
            {
                if (deviceFunction == "get")
                {
                    processResult.i2cInterfaceStatus = pTemperatureSensor->getTemperatureLimitLow(temperature);
                    if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                    {
                        std::cout << std::format("Get temperature limit low: {} C\n", temperature);
                        processResult.processStatus = ProcessResult::ProcessStatus::Success;
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                    }
                }
                else
                {
                    try
                    {
                        temperature = std::stof(deviceFunctionArgs);
                        processResult.i2cInterfaceStatus = pTemperatureSensor->setTemperatureLimitLow(temperature);
                        if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                        {
                            std::cout << std::format("Set temperature limit low: {} C\n", temperature);
                            processResult.processStatus = ProcessResult::ProcessStatus::Success;
                        }
                        else
                        {
                            processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                        }
                    }
                    catch (std::invalid_argument)
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                    }
                }
            }
            else if (deviceFunctionMode == "limit-h")
            {
                if (deviceFunction == "get")
                {
                    processResult.i2cInterfaceStatus = pTemperatureSensor->getTemperatureLimitHigh(temperature);
                    if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                    {
                        std::cout << std::format("Get temperature limit high: {} C\n", temperature);
                        processResult.processStatus = ProcessResult::ProcessStatus::Success;
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                    }
                }
                else
                {
                    try
                    {
                        temperature = std::stof(deviceFunctionArgs);
                        processResult.i2cInterfaceStatus = pTemperatureSensor->setTemperatureLimitHigh(temperature);
                        if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                        {
                            std::cout << std::format("Set temperature limit high: {} C\n", temperature);
                            processResult.processStatus = ProcessResult::ProcessStatus::Success;
                        }
                        else
                        {
                            processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                        }
                    }
                    catch (std::invalid_argument)
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                    }
                }
            }
            else if (deviceFunctionMode == "conf")
            {
                if (deviceFunction == "get")
                {
                    processResult.i2cInterfaceStatus = pTemperatureSensor->getConfiguration(configuration);
                    if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                    {
                        std::cout << std::format("Configuration: {}\n", configuration.toString());
                        processResult.processStatus = ProcessResult::ProcessStatus::Success;
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                    }
                }
                else
                {
                    if (configuration.fromString(deviceFunctionArgs) == true)
                    {
                        processResult.i2cInterfaceStatus = pTemperatureSensor->setConfiguration(configuration);
                        if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                        {
                            std::cout << std::format("Set configuration: {}\n", configuration.toString());
                            processResult.processStatus = ProcessResult::ProcessStatus::Success;
                        }
                        else
                        {
                            processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                        }
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                    }
                }
            }
            else
            {
                processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
            }
        }
        else if (deviceFunction == "one-shot")
        {
            processResult.i2cInterfaceStatus = pTemperatureSensor->startOneShotConversion();
            if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
            {
                processResult.i2cInterfaceStatus = pTemperatureSensor->readTemperature(temperature);
                if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                {
                    std::cout << std::format("Temperature: {} C\n", temperature);
                    processResult.processStatus = ProcessResult::ProcessStatus::Success;
                }
                else
                {
                    processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                }
            }
            else
            {
                processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
            }
        }
        else
        {
            processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
        }

        return processResult;
    }

    ProcessResult Device_MCP3426_Process()
    {
        using namespace I2C_Device;

        auto pADC = std::dynamic_pointer_cast<MCP3426>(pI2CDevice);
        assert(pADC != nullptr);

        const auto& deviceFunction = cliArgs.CommandDevice.DeviceFunction;
        const auto& deviceFunctionMode = cliArgs.CommandDevice.DeviceFunctionMode;
        const auto& deviceFunctionArgs = cliArgs.CommandDevice.DeviceFunctionArgs;

        ProcessResult processResult{};
        uint16_t adcData = 0;
        MCP3426_Configuration configuration;

        if (deviceFunction == "get" || deviceFunction == "set")
        {
            if (deviceFunctionMode == "adc")
            {
                if (deviceFunction == "get")
                {
                    processResult.i2cInterfaceStatus = pADC->readDataWaitForReady(adcData, configuration);
                    if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                    {
                        std::cout << std::format("Configuration: '{0}', data: 0x{1:04X} / 0b{1:016b}, voltage: {2} V\n",
                            configuration.toString(), adcData, MCP3426::convertDataToVoltage(adcData, configuration));
                        processResult.processStatus = ProcessResult::ProcessStatus::Success;
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                    }
                }
                else
                {
                    processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                }
            }
            else if (deviceFunctionMode == "conf")
            {
                if (deviceFunction == "set")
                {
                    if (configuration.fromString(deviceFunctionArgs) == true)
                    {
                        processResult.i2cInterfaceStatus = pADC->setConfiguration(configuration);
                        if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                        {
                            std::cout << std::format("Set configuration: {}\n", configuration.toString());
                            processResult.processStatus = ProcessResult::ProcessStatus::Success;
                        }
                        else
                        {
                            processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                        }
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                    }
                }
                else
                {
                    processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                }
            }
            else
            {
                processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
            }
        }
        else if (deviceFunction == "call-reset")
        {
            processResult.i2cInterfaceStatus = pADC->generalCallReset();
            if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
            {
                std::cout << std::format("General call: reset\n");
                processResult.processStatus = ProcessResult::ProcessStatus::Success;
            }
            else
            {
                processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
            }
        }
        else if (deviceFunction == "call-latch")
        {
            processResult.i2cInterfaceStatus = pADC->generalCallLatchAddress();
            if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
            {
                std::cout << std::format("General call: latch address\n");
                processResult.processStatus = ProcessResult::ProcessStatus::Success;
            }
            else
            {
                processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
            }
        }
        else if (deviceFunction == "call-start")
        {
            processResult.i2cInterfaceStatus = pADC->generalCallStartOneShotConversion();
            if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
            {
                std::cout << std::format("General call: start one-shot conversion\n");
                processResult.processStatus = ProcessResult::ProcessStatus::Success;
            }
            else
            {
                processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
            }
        }
        else
        {
            processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
        }

        return processResult;
    }

    ProcessResult Device_PCA6408A_Process()
    {
        auto parseBits = [](std::string str) -> std::optional<uint8_t>
        {
            uint8_t value{};

            try
            {
                std::bitset<8> bitmap{ str };

                value = (uint8_t)bitmap.to_ulong();
            }
            catch (std::invalid_argument)
            {
                return {};
            }

            return { value };
        };

        using namespace I2C_Device;

        auto pGPIO = std::dynamic_pointer_cast<PCA6408A>(pI2CDevice);
        assert(pGPIO != nullptr);

        const auto& deviceFunction = cliArgs.CommandDevice.DeviceFunction;
        const auto& deviceFunctionMode = cliArgs.CommandDevice.DeviceFunctionMode;
        const auto& deviceFunctionArgs = cliArgs.CommandDevice.DeviceFunctionArgs;

        ProcessResult processResult{};

        if (deviceFunction == "get" || deviceFunction == "set")
        {
            if (deviceFunctionMode == "in")
            {
                if (deviceFunction == "get")
                {
                    uint8_t inputStateBitmap{};

                    processResult.i2cInterfaceStatus = pGPIO->readInput(inputStateBitmap);
                    if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                    {
                        std::cout << std::format("Get inputs: 0b{:08b}\n", inputStateBitmap);
                        processResult.processStatus = ProcessResult::ProcessStatus::Success;
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                    }
                }
                else
                {
                    processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                }
            }
            else if (deviceFunctionMode == "out")
            {
                if (deviceFunction == "set")
                {
                    auto bits = parseBits(deviceFunctionArgs);

                    if (bits)
                    {
                        processResult.i2cInterfaceStatus = pGPIO->writeOutput(*bits);
                        if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                        {
                            std::cout << std::format("Set outputs: 0b{:08b}\n", *bits);
                            processResult.processStatus = ProcessResult::ProcessStatus::Success;
                        }
                        else
                        {
                            processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                        }
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                    }
                }
                else
                {
                    uint8_t outputStateBitmap{};

                    processResult.i2cInterfaceStatus = pGPIO->readOutput(outputStateBitmap);
                    if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                    {
                        std::cout << std::format("Get outputs: 0b{:08b}\n", outputStateBitmap);
                        processResult.processStatus = ProcessResult::ProcessStatus::Success;
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                    }
                }
            }
            else if (deviceFunctionMode == "polarity")
            {
                if (deviceFunction == "set")
                {
                    auto bits = parseBits(deviceFunctionArgs);

                    if (bits)
                    {
                        processResult.i2cInterfaceStatus = pGPIO->writePolarityInversion(*bits);
                        if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                        {
                            std::cout << std::format("Set outputs: 0b{:08b}\n", *bits);
                            processResult.processStatus = ProcessResult::ProcessStatus::Success;
                        }
                        else
                        {
                            processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                        }
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                    }
                }
                else
                {
                    uint8_t inversionStateBitmap{};

                    processResult.i2cInterfaceStatus = pGPIO->readPolarityInversion(inversionStateBitmap);
                    if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                    {
                        std::cout << std::format("Get outputs: 0b{:08b}\n", inversionStateBitmap);
                        processResult.processStatus = ProcessResult::ProcessStatus::Success;
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                    }
                }
            }
            else if (deviceFunctionMode == "dir")
            {
                if (deviceFunction == "set")
                {
                    auto bits = parseBits(deviceFunctionArgs);

                    if (bits)
                    {
                        processResult.i2cInterfaceStatus = pGPIO->writeDirection(*bits);
                        if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                        {
                            std::cout << std::format("Set direction: 0b{:08b}\n", *bits);
                            processResult.processStatus = ProcessResult::ProcessStatus::Success;
                        }
                        else
                        {
                            processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                        }
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
                    }
                }
                else
                {
                    uint8_t directionStateBitmap{};

                    processResult.i2cInterfaceStatus = pGPIO->readDirection(directionStateBitmap);
                    if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
                    {
                        std::cout << std::format("Get direction: 0b{:08b}\n", directionStateBitmap);
                        processResult.processStatus = ProcessResult::ProcessStatus::Success;
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
                    }
                }
            }
            else
            {
                processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
            }
        }
        else
        {
            processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
        }

        return processResult;
    }

    ProcessResult Device_EEPROM_Process()
    {
        using namespace I2C_Device;
        using namespace I2C_Interface;

        auto pEEPROM = std::dynamic_pointer_cast<EEPROM>(pI2CDevice);
        assert(pEEPROM != nullptr);

        const auto& deviceFunction = cliArgs.CommandDeviceEEPROM.DeviceFunction;
        const auto& memoryAddress = cliArgs.CommandDeviceEEPROM.MemoryAddress;
        const auto& memoryLength = cliArgs.CommandDeviceEEPROM.MemoryLength;
        const auto& writeData = cliArgs.CommandDeviceEEPROM.WriteData;
        const auto& fileName = cliArgs.CommandDeviceEEPROM.DataFileName;
        const bool isFile = fileName.empty() == false;

        ProcessResult processResult{};

        if (deviceFunction == "rd")
        {
            auto dataBuffer = std::make_unique_for_overwrite<uint8_t[]>(memoryLength);

            std::cout << std::format("Read {} byte(s) at 0x{:08X} from EEPROM {} (8-bit address: 0x{:02X})...\n",
                memoryLength, memoryAddress,
                pEEPROM->getName(),
                cliArgs.I2CMaster.SlaveAddress8bit);

            size_t memoryLengthActual{};

            processResult.i2cInterfaceStatus = pEEPROM->read(memoryAddress, memoryLength, memoryLengthActual, dataBuffer.get());
            if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
            {
                if (memoryLengthActual != memoryLength)
                {
                    std::cout << std::format("Truncated read length: requested: {}, actual: {} byte(s)\n", memoryLength, memoryLengthActual);
                }

                if (isFile == true)
                {
                    if (memoryLengthActual > 0)
                    {
                        std::cout << std::format("Write data to file: '{}'\n", fileName);
                        
                        std::ofstream file(fileName, std::ios::out | std::ios::binary);

                        if (file.is_open() == false)
                        {
                            std::cout << std::format("File open error\n");

                            processResult.processStatus = ProcessResult::ProcessStatus::FileError;
                        }
                        else
                        {
                            if (!file.write((const char*)dataBuffer.get(), memoryLengthActual))
                            {
                                std::cout << std::format("File write error\n");

                                processResult.processStatus = ProcessResult::ProcessStatus::FileError;
                            }

                            file.close();

                            processResult.processStatus = ProcessResult::ProcessStatus::Success;
                        }
                    }
                    else
                    {
                        processResult.processStatus = ProcessResult::ProcessStatus::Success;
                    }
                }
                else
                {
                    if (memoryLengthActual > 0)
                    {
                        std::cout << '\n';
                        Utilities::printData(dataBuffer.get(), memoryLengthActual);
                        std::cout << '\n';
                        Utilities::printCArray(dataBuffer.get(), memoryLengthActual);
                        std::cout << '\n';
                    }

                    processResult.processStatus = ProcessResult::ProcessStatus::Success;
                }
            }
            else
            {
                processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
            }
        }
        else if (deviceFunction == "wr")
        {
            std::vector<uint8_t> dataBuffer;

            if (isFile == true)
            {
                std::cout << std::format("Read data from file: '{}'\n", fileName);

                dataBuffer.resize(memoryLength);
                std::ifstream file(fileName, std::ios::in | std::ios::binary);

                if (file.is_open() == false)
                {
                    std::cout << std::format("File open error\n");

                    processResult.processStatus = ProcessResult::ProcessStatus::FileError;
                    return processResult;
                }
                else
                {
                    if (!file.read((char*)dataBuffer.data(), dataBuffer.size()))
                    {
                        std::cout << std::format("File read error\n");

                        file.close();
                        processResult.processStatus = ProcessResult::ProcessStatus::FileError;
                        return processResult;
                    }

                    file.close();
                }
            }
            else
            {
                dataBuffer = writeData;
            }

            std::cout << std::format("Write {} byte(s) at 0x{:08X} to EEPROM {} (8-bit address: 0x{:02X})...\n",
                dataBuffer.size(), memoryAddress,
                pEEPROM->getName(),
                cliArgs.I2CMaster.SlaveAddress8bit);

            size_t writeLengthActual{};

            processResult.i2cInterfaceStatus = pEEPROM->write(memoryAddress, dataBuffer.size(), writeLengthActual, dataBuffer.data());
            if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
            {
                if (writeLengthActual != dataBuffer.size())
                {
                    std::cout << std::format("Truncated write length: requested: {}, actual: {} byte(s)\n", dataBuffer.size(), writeLengthActual);
                }

                if (isFile == false)
                {
                    if (writeLengthActual > 0)
                    {
                        std::cout << '\n';
                        Utilities::printData(dataBuffer.data(), writeLengthActual);
                        std::cout << '\n';
                        Utilities::printCArray(dataBuffer.data(), writeLengthActual);
                        std::cout << '\n';
                    }
                }

                processResult.processStatus = ProcessResult::ProcessStatus::Success;
            }
            else
            {
                processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
            }
        }
        else if (deviceFunction == "fill")
        {
            std::cout << std::format("Fill {} byte(s) with value 0x{:02X} at 0x{:08X} in EEPROM {} (8-bit address: 0x{:02X})...\n",
                memoryLength, cliArgs.CommandDeviceEEPROM.FillByte, memoryAddress,
                pEEPROM->getName(),
                cliArgs.I2CMaster.SlaveAddress8bit);

            processResult.i2cInterfaceStatus = pEEPROM->fill(memoryAddress, memoryLength, cliArgs.CommandDeviceEEPROM.FillByte);
            if (processResult.i2cInterfaceStatus == I2C_Interface::I2C_Interface_Status::Success)
            {
                processResult.processStatus = ProcessResult::ProcessStatus::Success;
            }
            else
            {
                processResult.processStatus = ProcessResult::ProcessStatus::I2CError;
            }
        }
        else
        {
            processResult.processStatus = ProcessResult::ProcessStatus::ArgumentError;
        }

        return processResult;
    }

    ProcessResult Device_Si5351_Process()
    {
        using namespace I2C_Device;
        using namespace I2C_Interface;

        auto pClockGenerator = std::dynamic_pointer_cast<Si5351>(pI2CDevice);
        assert(pClockGenerator != nullptr);

        ProcessResult processResult{ .processStatus = ProcessResult::ProcessStatus::Success };

        if (cliArgs.CommandDeviceSi5351.Initialize)
        {
            std::cout << std::format("Initialize Si5351\n");

            pClockGenerator->initialize();
        }

        if (cliArgs.CommandDeviceSi5351.Enable)
        {
            std::cout << std::format("Enable output: {}, state: {:d}\n",
                *cliArgs.CommandDeviceSi5351.Output,
                *cliArgs.CommandDeviceSi5351.Enable);

            pClockGenerator->enableOutput(*cliArgs.CommandDeviceSi5351.Output,
                *cliArgs.CommandDeviceSi5351.Enable);
        }

        if (cliArgs.CommandDeviceSi5351.Frequency && cliArgs.CommandDeviceSi5351.Strength && cliArgs.CommandDeviceSi5351.Invert)
        {
            std::cout << std::format("Set output: {}, frequency: {} Hz, strength: {}, invert: {:d}\n",
                *cliArgs.CommandDeviceSi5351.Output,
                *cliArgs.CommandDeviceSi5351.Frequency,
                *cliArgs.CommandDeviceSi5351.Strength,
                *cliArgs.CommandDeviceSi5351.Invert);

            pClockGenerator->setOutput(*cliArgs.CommandDeviceSi5351.Output,
                *cliArgs.CommandDeviceSi5351.Frequency,
                *cliArgs.CommandDeviceSi5351.Strength,
                *cliArgs.CommandDeviceSi5351.Invert);
        }

        return processResult;
    }
};

/// <summary>
/// Application main class.
/// </summary>
class I2CTool final
{
private:
    std::shared_ptr<ICommand> pCommand;
    CLIArguments cliArgs{};

public:
    I2CTool() = default;
    ~I2CTool() = default;

    // Disallow copying and moving objects.
    I2CTool(const I2CTool&) = delete;
    I2CTool& operator=(const I2CTool&) = delete;
    I2CTool(I2CTool&&) = delete;
    I2CTool& operator=(I2CTool&&) = delete;

    enum class CLIParseResult
    {
        Success,
        Error,
        Help
    };

    CLIParseResult parseCommand(int argc, char* argv[])
    {
        using namespace I2C_Interface;

        CLI::App app(HELP_HEADER);
        app.set_help_all_flag("--help-all", "Expand all help");

        auto formatter = std::make_shared<CustomCLIFormatter>();
        formatter->column_width(25);
        app.formatter(formatter);

        app.add_option("INTERFACE-NAME", cliArgs.I2CMaster.InterfaceName,
            "I2C interface name (CP2112 or FT260).")
            ->required()
            ->check(CLI::IsMember({"CP2112", "FT260"}, CLI::ignore_case));

        app.add_option("INTERFACE-INSTANCE", cliArgs.I2CMaster.InterfaceInstance,
            "I2C interface instance number (0-255, HEX/DEC format).")
            ->required()
            ->check(CLI::Range(0, 255));
    
        app.add_option("BITRATE", cliArgs.I2CMaster.Bitrate,
            "I2C interface bitrate in bps (HEX/DEC format).")
            ->required();

        auto optionTimeout = app.add_option("-t,--timeout", cliArgs.CommandBasic.Timeout,
            "I2C transfer timeout in seconds (1-60, HEX/DEC format).")
            ->check(CLI::Range(1, 60));

        auto optionRepeatCounter = app.add_option("--repeat", cliArgs.CommandRepeatCounter,
            "Command repeat counter.")
            ->check(CLI::Range(1, 10'000'000));

        auto optionRepeatDelay = app.add_option("--delay", cliArgs.CommandRepeatDelay,
            "Command repeat delay in ms.")
            ->check(CLI::Range(0ms, 5 * 60 * 1000ms));

        optionRepeatCounter->needs(optionRepeatDelay);
        optionRepeatDelay->needs(optionRepeatCounter);

        auto commandDetect = app.add_subcommand("detect", "Detect slave devices connected to I2C bus (by ACK check at write or read address).");
        auto commandWr     = app.add_subcommand("wr",     "Write data to I2C slave.");
        auto commandRd     = app.add_subcommand("rd",     "Read data from I2C slave (without memory address).");
        auto commandRda    = app.add_subcommand("rda",    "Read data from I2C slave (with memory address).");
        auto commandDev    = app.add_subcommand("dev",    "High level functions for various I2C slave devices.");

        app.require_subcommand(1);

        //
        // Command 'wr'.
        //

        commandWr->add_option("SLAVE-ADDRESS", cliArgs.I2CMaster.SlaveAddress8bit,
            "I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).")
            ->required()
            ->check(CLI::Range(0x02, 0xFE));

        commandWr->add_option("DATA", cliArgs.CommandBasic.WriteDataRaw,
            "Write data (quoted string with bytes in HEX format with optional '0x' prefix and optional separators: whitespaces, commas, colons and dashes).")
            ->required();

        //
        // Command 'rd'.
        //

        commandRd->add_option("SLAVE-ADDRESS", cliArgs.I2CMaster.SlaveAddress8bit,
            "I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).")
            ->required()
            ->check(CLI::Range(0x02, 0xFE));

        commandRd->add_option("READ-LENGTH", cliArgs.CommandBasic.ReadLength,
            "Data read length (HEX/DEC format).")
            ->required()
            ->check(CLI::Range(0x1u, 1024u * 1024u));

        //
        // Command 'rda'.
        //

        commandRda->add_option("SLAVE-ADDRESS", cliArgs.I2CMaster.SlaveAddress8bit,
            "I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).")
            ->required()
            ->check(CLI::Range(0x02, 0xFE));

        commandRda->add_option("READ-LENGTH", cliArgs.CommandBasic.ReadLength,
            "Data read length (HEX/DEC format).")
            ->required()
            ->check(CLI::Range(0x1u, 1024u * 1024u));

        commandRda->add_option("MEMORY-ADDRESS-LENGTH", cliArgs.CommandBasic.MemoryAddressLength,
            "Memory Address Length (1-4 bytes, HEX/DEC format).")
            ->required()
            ->check(CLI::Range(1, 4));

        commandRda->add_option("MEMORY-ADDRESS", cliArgs.CommandBasic.MemoryAddress,
            "Memory Address (0x00000000-0xFFFFFFFF, HEX/DEC format, send MSB first).")
            ->required()
            ->check(CLI::Range(0x0000'0000u, 0xFFFF'FFFFu));

        //
        // Command 'dev'.
        //
        
        commandDev->add_option("SLAVE-ADDRESS", cliArgs.I2CMaster.SlaveAddress8bit,
            "I2C slave 8-bit address (0x02-0xFE, HEX/DEC format).")
            ->required()
            ->check(CLI::Range(0x02, 0xFE));

        auto commandDev_TMP75C = commandDev->add_subcommand("TMP75C",
            "FUNCTION={get,set,one-shot} MODE={temp,limit-l,limit-h,conf} ARGS={temperature,configuration}.")
            ->ignore_case();

        auto commandDev_MCP3426 = commandDev->add_subcommand("MCP3426",
            "FUNCTION={get,set} MODE={adc,conf} ARGS={configuration}.")
            ->ignore_case();

        auto commandDev_PCA6408A = commandDev->add_subcommand("PCA6408A",
            "FUNCTION={get,set} MODE={in,out,polarity,dir} ARGS={value}.")
            ->ignore_case();

        auto commandDev_EEPROM = commandDev->add_subcommand("EEPROM",
            "--list -t TYPE -f FUNCTION={rd,wr,fill} -a MEMORY-ADDRESS -l MEMORY-LENGTH -d \"WRITE-DATA\" --file FILE -b FILL BYTE.")
            ->ignore_case();

        auto commandDev_Si5351 = commandDev->add_subcommand("Si5351",
            "--init -o OUTPUT -e ENABLE -i INVERT -s STRENGTH -f FREQUENCY.")
            ->ignore_case();

        commandDev->require_subcommand(1);

        //
        // Command 'dev::TMP75C'.
        //

        commandDev_TMP75C->add_option("FUNCTION", cliArgs.CommandDevice.DeviceFunction,
            "TMP75C function.")
            ->required()
            ->check(CLI::IsMember({"get", "set", "one-shot"}, CLI::ignore_case));

        commandDev_TMP75C->add_option("MODE", cliArgs.CommandDevice.DeviceFunctionMode,
            "TMP75C function mode.")
            ->check(CLI::IsMember({"temp", "limit-l", "limit-h", "conf"}, CLI::ignore_case));

        commandDev_TMP75C->add_option("ARGS", cliArgs.CommandDevice.DeviceFunctionArgs,
            "TMP75C function arguments.")
            ->ignore_case();

        //
        // Command 'dev::MCP3426'.
        //

        commandDev_MCP3426->add_option("FUNCTION", cliArgs.CommandDevice.DeviceFunction,
            "MCP3426 function.")
            ->required()
            ->check(CLI::IsMember({"get", "set", "call-reset", "call=latch", "call-start"}, CLI::ignore_case));

        commandDev_MCP3426->add_option("MODE", cliArgs.CommandDevice.DeviceFunctionMode,
            "MCP3426 function mode.")
            ->check(CLI::IsMember({"adc", "conf"}, CLI::ignore_case));

        commandDev_MCP3426->add_option("ARGS", cliArgs.CommandDevice.DeviceFunctionArgs,
            "MCP3426 function arguments.")
            ->ignore_case();

        //
        // Command 'dev::PCA6408A'.
        //

        commandDev_PCA6408A->add_option("FUNCTION", cliArgs.CommandDevice.DeviceFunction,
            "PCA6408A function.")
            ->required()
            ->check(CLI::IsMember({"get", "set"}, CLI::ignore_case));

        commandDev_PCA6408A->add_option("MODE", cliArgs.CommandDevice.DeviceFunctionMode,
            "PCA6408A function mode.")
            ->required()
            ->check(CLI::IsMember({"in", "out", "polarity", "dir"}, CLI::ignore_case));

        commandDev_PCA6408A->add_option("ARGS", cliArgs.CommandDevice.DeviceFunctionArgs,
            "PCA6408A function arguments.")
            ->ignore_case();

        //
        // Command 'dev::EEPROM'.
        //

        // CLI format for EEPROMs:
        // --list
        // -t TYPE -f rd   -a MEMORY-ADDRESS -l MEMORY-LENGTH
        // -t TYPE -f rd   -a MEMORY-ADDRESS -l MEMORY-LENGTH --file FILE-NAME
        // -t TYPE -f wr   -a MEMORY-ADDRESS -d "WRITE-DATA"
        // -t TYPE -f wr   -a MEMORY-ADDRESS -l MEMORY-LENGTH --file FILE-NAME
        // -t TYPE -f fill -a MEMORY-ADDRESS -l MEMORY-LENGTH -b FILL-BYTE

        commandDev_EEPROM->add_flag("--list", cliArgs.CommandDeviceEEPROM.ListDeviceType, "List available EEPROM types.");

        auto optionEEPROMType = commandDev_EEPROM->add_option("-t,--type", cliArgs.CommandDeviceEEPROM.DeviceType,
            "EEPROM type.")
            ->ignore_case();

        auto optionEEPROMOperation = commandDev_EEPROM->add_option("-f,--function", cliArgs.CommandDeviceEEPROM.DeviceFunction,
            "EEPROM function.")
            ->check(CLI::IsMember({"rd", "wr", "fill"}, CLI::ignore_case));

        auto optionEEPROMAddress = commandDev_EEPROM->add_option("-a,--address", cliArgs.CommandDeviceEEPROM.MemoryAddress,
            "Memory start address (0x0000'0000-0xFFFF'FFFF, HEX/DEC format).")
            ->check(CLI::Range(0x0000'0000u, 0xFFFF'FFFFu));

        auto optionEEPROMLength = commandDev_EEPROM->add_option("-l,--length", cliArgs.CommandDeviceEEPROM.MemoryLength,
            "Memory length (1-1048576, HEX/DEC format).")
            ->check(CLI::Range(0x1u, 1024u * 1024u));

        auto optionEEPROMData = commandDev_EEPROM->add_option("-d,--data", cliArgs.CommandDeviceEEPROM.WriteDataRaw,
            "Write data (quoted string with bytes in HEX format with optional '0x' prefix and optional separators: whitespaces, commas, colons and dashes).");

        auto optionEEPROMFile = commandDev_EEPROM->add_option("--file", cliArgs.CommandDeviceEEPROM.DataFileName, "File name.");

        auto optionEEPROMByte = commandDev_EEPROM->add_option("-b,--byte", cliArgs.CommandDeviceEEPROM.FillByte, "Fill byte value.")
            ->check(CLI::Range(0x00u, 0xFFu));

        optionEEPROMType->needs(optionEEPROMOperation);
        optionEEPROMOperation->needs(optionEEPROMAddress);

        //
        // Command 'dev::Si5351'.
        //

        commandDev_Si5351->add_flag("--init", cliArgs.CommandDeviceSi5351.Initialize, "Initialize registers.");

        auto optionSi5351Output = commandDev_Si5351->add_option("-o,--output", cliArgs.CommandDeviceSi5351.Output,
            "Select output: 0 or 2.")
            ->ignore_case()
            ->check(CLI::IsMember({ "0", "2" }, CLI::ignore_case));

        auto optionSi5351Enable = commandDev_Si5351->add_option("-e,--enable", cliArgs.CommandDeviceSi5351.Enable,
            "Enable output: 0=off, 1=on.")
            ->ignore_case()
            ->check(CLI::Range(0, 1));

        auto optionSi5351Invert = commandDev_Si5351->add_option("-i,--invert", cliArgs.CommandDeviceSi5351.Invert,
            "Invert output: 0=not inverted, 1=inverted.")
            ->ignore_case()
            ->check(CLI::Range(0, 1));

        auto optionSi5351Strength = commandDev_Si5351->add_option("-s,--strength", cliArgs.CommandDeviceSi5351.Strength,
            "Set output drive strength: 0=2mA, 1=4mA, 2=6mA, 3=8mA.")
            ->ignore_case()
            ->check(CLI::Range(0, 3));

        auto optionSi5351Frequency = commandDev_Si5351->add_option("-f,--frequency", cliArgs.CommandDeviceSi5351.Frequency,
            "Set output frequency in Hz (integer).")
            ->ignore_case()
            ->check(CLI::Range(8000, 160'000'000));

        optionSi5351Frequency->needs(optionSi5351Strength);
        optionSi5351Strength->needs(optionSi5351Invert);
        optionSi5351Invert->needs(optionSi5351Frequency);

        //
        // Parse CLI arguments.
        //

        try
        {
            app.parse(argc, argv);
        }
        catch (const CLI::ParseError& e)
        {
            app.exit(e);

            if ((e.get_name() == "CallForHelp")
             || (e.get_name() == "CallForAllHelp"))
            {
                return CLIParseResult::Help;
            }
            else
            {
                return CLIParseResult::Error;
            }
        }

        //
        // Create I2C interface.
        //

        const auto pI2CInterface = I2CInterfaceMake(cliArgs.I2CMaster.InterfaceName);
        assert(pI2CInterface != nullptr);

        if (optionTimeout->count() > 0)
        {
            pI2CInterface->setTimeout(cliArgs.CommandBasic.Timeout);
        }

        //
        // CLI arguments values check.
        //

        const auto [bitRateMin, bitRateMax] = pI2CInterface->getBitRateRange();

        if (cliArgs.I2CMaster.Bitrate < bitRateMin || cliArgs.I2CMaster.Bitrate > bitRateMax)
        {
            std::cout << std::format("Invalid BITRATE (in {} limited to {}-{} bps)\n",
                pI2CInterface->getName(), bitRateMin, bitRateMax);

            return CLIParseResult::Error;
        }

        if ((cliArgs.I2CMaster.SlaveAddress8bit & 0x01) != 0)
        {
            std::cout << "Invalid 8-bit SLAVE-ADDRESS (bit R/W must be 0)\n";
            return CLIParseResult::Error;
        }

        if (app.got_subcommand(commandWr) == true)
        {
            if (Utilities::parseHexString(cliArgs.CommandBasic.WriteDataRaw, cliArgs.CommandBasic.WriteData) != true)
            {
                return CLIParseResult::Error;
            }

            const auto [writeLengthMin, writeLengthMax] = pI2CInterface->getWriteLengthRange();
            const auto writeLength = cliArgs.CommandBasic.WriteData.size();

            if (writeLength < writeLengthMin || writeLength > writeLengthMax)
            {
                std::cout << std::format("Invalid DATA length (in {} limited to {}-{} bytes)\n",
                    pI2CInterface->getName(), writeLengthMin, writeLengthMax);

                return CLIParseResult::Error;
            }
        }
        else if (app.got_subcommand(commandRd) == true || app.got_subcommand(commandRda) == true)
        {
            const auto [readLengthMin, readLengthMax] = pI2CInterface->getReadLengthRange();

            if (cliArgs.CommandBasic.ReadLength < readLengthMin || cliArgs.CommandBasic.ReadLength > readLengthMax)
            {
                std::cout << std::format("Invalid READ-LENGTH (in {} limited to {}-{} bytes)\n",
                    pI2CInterface->getName(), readLengthMin, readLengthMax);

                return CLIParseResult::Error;
            }
        }

        if (commandDev->got_subcommand(commandDev_TMP75C) == true)
        {
            cliArgs.CommandDevice.DeviceName = "TMP75C";
        }
        else if (commandDev->got_subcommand(commandDev_MCP3426) == true)
        {
            cliArgs.CommandDevice.DeviceName = "MCP3426";
        }
        else if (commandDev->got_subcommand(commandDev_PCA6408A) == true)
        {
            cliArgs.CommandDevice.DeviceName = "PCA6408A";
        }
        else if (commandDev->got_subcommand(commandDev_EEPROM) == true)
        {
            if (cliArgs.CommandDeviceEEPROM.ListDeviceType == true)
            {
                std::cout << "Supported EEPROMs:\n";

                const auto names = I2C_Device::EEPROM::getSupportedList();

                for (const auto& str : names)
                {
                    std::cout << str << '\n';
                }

                return CLIParseResult::Help;
            }

            if ((optionEEPROMType->count() > 0)
                && (cliArgs.CommandDeviceEEPROM.DeviceType.empty() == false))
            {
                if (I2C_Device::EEPROM::isSupported(cliArgs.CommandDeviceEEPROM.DeviceType) == false)
                {
                    std::cout << "EEPROM '" << cliArgs.CommandDeviceEEPROM.DeviceType << "' is not supported.\n";
                    return CLIParseResult::Error;
                }
            }
            else
            {
                std::cout << "Missing options: -t,--type or --list.\n";
                return CLIParseResult::Error;
            }

            if (cliArgs.CommandDeviceEEPROM.DeviceFunction == "rd")
            {
                const bool isLength = optionEEPROMLength->count() > 0;

                if (isLength == false)
                {
                    std::cout << "Missing option -l,--length.\n";
                    return CLIParseResult::Error;
                }
            }
            else if (cliArgs.CommandDeviceEEPROM.DeviceFunction == "wr")
            {
                const bool isLength = optionEEPROMLength->count() > 0;
                const bool isFile = optionEEPROMFile->count() > 0;
                const bool isData = optionEEPROMData->count() > 0;

                if (isData == true)
                {
                    if (Utilities::parseHexString(cliArgs.CommandDeviceEEPROM.WriteDataRaw, cliArgs.CommandDeviceEEPROM.WriteData) != true)
                    {
                        return CLIParseResult::Error;
                    }
                }
                else
                {
                    if ((isLength && isFile) == false)
                    {
                        std::cout << "Missing options: -l,--length and -f,--file.\n";
                        return CLIParseResult::Error;
                    }
                }
            }
            else if (cliArgs.CommandDeviceEEPROM.DeviceFunction == "fill")
            {
                const bool isLength = optionEEPROMLength->count() > 0;
                const bool isByte = optionEEPROMByte->count() > 0;

                if ((isLength && isByte) == false)
                {
                    std::cout << "Missing options: -l,--length and -b,--byte.\n";
                    return CLIParseResult::Error;
                }
            }
            else
            {
                std::cout << "Invalid EEPROM function.\n";
                return CLIParseResult::Error;
            }
        }
        else if (commandDev->got_subcommand(commandDev_Si5351) == true)
        {
            cliArgs.CommandDevice.DeviceName = "Si5351";

            if (!cliArgs.CommandDeviceSi5351.Initialize
                && !cliArgs.CommandDeviceSi5351.Output)
            {
                std::cout << "Missing options: --init or -o,--output.\n";
                return CLIParseResult::Error;
            }

            if (cliArgs.CommandDeviceSi5351.Enable
                || cliArgs.CommandDeviceSi5351.Invert
                || cliArgs.CommandDeviceSi5351.Strength
                || cliArgs.CommandDeviceSi5351.Frequency)
            {
                if (!cliArgs.CommandDeviceSi5351.Output)
                {
                    std::cout << "Missing option: -o,--output.\n";
                    return CLIParseResult::Error;
                }
            }

            if (cliArgs.CommandDeviceSi5351.Output)
            {
                if (!cliArgs.CommandDeviceSi5351.Enable
                    && !cliArgs.CommandDeviceSi5351.Invert
                    && !cliArgs.CommandDeviceSi5351.Strength
                    && !cliArgs.CommandDeviceSi5351.Frequency)
                {
                    std::cout << "Missing options: -e, -i, -s, -f.\n";
                    return CLIParseResult::Error;
                }
            }
        }

        //
        // Create command for execution.
        //

        if (app.got_subcommand(commandDetect) == true)
        {
            pCommand = std::make_shared<CommandDetect>(pI2CInterface, cliArgs);
        }
        else if (app.got_subcommand(commandWr) == true)
        {
            pCommand = std::make_shared<CommandWrite>(pI2CInterface, cliArgs);
        }
        else if (app.got_subcommand(commandRd) == true)
        {
            pCommand = std::make_shared<CommandRead>(pI2CInterface, cliArgs);
        }
        else if (app.got_subcommand(commandRda) == true)
        {
            pCommand = std::make_shared<CommandReadWithAddress>(pI2CInterface, cliArgs);
        }
        else if (app.got_subcommand(commandDev) == true)
        {
            std::string deviceName;

            if (commandDev->got_subcommand(commandDev_EEPROM) == true)
            {
                deviceName = cliArgs.CommandDeviceEEPROM.DeviceType;
            }
            else
            {
                deviceName = cliArgs.CommandDevice.DeviceName;
            }

            auto pI2CDevice = I2C_Device::I2CDeviceMake(deviceName, pI2CInterface, cliArgs.I2CMaster.SlaveAddress8bit);
            assert(pI2CDevice != nullptr);

            pCommand = std::make_shared<CommandDevice>(pI2CInterface, cliArgs, pI2CDevice);
        }
        else
        {
            std::cout << "Unknown command\n";
            return CLIParseResult::Error;
        }

        return CLIParseResult::Success;
    }

    bool executeCommand()
    {
        bool result;

        if (pCommand == nullptr)
        {
            return false;
        }

        result = pCommand->openInterface();
        if (result != true)
        {
            return false;
        }

        const uint32_t repeatCounterMax = cliArgs.CommandRepeatCounter;
        uint32_t repeatCounter = 1;

        do
        {
            result = pCommand->execute();

            if (cliArgs.CommandRepeatCounter > 0)
            {
                std::cout << std::format("Wait {} for command repetition {} from {}...\n",
                    cliArgs.CommandRepeatDelay, repeatCounter, repeatCounterMax);

                repeatCounter++;

                std::this_thread::sleep_for(cliArgs.CommandRepeatDelay);
            }
        } while (cliArgs.CommandRepeatCounter-- > 0);

        pCommand->closeInterface();

        return result;
    }
};


int main(int argc, char* argv[])
{
#if defined _DEBUG
    spdlog::set_level(spdlog::level::trace);
#else
    spdlog::set_level(spdlog::level::critical);
#endif

    SPDLOG_INFO("I2CTool start");

    I2CTool i2cTool;
    I2CTool::CLIParseResult cliParseResult;
    bool commandResult = false;
    std::chrono::high_resolution_clock::time_point commandStartTime{};
    std::chrono::nanoseconds commandExecutionTime{};

    cliParseResult = i2cTool.parseCommand(argc, argv);
    if (cliParseResult == I2CTool::CLIParseResult::Success)
    {
        commandStartTime = std::chrono::high_resolution_clock::now();
        commandResult = i2cTool.executeCommand();
        commandExecutionTime = std::chrono::high_resolution_clock::now() - commandStartTime;
    }
    else if (cliParseResult == I2CTool::CLIParseResult::Help)
    {
        commandResult = true;
    }

    if (cliParseResult != I2CTool::CLIParseResult::Help)
    {
        if (cliParseResult == I2CTool::CLIParseResult::Success)
        {
            std::cout << std::format("Execution time: {}\n",
                std::chrono::duration_cast<std::chrono::milliseconds>(commandExecutionTime));
        }

        std::cout << std::format("Command result: {}\n",
            (commandResult == true) ? "success" : "failure");
    }

    SPDLOG_INFO("I2CTool end");

    return (commandResult == true) ? EXIT_SUCCESS : EXIT_FAILURE;
}
