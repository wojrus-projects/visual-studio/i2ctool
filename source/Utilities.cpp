#include <iostream>
#include <format>
#include <algorithm>
#include <regex>

#include "Utilities.h"


namespace Utilities
{
    void printI2CAddressTable(const std::vector<uint8_t>& addresses8bit)
    {
        std::cout << "+-----+-------------------------+-------------------------+\n";
        std::cout << "| Idx |        7-bit            |        8-bit            |\n";
        std::cout << "+-----+-------------------------+-------------------------+\n";

        for (size_t i = 0; i < addresses8bit.size(); i++)
        {
            const uint8_t address8bit = addresses8bit[i];

            std::cout << std::format("| {0: <3d} | 0x{1:02X} - 0b{1:08b} - {1: <3d} | 0x{2:02X} - 0b{2:08b} - {2: <3d} |\n",
                i, address8bit >> 1, address8bit);
        }

        std::cout << "+-----+-------------------------+-------------------------+\n";
    }


    void printData(const uint8_t* pData, size_t dataLength)
    {
        constexpr int ROW_LENGTH_BYTES = 16;

        int rowPaddingLength = 0;

        if ((dataLength % ROW_LENGTH_BYTES) != 0)
        {
            rowPaddingLength = ROW_LENGTH_BYTES - (int(dataLength) - (int(dataLength) / ROW_LENGTH_BYTES) * ROW_LENGTH_BYTES);
        }

        //
        // Header.
        //

        std::cout << "          ";

        for (int i = 0; i < ROW_LENGTH_BYTES; i++)
        {
            std::cout << std::format("  {:01X}", i);
        }

        std::cout << '\n';

        //
        // Data lines.
        //

        size_t dataOffset = 0;

        while (dataLength > 0)
        {
            const size_t rowDataLength = std::clamp(dataLength, size_t(1), size_t(ROW_LENGTH_BYTES));

            std::cout << std::format("0x{:08X}: ", dataOffset);

            //
            // Hex format segment.
            //

            for (size_t i = 0; i < rowDataLength; i++)
            {
                std::cout << std::format("{:02X} ", pData[i]);
            }

            if (rowDataLength < ROW_LENGTH_BYTES)
            {
                for (int i = 0; i < rowPaddingLength; i++)
                {
                    std::cout << "-- ";
                }
            }

            //
            // ASCII format segment.
            //

            std::cout << "  | ";

            for (size_t i = 0; i < rowDataLength; i++)
            {
                const int c = (int)(unsigned int)pData[i];

                if (std::isprint(c) != 0)
                {
                    std::cout << (char)c;
                }
                else
                {
                    std::cout << '.';
                }
            }

            if (rowDataLength < ROW_LENGTH_BYTES)
            {
                for (int i = 0; i < rowPaddingLength; i++)
                {
                    std::cout << '-';
                }
            }

            std::cout << " |\n";

            dataLength -= rowDataLength;
            dataOffset += rowDataLength;
            pData += rowDataLength;
        }
    }


    void printData(std::span<const uint8_t> data)
    {
        printData(data.data(), data.size());
    }


    void printCArray(const uint8_t* pData, size_t dataLength)
    {
        constexpr int ROW_LENGTH_BYTES = 16;

        std::cout << "<CArray>\n";

        for (size_t i = 0; i < dataLength; i++)
        {
            if ((i > 0) && (i % ROW_LENGTH_BYTES == 0))
            {
                std::cout << "\n";
            }

            std::cout << std::format("0x{:02X}", pData[i]);

            if (i < (dataLength - 1))
            {
                std::cout << ", ";
            }
        }

        std::cout << "\n</CArray>\n";
    }


    void printCArray(std::span<const uint8_t> data)
    {
        printCArray(data.data(), data.size());
    }


    bool parseHexString(std::string hexString, std::vector<uint8_t>& bytes)
    {
        try
        {
            const std::regex regexHexData("(?:\\s*)(?:0[xX])?([0-9a-fA-F]{2})(?:\\s*[,:-]?\\s*)", std::regex::ECMAScript);

            std::smatch match;

            while (std::regex_search(hexString, match, regexHexData) == true)
            {
                if (match.size() != 2)
                {
                    std::cout << std::format("Invalid data length\n");
                    return false;
                }

                if (match.prefix().length() != 0)
                {
                    std::cout << std::format("Invalid data syntax at index: 0 to {} in substring: '{}'\n", match.prefix().length(), hexString);
                    return false;
                }

                const std::string strOctet{ match.str(1) };

                if (strOctet.length() != 2)
                {
                    std::cout << std::format("Invalid data length\n");
                    return false;
                }

                std::from_chars_result fromCharsResult;
                uint8_t octetValue;

                fromCharsResult = std::from_chars(strOctet.c_str(), strOctet.c_str() + 2, octetValue, 16);
                if (fromCharsResult.ec != std::errc())
                {
                    std::cout << "Invalid octet string: '" << strOctet << "'\n";
                    return false;
                }

                bytes.push_back(octetValue);
                hexString = match.suffix().str();
            }

            if (hexString.length() != 0)
            {
                std::cout << std::format("Invalid data syntax at index 0 to end in substring: '{}'\n", hexString);
                return false;
            }
        }
        catch (const std::regex_error& err)
        {
            std::cout << "Exception: " << err.what() << std::endl;
            throw;
        }

        return true;
    }
} // namespace Utilities
