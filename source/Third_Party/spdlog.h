//
// Global configuration for logger.
//

#pragma once

#if defined _DEBUG
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE
#endif

#include "spdlog/spdlog.h"
#include "spdlog/fmt/bin_to_hex.h"
