#pragma once

#include <cstdint>
#include <span>
#include <string>
#include <vector>


namespace Utilities
{
    /// <summary>
    /// Wyświetla tabelę z adresami urządzeń I2C.
    /// </summary>
    /// <param name="addresses8bit">8-bitowe adresy I2C</param>
    void printI2CAddressTable(const std::vector<uint8_t>& addresses8bit);


    /// <summary>
    /// Wyświetla tabelę z danymi binarnymi w formatach HEX i ASCII.
    /// </summary>
    /// <param name="pData">Dane</param>
    /// <param name="dataLength">Długość danych w bajtach</param>
    void printData(const uint8_t* pData, size_t dataLength);


    /// <summary>
    /// Wyświetla tabelę z danymi binarnymi w formatach HEX i ASCII.
    /// </summary>
    /// <param name="data">Dane</param>
    void printData(std::span<const uint8_t> data);


    /// <summary>
    /// Wyświetla dane binarne w formacie HEX jako tablica C.
    /// </summary>
    /// <param name="pData">Dane</param>
    /// <param name="dataLength">Długość danych w bajtach</param>
    void printCArray(const uint8_t* pData, size_t dataLength);


    /// <summary>
    /// Wyświetla dane binarne w formacie HEX jako tablica C.
    /// </summary>
    /// <param name="data">Dane</param>
    void printCArray(std::span<const uint8_t> data);


    /// <summary>
    /// Parsuje string z bajtami w formacie HEX.
    /// Bajty muszą być podane w cudzysłowie w postaci HEX z opcjonalnymi prefiksami '0x'
    /// oraz z opcjonalnymi separatorami (spacja, przecinek, dwukropek, minus).
    /// Przykład:
    /// "0x00, 0x31, 0x32, 0x33 AB:CD 55-56-57"
    /// </summary>
    /// <param name="hexString">String z bajtami HEX</param>
    /// <param name="bytes">Dane binarne po parsowaniu</param>
    /// <returns>Status parsowania: false=niepoprawne, true=poprawne</returns>
    bool parseHexString(std::string hexString, std::vector<uint8_t>& bytes);
}
